<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
*/

Route::namespace('Auth')->middleware(['throttle:30,1'])->group(function () {

    // Landing page for top level domain that will determine whether user is
    // authenticated and redirect to `auth.login.create` or `user.dashboard.index`
    Route::get('/', ['as' => 'auth.check', 'uses' => 'AuthCheckController@index']);

    // Redirect the /user path to the /user/dashboard
    Route::get('/register', function () {
        return redirect()->route('auth.register.account.create');
    });

    // New account form
    Route::get('/register/account', ['as' => 'auth.register.account.create', 'uses' => 'AuthRegisterAccountController@create']);
    Route::post('/register/account', ['as' => 'auth.register.account.store', 'uses' => 'AuthRegisterAccountController@store']);

    // Edit account form (for changes mid-registration)
    Route::get('/register/account/edit', ['as' => 'auth.register.account.edit', 'uses' => 'AuthRegisterAccountController@edit']);
    Route::patch('/register/account', ['as' => 'auth.register.account.patch', 'uses' => 'AuthRegisterAccountController@update']);

    // New account plan selection
    Route::get('/register/plan', ['as' => 'auth.register.plan.create', 'uses' => 'AuthRegisterPlanController@create']);
    Route::post('/register/plan', ['as' => 'auth.register.plan.store', 'uses' => 'AuthRegisterPlanController@store']);

    // Edit plan (for changes mid-registration)
    Route::get('/register/plan/edit', ['as' => 'auth.register.plan.edit', 'uses' => 'AuthRegisterPlanController@edit']);
    Route::patch('/register/plan', ['as' => 'auth.register.plan.update', 'uses' => 'AuthRegisterPlanController@update']);

    // New account billing information
    Route::get('/register/billing', ['as' => 'auth.register.billing.create', 'uses' => 'AuthRegisterBillingController@create']);
    Route::post('/register/billing', ['as' => 'auth.register.billing.store', 'uses' => 'AuthRegisterBillingController@store']);

    // Checkout Form for Plan Upgrades and Downgrades
    Route::get('/register/checkout', ['as' => 'auth.register.checkout.create', 'uses' => 'AuthRegisterCheckoutController@create']);
    Route::post('/register/checkout', ['as' => 'auth.register.checkout.store', 'uses' => 'AuthRegisterCheckoutController@store']);

    // Login form
    Route::get('/login', ['as' => 'auth.login.create', 'uses' => 'AuthLoginController@create']);
    Route::post('/login', ['as' => 'auth.login.store', 'uses' => 'AuthLoginController@store']);

    // Reset Password Request
    Route::get('/reset', ['as' => 'auth.reset.create', 'uses' => 'AuthResetPasswordController@create']);
    Route::post('/reset', ['as' => 'auth.reset.store', 'uses' => 'AuthResetPasswordController@store']);

    // Reset Password Form using Token
    Route::get('/reset/{token}', ['as' => 'auth.reset.edit', 'uses' => 'AuthResetPasswordController@edit']);
    Route::patch('/reset/{token}', ['as' => 'auth.reset.update', 'uses' => 'AuthResetPasswordController@update']);

    // Logout action (no POST required)
    Route::get('/logout', ['as' => 'auth.logout', 'uses' => 'AuthLogoutController@get'])->middleware(['auth']);

});

Route::namespace('User')->middleware(['auth', 'throttle:30,1'])->group(function () {

    // Redirect the /user path to the /user/dashboard
    Route::get('/user', function () {
        return redirect()->route('user.dashboard.index');
    });

    // User dashboard
    Route::get('/user/tutorial', ['as' => 'user.tutorial.index', 'uses' => 'UserTutorialController@index']);
    Route::get('/user/dashboard', ['as' => 'user.dashboard.index', 'uses' => 'UserDashboardController@index']);

    Route::prefix('user/profile')->group(function () {

        // User profile information
        Route::get('/', ['as' => 'user.profile.show', 'uses' => 'UserProfileController@show']);

        // Edit user account information
        Route::get('/edit', ['as' => 'user.profile.edit', 'uses' => 'UserProfileController@edit']);
        Route::patch('/', ['as' => 'user.profile.update', 'uses' => 'UserProfileController@update']);

        // Delete User Account
        Route::delete('/', ['as' => 'user.profile.delete', 'uses' => 'UserProfileController@delete']);

        // Download SSH Key Pair
        Route::get('/ssh/key/download', ['as' => 'user.profile.ssh.key.download', 'uses' => 'UserSshKeyController@download']);

    });

    Route::prefix('user/image/templates')->group(function () {

        // List of image templates
        Route::get('/', ['as' => 'user.image.templates.index', 'uses' => 'UserImageTemplateController@index']);

        // Create an image template
        Route::get('/create', ['as' => 'user.image.templates.create', 'uses' => 'UserImageTemplateController@create']);
        Route::post('/', ['as' => 'user.image.templates.store', 'uses' => 'UserImageTemplateController@store']);

        // Get an existing image template
        Route::get('/{cloud_image_template_id}', ['as' => 'user.image.templates.show', 'uses' => 'UserImageTemplateController@show']);

        // Edit an image template
        Route::get('/{cloud_image_template_id}/edit', ['as' => 'user.image.templates.edit', 'uses' => 'UserImageTemplateController@edit']);
        Route::patch('/{cloud_image_template_id}', ['as' => 'user.image.templates.update', 'uses' => 'UserImageTemplateController@update']);

        // Delete Image Template
        Route::delete('/{cloud_image_template_id}', ['as' => 'user.image.templates.delete', 'uses' => 'UserImageTemplateController@delete']);

        // Get usage for an image template
        Route::get('/{cloud_image_template_id}/usage', ['as' => 'user.image.templates.usage.index', 'uses' => 'UserImageTemplateUsageController@index']);

    });

    Route::prefix('user/image/settings')->group(function () {

        // Update image template default settings
        Route::get('/', ['as' => 'user.image.settings.edit', 'uses' => 'UserImageSettingController@edit']);
        Route::patch('/', ['as' => 'user.image.settings.update', 'uses' => 'UserImageSettingController@update']);

    });

    Route::prefix('user/reservations')->group(function () {

        // List of reservations
        Route::get('/', ['as' => 'user.reservations.index', 'uses' => 'UserReservationController@index']);

        // Create an reservation
        Route::get('/create', ['as' => 'user.reservations.create', 'uses' => 'UserReservationController@create']);
        Route::post('/', ['as' => 'user.reservations.store', 'uses' => 'UserReservationController@store']);

        // Get an existing reservation (created with user.image.templates.provision)
        //Route::get('/{tenant_reservation_id}', ['as' => 'user.reservations.show', 'uses' => 'UserReservationController@show']);

        // Edit an existing reservation
        Route::get('/{tenant_reservation_id}/edit', ['as' => 'user.reservations.edit', 'uses' => 'UserReservationController@edit']);
        Route::patch('/{tenant_reservation_id}', ['as' => 'user.reservations.update', 'uses' => 'UserReservationController@update']);

        // Actions for a reservation
        Route::get('/{tenant_reservation_id}/access', ['as' => 'user.reservations.access', 'uses' => 'UserReservationController@access']);
        Route::post('/{tenant_reservation_id}/extend', ['as' => 'user.reservations.extend', 'uses' => 'UserReservationController@extend']);
        Route::post('/{tenant_reservation_id}/snapshot', ['as' => 'user.reservations.snapshot', 'uses' => 'UserReservationController@snapshot']);
        Route::post('/{tenant_reservation_id}/terminate', ['as' => 'user.reservations.terminate', 'uses' => 'UserReservationController@terminate']);

    });

    Route::prefix('user/tokens')->group(function () {

        // List of reservation tokens
        Route::get('/', ['as' => 'user.tokens.index', 'uses' => 'UserTokenController@index']);

        // Create a reservation token
        Route::get('/create', ['as' => 'user.tokens.create', 'uses' => 'UserTokenController@create']);
        Route::post('/', ['as' => 'user.tokens.store', 'uses' => 'UserTokenController@store']);

        // Get an existing reservation token
        Route::get('/{tenant_reservation_token_id}', ['as' => 'user.tokens.show', 'uses' => 'UserTokenController@show']);

        // Edit an existing reservation token
        Route::patch('/{tenant_reservation_token_id}', ['as' => 'user.tokens.update', 'uses' => 'UserTokenController@update']);

        // Deactivate or delete a token
        Route::delete('/{tenant_reservation_token_id}/delete', ['as' => 'user.tokens.delete', 'uses' => 'UserTokenController@delete']);

        // Usage for reservation token
        Route::get('/{tenant_reservation_token_id}/usage', ['as' => 'user.tokens.usage.index', 'uses' => 'UserTokenUsageController@index']);

        // Actions for a reservation token
        //Route::get('/{tenant_reservation_token_id}/access', ['as' => 'user.reservations.tokens.access', 'uses' => 'UserReservationTokenController@access']);
        //Route::post('/{tenant_reservation_token_id}/extend', ['as' => 'user.reservations.tokens.extend', 'uses' => 'UserReservationTokenController@extend']);
        //Route::post('/{tenant_reservation_token_id}/snapshot', ['as' => 'user.reservations.tokens.snapshot', 'uses' => 'UserReservationTokenController@snapshot']);
        //Route::post('/{tenant_reservation_token_id}/terminate', ['as' => 'user.reservations.tokens.terminate', 'uses' => 'UserReservationTokenController@terminate']);

    });

    Route::prefix('user/billing')->group(function () {

        // Billing Dashboard
        Route::get('/', ['as' => 'user.billing.index', 'uses' => 'UserBillingController@index']);

        // Billing Usage Dashboard
        Route::get('/usage', ['as' => 'user.billing.usage.index', 'uses' => 'UserBillingUsageController@index']);

        // Download Invoice
        Route::get('/{chargebee_customer_id}/invoices/{chargebee_invoice_id}/download', ['as' => 'user.billing.invoice.download', 'uses' => 'UserBillingController@downloadInvoice']);

        // Modal Window Form Posts for Updating Payment and Billing Address
        Route::patch('/payment', ['as' => 'user.billing.payment.update', 'uses' => 'UserBillingPaymentController@update']);
        Route::patch('/address', ['as' => 'user.billing.address.update', 'uses' => 'UserBillingAddressController@update']);

        // Checkout Form for Plan Upgrades and Downgrades
        //Route::get('/checkout', ['as' => 'user.billing.checkout.create', 'uses' => 'UserBillingCheckoutController@create']);
        //Route::post('/checkout', ['as' => 'user.billing.checkout.store', 'uses' => 'UserBillingCheckoutController@store']);

        // Checkout Form for Plan Upgrades and Downgrades
        Route::post('/plan/upgrade', ['as' => 'user.billing.plan.upgrade', 'uses' => 'UserBillingPlanController@upgrade']);
        Route::post('/plan/downgrade', ['as' => 'user.billing.plan.downgrade', 'uses' => 'UserBillingPlanController@downgrade']);
        Route::post('/plan/cancel', ['as' => 'user.billing.plan.cancel', 'uses' => 'UserBillingPlanController@cancel']);

        // Cancellation Form for Paid Plan Cancellation
        Route::get('/cancel', ['as' => 'user.billing.cancel.create', 'uses' => 'UserBillingCancelController@create']);
        Route::post('/cancel', ['as' => 'user.billing.cancel.store', 'uses' => 'UserBillingCancelController@store']);

        // Chargbee return URL webhook
        Route::get('/webhook/checkout', ['as' => 'user.billing.webhook.checkout', 'uses' => 'UserBillingWebhookController@checkout']);

    });

});

Route::namespace('Support')->group(function () {

    // Redirect the /support path to the /support/dashboard
    Route::get('/support', function () {
        return redirect()->route('support.kb.index');
    });

    // Support dashboard
    Route::get('/support/dashboard', function () {
        return redirect()->route('support.kb.index');
    });

    Route::get('/support/kb', function () {
        return redirect('https://cloudstration.freshdesk.com/support/solutions/44000802406');
    })->name('support.kb.index');

    // Billing

    Route::get('/support/kb/trylabs/billing/cancellation-policy', function () {
        return redirect('https://cloudstration.freshdesk.com/xxx');
    })->name('support.kb.trylabs.billing.cancellation-policy');

    Route::get('/support/kb/trylabs/billing/plan-changes', function () {
        return redirect('https://cloudstration.freshdesk.com/support/solutions/articles/44001781194');
    })->name('support.kb.trylabs.billing.plan-changes');

    // Image Templates

    Route::get('/support/kb/trylabs/templates/how-to-build', function () {
        return redirect('https://cloudstration.freshdesk.com/support/solutions/articles/44001783483');
    })->name('support.kb.trylabs.templates.how-to-build');

    Route::get('/support/kb/trylabs/templates/best-practices', function () {
        return redirect('https://cloudstration.freshdesk.com/support/solutions/articles/44001783484');
    })->name('support.kb.trylabs.templates.best-practices');

    Route::get('/support/kb/trylabs/templates/tutorials-linux-ocean', function () {
        return redirect('https://cloudstration.freshdesk.com/support/solutions/articles/44001783490');
    })->name('support.kb.trylabs.templates.tutorials-linux-ocean');

    // Reservations

    Route::get('/support/kb/trylabs/reservations/ssh-keys', function () {
        return redirect('https://cloudstration.freshdesk.com/support/solutions/articles/44001781199');
    })->name('support.kb.trylabs.reservations.ssh-keys');

    Route::get('/support/kb/trylabs/reservations/using-guacamole-vnc', function () {
        return redirect('https://cloudstration.freshdesk.com/support/solutions/articles/44001783472');
    })->name('support.kb.trylabs.reservations.using-guacamole-vnc');

    Route::get('/support/kb/trylabs/reservations/using-guacamole-ssh', function () {
        return redirect('https://cloudstration.freshdesk.com/support/solutions/articles/44001783473');
    })->name('support.kb.trylabs.reservations.using-guacamole-ssh');

    // Lab Access Tokens

    Route::get('/support/kb/trylabs/tokens/how-to-redeem', function () {
        return redirect('https://cloudstration.freshdesk.com/support/solutions/articles/44001783485');
    })->name('support.kb.trylabs.tokens.how-to-redeem');

    Route::get('/support/kb/trylabs/tokens/best-practices', function () {
        return redirect('https://cloudstration.freshdesk.com/support/solutions/articles/44001783474');
    })->name('support.kb.trylabs.tokens.best-practices');

    Route::get('/support/kb/trylabs/tokens/ux', function () {
        return redirect('https://cloudstration.freshdesk.com/support/solutions/articles/44001783475');
    })->name('support.kb.trylabs.tokens.ux');

    Route::get('/support/kb/trylabs/tokens/share-email', function () {
        return redirect('https://cloudstration.freshdesk.com/support/solutions/articles/44001783476');
    })->name('support.kb.trylabs.tokens.share-email');

    Route::get('/support/kb/trylabs/tokens/share-sharepoint', function () {
        return redirect('https://cloudstration.freshdesk.com/support/solutions/articles/44001783477');
    })->name('support.kb.trylabs.tokens.share-sharepoint');

    Route::get('/support/kb/trylabs/tokens/share-html', function () {
        return redirect('https://cloudstration.freshdesk.com/support/solutions/articles/44001783478');
    })->name('support.kb.trylabs.tokens.share-html');

    Route::get('/support/kb/trylabs/tokens/share-youtube', function () {
        return redirect('https://cloudstration.freshdesk.com/support/solutions/articles/44001783479');
    })->name('support.kb.trylabs.tokens.share-youtube');

    Route::get('/support/kb/trylabs/tokens/share-lms-skilljar', function () {
        return redirect('https://cloudstration.freshdesk.com/support/solutions/articles/44001783480');
    })->name('support.kb.trylabs.tokens.share-lms-skilljar');

    Route::get('/support/kb/trylabs/tokens/share-lms-other', function () {
        return redirect('https://cloudstration.freshdesk.com/support/solutions/articles/44001783481');
    })->name('support.kb.trylabs.tokens.share-lms-other');

    // Legal

    Route::get('/support/kb/trylabs/legal/terms-of-service', function () {
        return redirect('https://cloudstration.freshdesk.com/support/solutions/articles/44001781197');
    })->name('support.kb.trylabs.legal.terms-of-service');

    Route::get('/support/kb/trylabs/legal/privacy-policy', function () {
        return redirect('https://cloudstration.freshdesk.com/support/solutions/articles/44001781198');
    })->name('support.kb.trylabs.legal.privacy-policy');

    /*
    Route::get('/support/kb/trylabs/roadmap/xxx', function () {
        return redirect('https://cloudstration.freshdesk.com/xxx');
    })->name('support.kb.trylabs.roadmap.xxx');
    */

    /*
    Route::get('/support/kb/trylabs/bugs/xxx', function () {
        return redirect('https://cloudstration.freshdesk.com/xxx');
    })->name('support.kb.trylabs.bugs.xxx');
    */

    /*
    Route::get('/support/kb/trylabs/releases/xxx', function () {
        return redirect('https://cloudstration.freshdesk.com/xxx');
    })->name('support.kb.trylabs.releases.xxx');
    */

});

Route::namespace('Legal')->group(function () {

    Route::get('/legal', function () {
        return redirect('https://cloudstration.freshdesk.com/support/solutions/folders/44001193401');
    })->name('legal.terms.show');

    Route::get('/support/kb/trylabs/legal/terms-of-service', function () {
        return redirect('https://cloudstration.freshdesk.com/support/solutions/articles/44001781197');
    })->name('legal.terms.show');

    Route::get('/support/kb/trylabs/legal/privacy-policy', function () {
        return redirect('https://cloudstration.freshdesk.com/support/solutions/articles/44001781198');
    })->name('legal.privacy.show');

});
