<?php

return [
    'api' => [
        'debug' => env('TRYLABS_API_DEBUG', false),
        'url' => env('TRYLABS_API_URL'),
        'api_key' => env('TRYLABS_API_KEY'),
    ],
    'login' => [
        'enabled' => env('TRYLABS_LOGIN_ENABLED', true),
    ],
    'motd' => [
        'text' => env('TRYLABS_MOTD_TEXT'),
        'color' => env('TRYLABS_MOTD_COLOR', 'danger'),
        'enabled' => env('TRYLABS_MOTD_ENABLED', false),
    ],
    'provider' => [
        'ocean' => [
            'api' => [
                'key' => env('TRYLABS_PROVIDER_OCEAN_API_KEY', false),
            ]
        ],
    ],
    'portal' => [
        'reservation' => [
            'url' => env('TRYLABS_PORTAL_RESERVATION_URL', false),
        ],
        'tenant' => [
            'url' => env('TRYLABS_PORTAL_TENANT_URL', false),
        ]
    ],
    'recaptcha' => [
        'enabled' => env('TRYLABS_RECPATCHA_ENABLED', false),
    ],
    'registration' => [
        'enabled' => env('TRYLABS_REGISTRATION_ENABLED', true),
    ],
    'reservations' => [
        'enabled' => env('TRYLABS_RESERVATIONS_ENABLED', true)
    ],


];
