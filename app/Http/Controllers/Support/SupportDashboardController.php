<?php

namespace App\Http\Controllers\Support;

use App\Libraries\BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;

class SupportDashboardController extends BaseController
{

    public function index(Request $request)
    {

        return redirect('http://support.cloudstration.com');

    }

}
