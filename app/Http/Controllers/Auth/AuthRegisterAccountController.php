<?php

namespace App\Http\Controllers\Auth;

use App\Libraries\BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Validator;

class AuthRegisterAccountController extends BaseController
{

    public function create(Request $request)
    {

        // Use Laravel authentication to check if user has authenticated
        if (Auth::check()) {

            // Redirect to the user dashboard
            return redirect()->route('user.dashboard.index');

        // If user has not been authenticated
        } else {

            // Show the register form
            return view('auth.register.account.create', compact([
                'request'
            ]));

        }

    }

    public function store(Request $request)
    {

        //
        // Input Validation using Laravel Validator
        // --------------------------------------------------------------------
        // The validation rules below are the same ones that we have in our
        // API. If validation fails, Laravel will redirect the user back to
        // the account creation page with the error message(s).
        //

        // Validate input
        $validator = Validator::make($request->all(), [
            'timezone' => 'required|timezone',
            'first_name' => 'required|max:30|regex:/^[a-zA-Z]+(([\',. -][a-zA-Z ])?[a-zA-Z]*)*$/',
            'last_name' => 'required|max:30|regex:/^[a-zA-Z]+(([\',. -][a-zA-Z ])?[a-zA-Z]*)*$/',
            'job_title' => 'nullable|string|max:100',
            'organization_name' => 'nullable|string|max:50',
            'email' => 'required|email|unique:auth_user_accounts,email|max:100',
            'phone_number' => 'nullable|numeric|digits_between:4,14',
            'phone_country_code' => 'nullable|numeric|digits_between:1,3|required_with:phone_number',
            'password' => 'required|min:8|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/',
            'confirm_password' => 'same:password',
            'flag_privacy_accepted' => 'accepted',
        ],
        [
            'email.unique' => 'That email address has already been registered.',
            'password.min' => 'Your password must be at least 8 characters.',
            'password.regex' => 'Your password must contain an upper case letter, lowercase letter, number and one of the following symbols (#?!@$%^&*-).',
            'confirm_password.same' => 'Your password does not match.',
            'flag_privacy_accepted.accepted' => 'You have not accepted the privacy policy.'
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route('auth.register.account.create')
                ->withErrors($validator)
                ->withInput();
        }

        //
        // Use API to save record in the database
        // --------------------------------------------------------------------
        // All of the logic for storing the record in the database is located
        // in the API.
        //

        // Create array of request data that will be used in array_get below
        // and make handling null values easier than throwing exceptions
        $request_data = $request->all();

        // Perform API call to store data
        $api_request = $this->postApiRequest('/v1/admin/auth/user/accounts', [
            'first_name' => array_get($request_data, 'first_name'),
            'last_name' => array_get($request_data, 'last_name'),
            'job_title' => array_get($request_data, 'job_title'),
            'organization_name' => array_get($request_data, 'organization_name'),
            'email' => array_get($request_data, 'email'),
            'phone_number' => array_get($request_data, 'phone_number'),
            'phone_country_code' => array_get($request_data, 'phone_country_code'),
            'password' => array_get($request_data, 'password'),
            'timezone' => array_get($request_data, 'timezone', 'UTC'),
            'flag_privacy_accepted' => 1,
            'notify_send_verification_email' => 1
        ]);

        // If API request failed, there is likely a system error that can
        // not be handled by the user. We will create a log entry and return
        // a 500 error to the user for additional investigation.
        if($api_request->meta->result != 'success') {
            Log::debug(
                'AuthRegisterAccountController store() API request did not return success result',
                [
                    'request-data' => $request->all(),
                    'api-request' => $api_request
                ]
            );

            abort(500, 'There was an error creating your account. Please contact the support team for assistance.');
        }

        //
        // Authenticate User with New Session
        // --------------------------------------------------------------------
        // Now that the account has been created, we will automatically log the
        // user in to proceed to the next step of the registration process.
        //

        // Create array with credentials to check. Note that we can add additional
        // fields to check in the auth_user_accounts table if needed (useful for
        // active flags on normal log in, etc).
        $credentials = [
            'email' => $request_data['email'],
            'password' => $request_data['password']
        ];

        // Use Laravel facade to attempt to log in
        if(Auth::attempt($credentials, $remember = true)) {

            // Generate a new authenticated session
            $request->session()->regenerate();

            // Redirect the user to the next step (select a plan)
            return redirect()->route('auth.register.plan.create');

        // If authentication failed, there is likely a system error that can
        // not be handled by the user. We will create a log entry and return
        // a 500 error to the user for additional investigation.
        } else {

            Log::debug(
                'AuthRegisterAccountController store() Unable to authenticate user',
                [
                    'request-data' => $request->all(),
                    'api-request' => $api_request
                ]
            );

            abort(500, 'There was an error creating your account. Please contact the support team for assistance.');

        }

    }

    /*
    public function edit(Request $request)
    {
        //
    }

    public function update(Request $request)
    {
        //
    }
    */

}
