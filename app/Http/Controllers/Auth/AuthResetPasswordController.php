<?php

namespace App\Http\Controllers\Auth;

use App\Libraries\BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthResetPasswordController extends BaseController
{

    public function create(Request $request)
    {

        // Use Laravel authentication to check if user has authenticated
        if (Auth::check()) {

            // Redirect to the user dashboard
            return redirect()->route('user.dashboard.index');

        // If user has not been authenticated
        } else {

            // Show the login form
            return view('auth.login.create', compact([
                'request'
            ]));

        }

    }

    public function store(Request $request)
    {

        //

    }

    public function edit($token, Request $request)
    {

    }

    public function update($token, Request $request)
    {

    }

}
