<?php

namespace App\Http\Controllers\Auth;

use App\Libraries\BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthCheckController extends BaseController
{

    public function index(Request $request)
    {

        // Use Laravel authentication to check if user has authenticated
        if (Auth::check()) {

            // Redirect to the user dashboard
            return redirect()->route('user.dashboard.index');

        // If user has not been authenticated
        } else {

            // Redirect to the login page
            return redirect()->route('auth.login.create');

        }

    }

}
