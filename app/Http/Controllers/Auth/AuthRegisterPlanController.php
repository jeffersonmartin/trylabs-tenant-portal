<?php

namespace App\Http\Controllers\Auth;

use App\Libraries\BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;

class AuthRegisterPlanController extends BaseController
{

    public function create(Request $request)
    {

        // Use Laravel authentication to check if user has authenticated
        if (Auth::check()) {

            // Check if session variable has been set for plan prefix
            if($request->session()->has('plan_id_prefix')) {

                // Redirect to the plan edit form
                return redirect()->route('auth.register.plan.edit');

            } else {

                // Show the plan selection form
                return view('auth.register.plan.create', compact([
                    'request'
                ]));

            }

        // If user has not been authenticated
        } else {

            // Redirect to the create user account form
            return redirect()->route('auth.register.account.create');

        }

    }

    public function store(Request $request)
    {

        //
        // Input Validation using Laravel Validator
        // --------------------------------------------------------------------
        // The validation rules below are NOT related to the API.
        //

        // Validate input
        $validator = Validator::make($request->all(), [
            'plan_id' => 'required|string|max:30|starts_with:try1u',
            'flag_terms_accepted' => 'accepted',
        ],
        [
            'flag_terms_accepted.accepted' => 'You have not accepted the terms of service.'
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route('auth.register.plan.create')
                ->withErrors($validator)
                ->withInput();
        }

        // Create array of request data that will be used in array_get below
        // and make handling null values easier than throwing exceptions
        $request_data = $request->all();

        //
        // Use API to update user account with terms accepted
        // --------------------------------------------------------------------
        // All of the logic for updating the record in the database is located
        // in the API.
        //

        // Perform API call to update data
        $api_request = $this->patchApiRequest('/v1/admin/auth/user/accounts/'.$request->user()->id, [
            'flag_terms_accepted' => 1,
        ]);

        // If API request failed, there is likely a system error that can
        // not be handled by the user. We will create a log entry and return
        // a 500 error to the user for additional investigation.
        if($api_request->meta->result != 'success') {
            Log::debug(
                'AuthRegisterPlanController store() API request did not return success result',
                [
                    'request-data' => $request->all(),
                    'api-request' => $api_request
                ]
            );

            abort(500, 'There was an error with accepting the terms of service. Please contact the support team for assistance.');
        }

        // If free plan was selected, we will bypass the billing page and proceed
        // to the activation screen. When the user account was created, the free
        // subscription plan was automatically selected so we do not need to take
        // any additional action that would have otherwise been performed in the
        // AuthRegisterBillingController::store() method.
        if($request_data['plan_id'] == 'try1u-plan-free-mo') {

            // Redirect the user to the dashboard page
            return redirect()->route('user.dashboard.index');

        } else {

            // Get Chargebee Subscription ID from Tenant Account Table
            $tenant_account = $this->getApiRequest('/v1/admin/tenant/accounts/'.$request->user()->tenant_account_id);

            // Create array of parameters for Chargebee subscription
            $chargebee_params = [
                'redirect_url' => config('trylabs.portal.tenant.url').'/user/billing/webhook/checkout',
                'cancel_url' => config('trylabs.portal.tenant.url').'/user/billing',
                'subscription_id' => $tenant_account->data->chargebee_subscription_id,
                'plan_id' => $request_data['plan_id']
            ];

            // Determine if plan is monthly or annual to determine whether coupon is added
            $plan_term = substr($request_data['plan_id'], -2);

            if($plan_term == 'yr') {
                $chargebee_params['coupon_id'] = $request_data['plan_id'].'-disc';
            }

            // Use API to create a hosted page URL for redirecting to complete payment and checkout
            $chargebee_hosted_page = $this->postApiRequest('/v1/admin/chargebee/hosted/pages/checkout/existing', $chargebee_params);

            // TODO Save ID of hosted page to database

            // Redirect the user to the next step (billing information)
            return redirect($chargebee_hosted_page->url);

        }

    }

    public function edit(Request $request)
    {
        // Use Laravel authentication to check if user has authenticated
        if (Auth::check()) {

            // Show the plan form
            return view('auth.register.plan.edit', compact([
                'request'
            ]));

        // If user has not been authenticated
        } else {

            // Redirect to the user dashboard
            return redirect()->route('auth.register.account.create');

        }
    }

    public function update(Request $request)
    {

        //
        // Input Validation using Laravel Validator
        // --------------------------------------------------------------------
        // The validation rules below are NOT related to the API.
        //

        // Validate input
        $validator = Validator::make($request->all(), [
            'plan_id_prefix' => 'required|string|max:30',
            'flag_terms_accepted' => 'accepted',
        ],
        [
            'flag_terms_accepted.accepted' => 'You have not accepted the terms of service.'
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route('auth.register.plan.edit')
                ->withErrors($validator)
                ->withInput();
        }

        // Create array of request data that will be used in array_get below
        // and make handling null values easier than throwing exceptions
        $request_data = $request->all();

        //
        // Use session variables to save plan ID prefix
        // --------------------------------------------------------------------
        // Since the plan ID with Chargebee has both the plan and interval that
        // we bill them at (ex. lab1u-plan-300-mo), and we have broken the plan
        // selection and billing interval selection onto two pages, we capture
        // the plan ID prefix (ex. lab1u-plan-300) and store it as a session
        // variable until that variable is accessed and concatenated in the
        // AuthRegisterBillingController::store() method to create a Chargebee
        // subscription for the user based on their plan selection and billing
        // interval (monthly, annual) and credit card information (via Stripe).
        //

        session(['plan_id_prefix' => $request_data['plan_id_prefix']]);

        //
        // Use API to update user account with terms accepted
        // --------------------------------------------------------------------
        // All of the logic for updating the record in the database is located
        // in the API.
        //

        // Perform API call to update data
        $api_request = $this->patchApiRequest('/v1/admin/auth/user/accounts/'.$request->user()->id, [
            'flag_terms_accepted' => 1,
        ]);

        // If API request failed, there is likely a system error that can
        // not be handled by the user. We will create a log entry and return
        // a 500 error to the user for additional investigation.
        if($api_request->meta->result != 'success') {
            Log::debug(
                'AuthRegisterPlanController store() API request did not return success result',
                [
                    'request-data' => $request->all(),
                    'api-request' => $api_request
                ]
            );

            abort(500, 'There was an error with accepting the terms of service. Please contact the support team for assistance.');
        }

        // If free plan was selected, we will bypass the billing page and proceed
        // to the activation screen. When the user account was created, the free
        // subscription plan was automatically selected so we do not need to take
        // any additional action that would have otherwise been performed in the
        // AuthRegisterBillingController::store() method.
        if($request_data['plan_id_prefix'] == 'try1u-plan-free') {

            // Redirect the user to the dashboard page
            return redirect()->route('user.dashboard.index');

        } else {

            // Redirect the user to the next step (billing information)
            return redirect()->route('auth.register.billing.create');

        }

    }

}
