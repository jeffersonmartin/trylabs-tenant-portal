<?php

namespace App\Http\Controllers\Auth;

use App\Libraries\BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthLogoutController extends BaseController
{

    public function get(Request $request)
    {

        // Use Laravel authentication to check if user has authenticated
        if (Auth::check()) {

            // Log the user out
            Auth::logout();

            // TODO Flash error message about being successfully logged out

            return redirect()->route('auth.login.create');

        // If user has not been authenticated
        } else {

            // TODO flash error message about user not being authenticated when logging out

            // Redirect to the login form
            return redirect()->route('auth.login.create');

        }

    }

}
