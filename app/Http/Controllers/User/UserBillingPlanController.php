<?php

namespace App\Http\Controllers\User;

use App\Libraries\BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Validator;

class UserBillingPlanController extends BaseController
{

    public function upgrade(Request $request)
    {

        //
        // Input Validation using Laravel Validator
        // --------------------------------------------------------------------
        // The validation rules below are NOT related to the API.
        //

        // Validate input
        $validator = Validator::make($request->all(), [
            'plan_id' => 'required|string|max:30|starts_with:try1u'
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route('user.billing.index')
                ->withErrors($validator)
                ->withInput();
        }

        // Create array of request data that will be used in array_get below
        // and make handling null values easier than throwing exceptions
        $request_data = $request->all();

        // Get Chargebee Subscription ID from Tenant Account Table
        $tenant_account = $this->getApiRequest('/v1/admin/tenant/accounts/'.$request->user()->tenant_account_id);

        // Create array of parameters for Chargebee subscription
        $chargebee_params = [
            'redirect_url' => config('trylabs.portal.tenant.url').'/user/billing/webhook/checkout',
            'cancel_url' => config('trylabs.portal.tenant.url').'/user/billing',
            'subscription_id' => $tenant_account->data->chargebee_subscription_id,
            'plan_id' => $request_data['plan_id']
        ];

        // Determine if plan is monthly or annual to determine whether coupon is added
        $plan_term = substr($request_data['plan_id'], -2);

        if($plan_term == 'yr') {
            $chargebee_params['coupon_id'] = $request_data['plan_id'].'-disc';
        }

        // Use API to create a hosted page URL for redirecting to complete payment and checkout
        $chargebee_hosted_page = $this->postApiRequest('/v1/admin/chargebee/hosted/pages/checkout/existing', $chargebee_params);

        // TODO Save ID of hosted page to database

        // Redirect the user to the next step (billing information)
        return redirect($chargebee_hosted_page->url);

    }

    public function downgrade(Request $request)
    {

        //

    }

    public function cancel(Request $request)
    {

        // Create array of request data that will be used in array_get below
        // and make handling null values easier than throwing exceptions
        $request_data = $request->all();

        // Create BugSnag error report temporarily for capturing this data
        // TODO Create FreshDesk ticket for easier support response
        // TODO Create database entries
        $cancel_meta_data = [
            'tenant_account_id' => $request->user()->tenant_account_id,
            'reason_expensive' => array_get($request_data, 'reason_expensive', 0),
            'reason_competitor' => array_get($request_data, 'reason_competitor', 0),
            'reason_byo' => array_get($request_data, 'reason_byo', 0),
            'reason_limiteduse' => array_get($request_data, 'reason_limiteduse', 0),
            'reason_limitations' => array_get($request_data, 'reason_limitations', 0),
            'reason_bugs' => array_get($request_data, 'reason_bugs', 0),
            'reason_other' => array_get($request_data, 'reason_other', 0),
            'cancel_feedback' => array_get($request_data, 'cancel_feedback'),
        ];

        Bugsnag::notifyError('CancelFeedback', 'Cancellation Feedback from '.$request->user()->full_name, function ($report) use ($cancel_meta_data) {
                $report->setMetaData([
                    'cancel_meta_data' => array($cancel_meta_data)
                ]);
            });

        // Get Chargebee Subscription ID from Tenant Account Table
        $tenant_account = $this->getApiRequest('/v1/admin/tenant/accounts/'.$request->user()->tenant_account_id);

        // Create array of parameters for Chargebee subscription
        $chargebee_params = [
            'subscription_id' => $tenant_account->data->chargebee_subscription_id
        ];

        // Use API to create a hosted page URL for redirecting to complete payment and checkout
        $chargebee_subscription = $this->postApiRequest('/v1/admin/chargebee/subscriptions/cancel', $chargebee_params);

        // TODO Save ID of hosted page to database

        // Redirect the user to the billing dashboard
        return redirect()->route('user.billing.index');

    }

}
