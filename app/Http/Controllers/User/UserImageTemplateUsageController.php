<?php

namespace App\Http\Controllers\User;

use App\Libraries\BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;

class UserImageTemplateUsageController extends BaseController
{

    public function index($cloud_image_template_id, Request $request)
    {

        // Use API to get list of image templates for user
        $image_template = $this->getApiRequest('/v1/admin/cloud/image/templates/'.$cloud_image_template_id);

        // Use API to get list of reservations for image template
        $reservations = $this->getApiRequest('/v1/admin/tenant/reservations?filter[tenant_account_id]='.$request->user()->tenant_account_id.'&filter[cloud_image_template_id]='.$image_template->data->id.'&include=cloud-image-template,tenant-reservation-token&sort=-created_at');

        // Show the list of image templates page
        return view('user.image.templates.usage.index', compact([
            'request',
            'image_template',
            'reservations'
        ]));

    }

}
