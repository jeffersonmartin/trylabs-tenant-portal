<?php

namespace App\Http\Controllers\User;

use App\Libraries\BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;

class UserImageSettingController extends BaseController
{

    public function edit(Request $request)
    {

        // Use API to get tenant account
        $tenant_account = $this->getApiRequest('/v1/admin/tenant/accounts/'.$request->user()->tenant_account_id);

        // Use API to get list of cloud domains
        $cloud_domains = $this->getApiRequest('/v1/admin/cloud/domains');

        // Use API to get list of cloud regions
        $cloud_regions = $this->getApiRequest('/v1/admin/cloud/regions');

        // Show the image settings page
        return view('user.image.settings.edit', compact([
            'request',
            'tenant_account',
            'cloud_domains',
            'cloud_regions'
        ]));

    }

    public function update(Request $request)
    {

        //
        // Input Validation using Laravel Validator
        // --------------------------------------------------------------------
        // The validation rules below are the same ones that we have in our
        // API. If validation fails, Laravel will redirect the user back to
        // the account creation page with the error message(s).
        //

        // Validate input
        $validator = Validator::make($request->all(), [
            'cloud_domain_id_default' => 'required|uuid|exists:cloud_domains,id',
            'cloud_region_id_default' => 'required|uuid|exists:cloud_regions,id',
        ],
        [
            //'field.rule' => 'Custom message',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route('user.image.settings.edit')
                ->withErrors($validator)
                ->withInput();
        }

        //
        // Use API to save record in the database
        // --------------------------------------------------------------------
        // All of the logic for storing the record in the database is located
        // in the API.
        //

        // Create array of request data that will be used in array_get below
        // and make handling null values easier than throwing exceptions
        $request_data = $request->all();

        // Perform API call to store data
        $api_request = $this->patchApiRequest('/v1/admin/tenant/accounts/'.$request->user()->tenant_account_id, [
            'cloud_domain_id_default' => array_get($request_data, 'cloud_domain_id_default'),
            'cloud_region_id_default' => array_get($request_data, 'cloud_region_id_default'),
        ]);

        // If API request failed, there is likely a system error that can
        // not be handled by the user. We will create a log entry and return
        // a 500 error to the user for additional investigation.
        if($api_request->meta->result != 'success') {
            Log::debug(
                'UserImageSettingController update() API request did not return success result',
                [
                    'request-data' => $request->all(),
                    'api-request' => $api_request
                ]
            );

            abort(500, 'There was an error updating your image settings. Please contact the support team for assistance.');
        }

        // TODO Evaluate if any actions need to be taken with image creation

        return redirect()->route('user.image.templates.index');

    }

}
