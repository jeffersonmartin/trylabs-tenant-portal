<?php

namespace App\Http\Controllers\User;

use App\Libraries\BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;

class UserBillingCheckoutController extends BaseController
{

    public function create(Request $request)
    {

        // Show the billing page
        return view('user.billing.index', compact([
            'request'
        ]));

    }

    public function store(Request $request)
    {

        //

    }

}
