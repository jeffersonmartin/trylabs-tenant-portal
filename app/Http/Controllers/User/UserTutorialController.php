<?php

namespace App\Http\Controllers\User;

use App\Libraries\BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;

class UserTutorialController extends BaseController
{

    public function index(Request $request)
    {

        // Show the tutorial page
        return view('user.tutorial.index', compact([
            'request'
        ]));

    }

}
