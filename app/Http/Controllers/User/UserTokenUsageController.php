<?php

namespace App\Http\Controllers\User;

use App\Libraries\BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;

class UserTokenUsageController extends BaseController
{

    public function index($tenant_reservation_token_id, Request $request)
    {

        // Use API to get list of tokens for user
        $token = $this->getApiRequest('/v1/admin/tenant/reservation/tokens/'.$tenant_reservation_token_id);

        // Use API to get list of reservations for this token
        $reservations = $this->getApiRequest('/v1/admin/tenant/reservations?filter[tenant_reservation_token_id]='.$tenant_reservation_token_id.'&sort=created_at');

        // Show the usage page for the token
        return view('user.tokens.usage.index', compact([
            'request',
            'token',
            'reservations'
        ]));

    }

}
