<?php

namespace App\Http\Controllers\User;

use App\Libraries\BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;

class UserProfileController extends BaseController
{

    public function show(Request $request)
    {

        // Use API to get user account information
        $user_account = $this->getApiRequest('/v1/admin/auth/user/accounts/'.$request->user()->id);

        // Show the dashboard page
        return view('user.profile.show', compact([
            'request',
            'user_account'
        ]));

    }

    public function update(Request $request)
    {

        //
        // Input Validation using Laravel Validator
        // --------------------------------------------------------------------
        // The validation rules below are the same ones that we have in our
        // API. If validation fails, Laravel will redirect the user back to
        // the account creation page with the error message(s).
        //

        // Validate input
        $validator = Validator::make($request->all(), [
            'first_name' => 'nullable|max:30|regex:/^[a-zA-Z]+(([\',. -][a-zA-Z ])?[a-zA-Z]*)*$/',
            'last_name' => 'nullable|max:30|regex:/^[a-zA-Z]+(([\',. -][a-zA-Z ])?[a-zA-Z]*)*$/',
            'organization_name' => 'nullable|string|max:50',
        ],
        [
            //'field.rule' => 'Custom message',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route('user.profile.show')
                ->withErrors($validator)
                ->withInput();
        }

        //
        // Use API to save record in the database
        // --------------------------------------------------------------------
        // All of the logic for storing the record in the database is located
        // in the API.
        //

        // Use API to get user account with existing values
        $user_account = $this->getApiRequest('/v1/admin/auth/user/accounts/'.$request->user()->id);

        // Create array of request data that will be used in array_get below
        // and make handling null values easier than throwing exceptions
        $request_data = $request->all();

        // Perform API call to store data
        $api_request = $this->patchApiRequest('/v1/admin/auth/user/accounts/'.$request->user()->id, [
            'first_name' => array_get($request_data, 'first_name', $user_account->data->first_name),
            'last_name' => array_get($request_data, 'last_name', $user_account->data->last_name),
            'organization_name' => array_get($request_data, 'organization_name', $user_account->data->organization_name),
        ]);

        // If API request failed, there is likely a system error that can
        // not be handled by the user. We will create a log entry and return
        // a 500 error to the user for additional investigation.
        if($api_request->meta->result != 'success') {
            Log::debug(
                'UserProfileController update() API request did not return success result',
                [
                    'request-data' => $request->all(),
                    'api-request' => $api_request
                ]
            );

            abort(500, 'There was an error updating your account. Please contact the support team for assistance.');
        }

        return redirect()->route('user.profile.show');

    }

    /*
    public function changePassword(Request $request)
    {

        //
        // Input Validation using Laravel Validator
        // --------------------------------------------------------------------
        // The validation rules below are the same ones that we have in our
        // API. If validation fails, Laravel will redirect the user back to
        // the account creation page with the error message(s).
        //

        // Validate input
        $validator = Validator::make($request->all(), [
            'current_password' => 'required|min:8|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/',
            'new_password' => 'required|min:8|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/',
            'new_password_confirmation' => 'required|min:8|confirmed|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/',
        ],
        [
            //'field.rule' => 'Custom message',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route('user.profile.show')
                ->withErrors($validator)
                ->withInput();
        }

        //
        // Use API to save record in the database
        // --------------------------------------------------------------------
        // All of the logic for storing the record in the database is located
        // in the API.
        //

        // Create array of request data that will be used in array_get below
        // and make handling null values easier than throwing exceptions
        $request_data = $request->all();

        // Perform API call to store data
        $api_request = $this->patchApiRequest('/v1/admin/auth/user/accounts/'.$request->user()->id, [
            //'password' => array_get($request_data, 'new_password', $token->data->new_password),
        ]);

        // TODO Fix this method

        // If API request failed, there is likely a system error that can
        // not be handled by the user. We will create a log entry and return
        // a 500 error to the user for additional investigation.
        if($api_request->meta->result != 'success') {
            Log::debug(
                'UserTokenController store() API request did not return success result',
                [
                    'request-data' => $request->all(),
                    'api-request' => $api_request
                ]
            );

            abort(500, 'There was an error updating your token. Please contact the support team for assistance.');
        }

        // TODO Evaluate if any actions need to be taken with token creation

        return redirect()->route('user.profile.show');

    }
    */

}
