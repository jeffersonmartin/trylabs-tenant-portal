<?php

namespace App\Http\Controllers\User;

use App\Libraries\BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;

class UserBillingCallbackController extends BaseController
{

    public function webhookEvent(Request $request)
    {

        // Decode JSON request
        //

    }

    public function hostedPage(Request $request)
    {

        //
        //id=<hosted_page_id>&state=succeeded
        //?id=<hosted_page_id>&state=cancelled

        // Get Chargebee hosted page
        $hosted_page = $this->getApiRequest('/v1/admin/chargebee/hosted/pages/'.$request->id);

        // TODO if hosted page not found

        // We will get the state from the API response instead of the URL to get real-time information and avoid
        // any injection or query string modification.
        $state = $hosted_page->data->state;
        $succeeded_action = $hosted_page->data->success_action;
        $cancelled_action = $hosted_page->data->cancel_action;

        // Define array with parameters about the customer and subscription for performing actions
        $form_params = [
            'tenant_account_id' = $hosted_page->data->tenant_account_id;
            'chargebee_customer_id' = $hosted_page->data->chargebee_customer_id;
            'chargebee_subscription_id' = $hosted_page->data->chargebee_subscription_id;
            'plan_id' = $hosted_page->data->plan_id;
        ];

        // If state of hosted page is `succeeded` (set by Chargebee), the user has completed the checkout workflow
        // and we can proceed with taking action with our application, usually updating credits
        if($state == 'succeeded') {

            if($succeeded_action == 'activate_plan') {
                $this->postApiRequest('/v1/admin/chargebee/actions/plan/activate', $form_params);
            } elseif($succeeded_action == 'upgrade_plan') {
                $this->postApiRequest('/v1/admin/chargebee/actions/plan/upgrade', $form_params);
            } elseif($succeeded_action == 'downgrade_plan') {
                $this->postApiRequest('/v1/admin/chargebee/actions/plan/downgrade', $form_params);
            } elseif($succeeded_action == 'renew_plan') {
                $this->postApiRequest('/v1/admin/chargebee/actions/plan/renew', $form_params);
            } elseif($succeeded_action == 'cancel_plan') {
                $this->postApiRequest('/v1/admin/chargebee/actions/plan/cancel', $form_params);
            } elseif($succeeded_action == 'payment_failed') {
                $this->postApiRequest('/v1/admin/chargebee/actions/payment/failed', $form_params);
            }

        } elseif($state == 'cancelled') {

            if($cancelled_action == 'activate_plan') {
                $this->postApiRequest('/v1/admin/chargebee/actions/plan/activate', $form_params);
            } elseif($cancelled_action == 'upgrade_plan') {
                $this->postApiRequest('/v1/admin/chargebee/actions/plan/upgrade', $form_params);
            } elseif($cancelled_action == 'downgrade_plan') {
                $this->postApiRequest('/v1/admin/chargebee/actions/plan/downgrade', $form_params);
            } elseif($cancelled_action == 'renew_plan') {
                $this->postApiRequest('/v1/admin/chargebee/actions/plan/renew', $form_params);
            } elseif($cancelled_action == 'cancel_plan') {
                $this->postApiRequest('/v1/admin/chargebee/actions/plan/cancel', $form_params);
            } elseif($cancelled_action == 'payment_failed') {
                $this->postApiRequest('/v1/admin/chargebee/actions/payment/failed', $form_params);
            }

        }

        return redirect()->route('user.dashboard.index');

    }

}
