<?php

namespace App\Http\Controllers\User;

use App\Libraries\BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;

class UserBillingController extends BaseController
{

    public function index(Request $request)
    {

        // Use API to get tenant account
        $tenant_account = $this->getApiRequest('/v1/admin/tenant/accounts/'.$request->user()->tenant_account_id);

        // Use API to get Chargebee Customer Info
        $chargebee_customer = $this->getApiRequest('/v1/admin/chargebee/customers/'.$tenant_account->data->chargebee_customer_id);
        //dd($chargebee_customer);

        $chargebee_active_subscription = null;
        $chargebee_future_subscription = null;
        $chargebee_cancelled_subscriptions = [];

        if(isset($chargebee_customer->subscriptions)) {
            foreach($chargebee_customer->subscriptions as $subscription) {
                if($subscription->status == 'in_trial' || $subscription->status == 'active' || $subscription->status == 'non_renewing') {
                    $chargebee_active_subscription = $subscription;
                } elseif($subscription->status == 'future') {
                    $chargebee_future_subscription = $subscription;
                } elseif($subscription->status == 'cancelled') {
                    $chargebee_cancelled_subscriptions[] = $subscription;
                }
            }
        }

        // Use API to get Chargebee Invoices
        $chargebee_invoices = $this->getApiRequest('/v1/admin/chargebee/customers/'.$tenant_account->data->chargebee_customer_id.'/invoices');

        if($chargebee_customer->card_status != 'no_card') {
            // Use API to get Chargebee/Stripe Payment Method
            $chargebee_payment_method = $this->getApiRequest('/v1/admin/chargebee/payment/sources/'.$chargebee_customer->primary_payment_source_id);
        } else {
            $chargebee_payment_method = null;
        }

        // Show the billing page
        return view('user.billing.index', compact([
            'request',
            'tenant_account',
            'chargebee_customer',
            'chargebee_active_subscription',
            'chargebee_future_subscription',
            'chargebee_cancelled_subscriptions',
            'chargebee_invoices',
            'chargebee_payment_method'
        ]));

    }

    public function downloadInvoice($chargebee_customer_id, $chargebee_invoice_id, Request $request)
    {

        $chargebee_invoice = $this->getApiRequest('/v1/admin/chargebee/customers/'.$chargebee_customer_id.'/invoices/'.$chargebee_invoice_id.'/download');

        return redirect($chargebee_invoice->download_url);

    }

}
