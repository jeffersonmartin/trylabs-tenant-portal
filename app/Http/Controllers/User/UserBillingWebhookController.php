<?php

namespace App\Http\Controllers\User;

use App\Libraries\BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;

class UserBillingWebhookController extends BaseController
{

    public function checkout(Request $request)
    {

        // Get hosted page parameters
        $hosted_page_id = $request->id;
        $hosted_page_state = $request->state;

        // Use API to get hosted page details
        $hosted_page = $this->getApiRequest('/v1/admin/chargebee/hosted/pages/'.$hosted_page_id);

        // If hosted page was successful
        if($hosted_page->state == 'succeeded' && $hosted_page->type == 'checkout_existing') {

            // Define variables from hosted page response
            $customer_id = $hosted_page->content->subscription->customer_id;
            $subscription_id = $hosted_page->content->subscription->id;
            $plan_id = $hosted_page->content->subscription->plan_id;
            $billing_period_unit = $hosted_page->content->subscription->billing_period_unit;
            // TODO timestamps

            // Get tenant account
            $tenant_account = $this->getApiRequest('/v1/admin/tenant/accounts/'.$hosted_page->content->customer->meta_data->tenant_account_id);

            foreach($hosted_page->content->subscription->addons as $addon) {

                if($addon->id == 'try1u-compute-usage-cr') {

                    $compute_credit_qty = $addon->quantity;
                    $current_credit_qty = $tenant_account->data->credit_balance_qty;

                    $new_credit_qty = $compute_credit_qty + $current_credit_qty;

                    $update_tenant_account = $this->patchApiRequest('/v1/admin/tenant/accounts/'.$hosted_page->content->customer->meta_data->tenant_account_id, [
                        'credit_balance_qty' => $new_credit_qty
                    ]);

                } elseif($addon->id == 'tryu-image-template-cr') {
                    // TODO set limits for image templates
                }

            } // END foreach

            return redirect()->route('user.dashboard.index');

        } // END if succeeded

        else {
            return redirect()->route('user.billing.index');
        }

    }

}
