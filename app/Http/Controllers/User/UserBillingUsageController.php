<?php

namespace App\Http\Controllers\User;

use App\Libraries\BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;

class UserBillingUsageController extends BaseController
{

    public function index(Request $request)
    {

        // Use API to get tenant account
        $tenant_account = $this->getApiRequest('/v1/admin/tenant/accounts/'.$request->user()->tenant_account_id);

        // Use API to get list of reservations for user
        $reservations = $this->getApiRequest('/v1/admin/tenant/reservations?filter[tenant_account_id]='.$request->user()->tenant_account_id.'&include=cloud-image-template,tenant-reservation-token&sort=-created_at');

        // Use API to get list of image templates for user
        $image_templates = $this->getApiRequest('/v1/admin/cloud/image/templates?filter[tenant_account_id]='.$request->user()->tenant_account_id.'&sort=-created_at');

        //dd($chargebee_customer);

        // Show the billing page
        return view('user.billing.usage.index', compact([
            'request',
            'tenant_account',
            'reservations',
            'image_templates'
        ]));

    }

}
