<?php

namespace App\Http\Controllers\User;

use App\Libraries\BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;

class UserBillingPaymentController extends BaseController
{

    public function update(Request $request)
    {

        // Get Chargebee Subscription ID from Tenant Account Table
        $tenant_account = $this->getApiRequest('/v1/admin/tenant/accounts/'.$request->user()->tenant_account_id);

        // Create array of parameters for Chargebee subscription
        $chargebee_params = [
            'customer_id' => $tenant_account->data->chargebee_customer_id,
            'redirect_url' => config('trylabs.portal.tenant.url').'/user/billing',
            'cancel_url' => config('trylabs.portal.tenant.url').'/user/billing',
        ];

        // Use API to create a hosted page URL for redirecting to complete payment and checkout
        $chargebee_hosted_page = $this->postApiRequest('/v1/admin/chargebee/hosted/pages/payment/method', $chargebee_params);

        // TODO Save ID of hosted page to database

        // Redirect the user to the next step (billing information)
        return redirect($chargebee_hosted_page->url);

    }

}
