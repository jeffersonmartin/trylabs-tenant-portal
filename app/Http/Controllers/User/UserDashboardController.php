<?php

namespace App\Http\Controllers\User;

use App\Libraries\BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;

class UserDashboardController extends BaseController
{

    public function index(Request $request)
    {

        // Use API to get tenant account
        $tenant_account = $this->getApiRequest('/v1/admin/tenant/accounts/'.$request->user()->tenant_account_id);

        // Use API to get list of reservations for user
        $reservations = $this->getApiRequest('/v1/admin/tenant/reservations?filter[tenant_account_id]='.$request->user()->tenant_account_id.'&include=cloud-image-template&sort=-created_at');
        // TODO Fix filters for terminated_at. For now we are just not displaying
        // those rows in thet able.

        // Use API to get list of image templates for user
        // This is used in the Create Reservation modal window
        $image_templates = $this->getApiRequest('/v1/admin/cloud/image/templates?filter[tenant_account_id]='.$request->user()->tenant_account_id);

        // Use API to get list of redemptions this month
        // This is used in the Create Reservation modal window
        $reservation_tokens = $this->getApiRequest('/v1/admin/tenant/reservation/tokens?filter[tenant_account_id]='.$request->user()->tenant_account_id);

        // Show the dashboard page
        return view('user.dashboard.index', compact([
            'request',
            'tenant_account',
            'reservations',
            'image_templates',
            'reservation_tokens',
        ]));

    }

}
