<?php

namespace App\Http\Controllers\User;

use App\Libraries\BaseController;
use Illuminate\Http\Request;
use File;
use Validator;

class UserSshKeyController extends BaseController
{

    public function download(Request $request)
    {

        // Get tenant account from request
        $tenant_account = \App\Models\Tenant\TenantAccount::findOrFail($request->user()->tenant_account_id);

        // Get SSH key from tenant account details
        $cloud_ssh_key = \App\Models\Cloud\CloudSshKey::findOrFail($tenant_account->cloud_ssh_key_id_default);

        // Create file name with concatenated tenant account short ID and SSH key short ID
        $file_name = 'trylabs-ssh-t'.$tenant_account->short_id.'-k'.$cloud_ssh_key->short_id;

        $key_path = storage_path('ssh_key_pairs');
        if(!File::exists($key_path)) {
            File::makeDirectory($key_path, 0777, true, true);
        }

        file_put_contents($key_path.'/'.$file_name.'.pub', $cloud_ssh_key->public_key);
        file_put_contents($key_path.'/'.$file_name, $cloud_ssh_key->private_key);

        // Initatialize PHP class for Zip Archive (not Laravel specific)
        $zip = new \ZipArchive();

        // Open the zip archive
        $zip->open($key_path.'/'.$file_name.'.zip', \ZipArchive::CREATE | \ZipArchive::OVERWRITE);

        // Add the key files to the zip archive
        $zip->addFile($key_path.'/'.$file_name.'.pub', $file_name.'.pub');
        $zip->addFile($key_path.'/'.$file_name, $file_name);
        $zip->close();

        // Delete the key files from the server
        unlink($key_path.'/'.$file_name.'.pub');
        unlink($key_path.'/'.$file_name);

        // Download the zip file and use the Laravel helper to delete the file after it
        // has been downloaded
        return response()->download($key_path.'/'.$file_name.'.zip')->deleteFileAfterSend();

    }

}
