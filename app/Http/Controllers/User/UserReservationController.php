<?php

namespace App\Http\Controllers\User;

use App\Libraries\BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;

class UserReservationController extends BaseController
{

    public function index(Request $request)
    {

        // Use API to get list of reservations for user
        $active_reservations = $this->getApiRequest('/v1/admin/tenant/reservations?filter[status]=provisioning-queued,provisioning,active&filter[tenant_account_id]='.$request->user()->tenant_account_id.'&include=cloud-image-template&sort=-created_at');

        // Use API to get list of reservations for user
        $historical_reservations = $this->getApiRequest('/v1/admin/tenant/reservations?filter[status]=terminated&filter[tenant_account_id]='.$request->user()->tenant_account_id.'&include=cloud-image-template&sort=-terminated_at');

        // Use API to get list of image templates for user
        // This is used in the Create Reservation modal window
        $image_templates = $this->getApiRequest('/v1/admin/cloud/image/templates?filter[tenant_account_id]='.$request->user()->tenant_account_id);

        // Show the billing page
        return view('user.reservations.index', compact([
            'active_reservations',
            'historical_reservations',
            'image_templates',
            'request'
        ]));

    }

    /*
    // TODO 7/29/19 JM - There doesn't appear to be a need for this page since we redirect to the token portal.
    public function show(Request $request)
    {

        // Show the reservation page
        return view('user.reservations.show', compact([
            'request'
        ]));

    }
    */

    public function create(Request $request)
    {

        // If request does not have an image template in the query string, then
        // return the user to the list of reservations. This is set by the GET
        // form request in the modal window on the list of reservations page.
        if(!$request->has('cloud_image_template_id')) {

            // TODO Flash error message that no image template selected
            return redirect()->route('user.reservations.index');
        }

        // Use API to get list of image templates for user
        // This is used in the Create Reservation modal window
        $image_templates = $this->getApiRequest('/v1/admin/cloud/image/templates?filter[tenant_account_id]='.$request->user()->tenant_account_id);

        // Use API to get image template in query string
        $image_template = $this->getApiRequest('/v1/admin/cloud/image/templates/'.$request->cloud_image_template_id);

        // TODO Handle any 404 errors gracefully

        // Verify that image template belongs to this tenant
        if($image_template->data->tenant_account_id != $request->user()->tenant_account_id) {

            // TODO Flash error message that image template is not associated with this account
            return redirect()->route('user.reservations.index');
        }

        // Get Tenant User
        // TODO evaluate error handling if no tenant user found
        $tenant_user = $this->getApiRequest('/v1/admin/tenant/users/lookup?filter[tenant_account_id]='.$request->user()->tenant_account_id.'&filter[auth_user_account_id]='.$request->user()->id);

        // Get Reservation Category
        $tenant_reservation_category = $this->getApiRequest('/v1/admin/tenant/reservation/categories/lookup?filter[slug]=trylabs-creator');

        // Show the billing page
        return view('user.reservations.create', compact([
            'image_templates',
            'image_template',
            'tenant_user',
            'tenant_reservation_category',
            'request'
        ]));

    }

    public function store(Request $request)
    {

        //
        // Input Validation using Laravel Validator
        // --------------------------------------------------------------------
        // The validation rules below are the same ones that we have in our
        // API. If validation fails, Laravel will redirect the user back to
        // the account creation page with the error message(s).
        //

        // Validate input
        $validator = Validator::make($request->all(), [
            'tenant_account_id' => 'required|uuid',
            'tenant_user_id' => 'required|uuid',
            'tenant_reservation_category_id' => 'required|uuid',
            'cloud_image_template_id' => 'required|uuid',
            'count_mins' => 'required|integer|max:44640',
            'terminate_action' => 'required|string|starts_with:replace,save,discard'
        ],
        [
            //'field.rule' => 'Custom message',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route('user.reservations.create', ['cloud_image_template_id' => $request->cloud_image_template_id])
                ->withErrors($validator)
                ->withInput();
        }

        //
        // Use API to save record in the database
        // --------------------------------------------------------------------
        // All of the logic for storing the record in the database is located
        // in the API.
        //

        // Create array of request data that will be used in array_get below
        // and make handling null values easier than throwing exceptions
        $request_data = $request->all();

        // Perform API call to store data
        $api_request = $this->postApiRequest('/v1/admin/tenant/reservations', [
            'tenant_account_id' => array_get($request_data, 'tenant_account_id'),
            'tenant_user_id' => array_get($request_data, 'tenant_user_id'),
            'tenant_reservation_category_id' => array_get($request_data, 'tenant_reservation_category_id'),
            'cloud_image_template_id' => array_get($request_data, 'cloud_image_template_id'),
            'count_mins' => array_get($request_data, 'count_mins'),
            'terminate_action' => array_get($request_data, 'terminate_action'),
        ]);

        // If API request failed, there is likely a system error that can
        // not be handled by the user. We will create a log entry and return
        // a 500 error to the user for additional investigation.
        if($api_request->meta->result != 'success') {
            Log::debug(
                'UserReservationController store() API request did not return success result',
                [
                    'request-data' => $request->all(),
                    'api-request' => $api_request
                ]
            );

            abort(500, 'There was an error creating your reservation. Please contact the support team for assistance.');
        }

        // TODO Evaluate if any actions need to be taken

        return redirect(config('trylabs.portal.reservation.url').'/r/'.$api_request->data->short_id);

    }

    /*
    public function update($id, Request $request)
    {

        // TODO 7/29/19 JM - There doesn't appear to be any update actions, until we add the extend feature.

    }
    */

    public function terminate($tenant_reservation_id, Request $request)
    {

        //
        // Input Validation using Laravel Validator
        // --------------------------------------------------------------------
        // The validation rules below are the same ones that we have in our
        // API. If validation fails, Laravel will redirect the user back to
        // the account creation page with the error message(s).
        //

        // Validate input
        $validator = Validator::make($request->all(), [
            'terminate_action' => 'required|string|starts_with:replace,save,discard'
        ],
        [
            'terminate_action.required' => 'You need to select whether to replace the image template or save a new one.'
            //'field.rule' => 'Custom message',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route('user.reservations.index', ['tenant_reservation_id' => $tenant_reservation_id])
                ->withErrors($validator)
                ->withInput();
        }

        //
        // Use API to save record in the database
        // --------------------------------------------------------------------
        // All of the logic for storing the record in the database is located
        // in the API.
        //

        // Create array of request data that will be used in array_get below
        // and make handling null values easier than throwing exceptions
        $request_data = $request->all();

        // Perform API call to update data
        $api_request_terminate = $this->patchApiRequest('/v1/admin/tenant/reservations/'.$tenant_reservation_id.'/terminate', [
            'terminate_action' => array_get($request_data, 'terminate_action'),
        ]);

        // If API request failed, there is likely a system error that can
        // not be handled by the user. We will create a log entry and return
        // a 500 error to the user for additional investigation.
        if($api_request_terminate->meta->result != 'success') {
            Log::debug(
                'UserReservationController terminate() Update API request did not return success result',
                [
                    'request-data' => $request->all(),
                    'api-request' => $api_request_terminate
                ]
            );

            abort(500, 'There was an error updating your reservation. Please contact the support team for assistance.');
        }

        // TODO Evaluate if any actions need to be taken

        return redirect()->route('user.reservations.index');

    }

}
