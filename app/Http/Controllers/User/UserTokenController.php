<?php

namespace App\Http\Controllers\User;

use App\Libraries\BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;

class UserTokenController extends BaseController
{

    public function index(Request $request)
    {

        // Use API to get list of reservations for user
        $reservation_tokens = $this->getApiRequest('/v1/admin/tenant/reservation/tokens?filter[tenant_account_id]='.$request->user()->tenant_account_id.'&include=cloud-image-template');

        // Use API to get list of image templates for user
        // This is used in the Create Reservation modal window
        $image_templates = $this->getApiRequest('/v1/admin/cloud/image/templates?filter[tenant_account_id]='.$request->user()->tenant_account_id);

        // Show the list of reservation tokens page
        return view('user.tokens.index', compact([
            'request',
            'reservation_tokens',
            'image_templates'
        ]));

    }

    public function show($tenant_reservation_token_id, Request $request)
    {

        // Use API to get tenant reservation token
        $token = $this->getApiRequest('/v1/admin/tenant/reservation/tokens/'.$tenant_reservation_token_id);

        // Show the reservation token page
        return view('user.tokens.show', compact([
            'request',
            'token'
        ]));

    }

    public function create(Request $request)
    {

        // If request does not have an image template in the query string, then
        // return the user to the list of reservations. This is set by the GET
        // form request in the modal window on the list of reservations page.
        if(!$request->has('cloud_image_template_id')) {

            // TODO Flash error message that no image template selected
            return redirect()->route('user.tokens.index');
        }

        // Use API to get list of image templates for user
        // This is used in the Create Reservation modal window
        $image_templates = $this->getApiRequest('/v1/admin/cloud/image/templates?filter[tenant_account_id]='.$request->user()->tenant_account_id);

        // Use API to get image template in query string
        $image_template = $this->getApiRequest('/v1/admin/cloud/image/templates/'.$request->cloud_image_template_id);

        // Use API to get tenant account for user
        $tenant_account = $this->getApiRequest('/v1/admin/tenant/accounts/'.$request->user()->tenant_account_id.'?include=default-cloud-domain');

        // TODO Handle any 404 errors gracefully

        // Show the create reservation token page
        return view('user.tokens.create', compact([
            'image_template',
            'image_templates',
            'tenant_account',
            'request'
        ]));

    }

    public function store(Request $request)
    {

        //
        // Input Validation using Laravel Validator
        // --------------------------------------------------------------------
        // The validation rules below are the same ones that we have in our
        // API. If validation fails, Laravel will redirect the user back to
        // the account creation page with the error message(s).
        //

        // Validate input
        $validator = Validator::make($request->all(), [
            'cloud_image_template_id' => 'required|uuid|exists:cloud_image_templates,id',
            'name' => 'required|string|max:55',
            'external_notes' => 'required|string',
            'internal_notes' => 'nullable|string',
            'billing_responsibility' => 'required|string|starts_with:creator,consumer',
            'flag_is_time_limit_protected' => 'nullable|integer|between:0,1',
            'time_limit_minutes' => 'nullable|integer|max:720',
            'flag_is_password_protected' => 'nullable|integer|between:0,1',
            'password' => 'nullable|string|min:4|max:55',
            'flag_is_redeem_count_protected' => 'nullable|integer|between:0,1',
            'redeem_count_max' => 'nullable|integer|max:99999',
            'flag_is_rate_limit_protected' => 'nullable|integer|between:0,1',
            'rate_limit_count_max' => 'nullable|integer',
            'rate_limit_interval' => 'nullable|string|starts_with:hour,day,week,month',
        ],
        [
            //'field.rule' => 'Custom message',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route('user.tokens.create', ['cloud_image_template_id' => $request->cloud_image_template_id])
                ->withErrors($validator)
                ->withInput();
        }

        //
        // Use API to save record in the database
        // --------------------------------------------------------------------
        // All of the logic for storing the record in the database is located
        // in the API.
        //

        // Create array of request data that will be used in array_get below
        // and make handling null values easier than throwing exceptions
        $request_data = $request->all();

        // Perform API call to store data
        $api_request = $this->postApiRequest('/v1/admin/tenant/reservation/tokens', [
            'tenant_account_id' => $request->user()->tenant_account_id,
            'cloud_image_template_id' => array_get($request_data, 'cloud_image_template_id'),
            'name' => array_get($request_data, 'name'),
            'external_notes' => array_get($request_data, 'external_notes'),
            'internal_notes' => array_get($request_data, 'internal_notes'),
            'billing_responsibility' => array_get($request_data, 'billing_responsibility'),
            'flag_is_time_limit_protected' => array_get($request_data, 'flag_is_time_limit_protected'),
            'time_limit_minutes' => array_get($request_data, 'time_limit_minutes'),
            'flag_is_password_protected' => array_get($request_data, 'flag_is_password_protected'),
            'password' => array_get($request_data, 'password'),
            'flag_is_redeem_count_protected' => array_get($request_data, 'flag_is_redeem_count_protected'),
            'redeem_count_max' => array_get($request_data, 'redeem_count_max'),
            'flag_is_rate_limit_protected' => array_get($request_data, 'flag_is_rate_limit_protected'),
            'rate_limit_count_max' => array_get($request_data, 'rate_limit_count_max'),
            'rate_limit_interval' => array_get($request_data, 'rate_limit_interval'),
            'flag_is_public' => 1,
            'starts_at' => now()->toDateTimeString(),
        ]);

        // If API request failed, there is likely a system error that can
        // not be handled by the user. We will create a log entry and return
        // a 500 error to the user for additional investigation.
        if($api_request->meta->result != 'success') {
            Log::debug(
                'UserTokenController store() API request did not return success result',
                [
                    'request-data' => $request->all(),
                    'api-request' => $api_request
                ]
            );

            abort(500, 'There was an error creating your token. Please contact the support team for assistance.');
        }

        // TODO Evaluate if any actions need to be taken with token creation

        return redirect()->route('user.tokens.index');

    }

    public function update($tenant_reservation_token_id, Request $request)
    {

        // Use API to get tenant reservation token
        $token = $this->getApiRequest('/v1/admin/tenant/reservation/tokens/'.$tenant_reservation_token_id);

        //
        // Input Validation using Laravel Validator
        // --------------------------------------------------------------------
        // The validation rules below are the same ones that we have in our
        // API. If validation fails, Laravel will redirect the user back to
        // the account creation page with the error message(s).
        //

        // Validate input
        $validator = Validator::make($request->all(), [
            //'cloud_image_template_id' => 'nullable|uuid|exists:cloud_image_templates,id',
            'name' => 'nullable|string|max:55',
            'external_notes' => 'nullable|string',
            'internal_notes' => 'nullable|string',
            'billing_responsibility' => 'nullable|string|starts_with:creator,consumer',
            'flag_is_time_limit_protected' => 'nullable|integer|between:0,1',
            'time_limit_minutes' => 'nullable|integer|max:720',
            'flag_is_password_protected' => 'nullable|integer|between:0,1',
            'password' => 'nullable|string|min:4|max:55',
            'flag_is_redeem_count_protected' => 'nullable|integer|between:0,1',
            'redeem_count_max' => 'nullable|integer|max:99999',
            'flag_is_rate_limit_protected' => 'nullable|integer|between:0,1',
            'rate_limit_count_max' => 'nullable|integer',
            'rate_limit_interval' => 'nullable|string|starts_with:hour,day,week,month',
        ],
        [
            //'field.rule' => 'Custom message',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route('user.tokens.show', ['tenant_reservation_token_id' => $tenant_reservation_token_id])
                ->withErrors($validator)
                ->withInput();
        }

        //
        // Use API to save record in the database
        // --------------------------------------------------------------------
        // All of the logic for storing the record in the database is located
        // in the API.
        //

        // Create array of request data that will be used in array_get below
        // and make handling null values easier than throwing exceptions
        $request_data = $request->all();

        // Perform API call to store data
        $api_request = $this->patchApiRequest('/v1/admin/tenant/reservation/tokens/'.$tenant_reservation_token_id, [
            'name' => array_get($request_data, 'name', $token->data->name),
            'external_notes' => array_get($request_data, 'external_notes', $token->data->external_notes),
            'internal_notes' => array_get($request_data, 'internal_notes', $token->data->internal_notes),
            'billing_responsibility' => array_get($request_data, 'billing_responsibility', $token->data->billing_responsibility),
            'flag_is_time_limit_protected' => array_get($request_data, 'flag_is_time_limit_protected', $token->data->flags->flag_is_time_limit_protected),
            'time_limit_minutes' => array_get($request_data, 'time_limit_minutes', $token->data->time_limit_minutes),
            'flag_is_password_protected' => array_get($request_data, 'flag_is_password_protected', $token->data->flags->flag_is_password_protected),
            'password' => array_get($request_data, 'password', $token->data->password),
            'flag_is_redeem_count_protected' => array_get($request_data, 'flag_is_redeem_count_protected', $token->data->flags->flag_is_redeem_count_protected),
            'redeem_count_max' => array_get($request_data, 'redeem_count_max', $token->data->redeem_count_max),
            'flag_is_rate_limit_protected' => array_get($request_data, 'flag_is_rate_limit_protected', $token->data->flags->flag_is_rate_limit_protected),
            'rate_limit_count_max' => array_get($request_data, 'rate_limit_count_max', $token->data->rate_limit_count_max),
            'rate_limit_interval' => array_get($request_data, 'rate_limit_interval', $token->data->rate_limit_interval),
        ]);

        // If API request failed, there is likely a system error that can
        // not be handled by the user. We will create a log entry and return
        // a 500 error to the user for additional investigation.
        if($api_request->meta->result != 'success') {
            Log::debug(
                'UserTokenController store() API request did not return success result',
                [
                    'request-data' => $request->all(),
                    'api-request' => $api_request
                ]
            );

            abort(500, 'There was an error updating your token. Please contact the support team for assistance.');
        }

        // TODO Evaluate if any actions need to be taken with token creation

        return redirect()->route('user.tokens.index');

    }

    public function delete($id, Request $request)
    {

        //

    }



}
