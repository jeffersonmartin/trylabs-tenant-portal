<?php

namespace App\Http\Controllers\User;

use App\Libraries\BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;

class UserImageTemplateController extends BaseController
{

    public function index(Request $request)
    {

        // Use API to get list of image templates for user
        $image_templates = $this->getApiRequest('/v1/admin/cloud/image/templates?filter[tenant_account_id]='.$request->user()->tenant_account_id);

        // Show the list of image templates page
        return view('user.image.templates.index', compact([
            'request',
            'image_templates'
        ]));

    }

    public function show($cloud_image_template_id, Request $request)
    {

        // Use API to get image template
        $cloud_image_template = $this->getApiRequest('/v1/admin/cloud/image/templates/'.$cloud_image_template_id);

        // Show the ___ page
        return view('user.image.templates.show', compact([
            'request',
            'cloud_image_template'
        ]));

    }

    public function create(Request $request)
    {

        // Use API to get tenant account (with default IDs)
        $tenant_account = $this->getApiRequest('/v1/admin/tenant/accounts/'.$request->user()->tenant_account_id);

        // Use API to get tenant account defaults
        $default_cloud_domain = $this->getApiRequest('/v1/admin/cloud/domains/'.$tenant_account->data->cloud_domain_id_default);
        $default_cloud_image_size = $this->getApiRequest('/v1/admin/cloud/image/sizes/'.$tenant_account->data->cloud_image_size_id_default);
        $default_cloud_region = $this->getApiRequest('/v1/admin/cloud/regions/'.$tenant_account->data->cloud_region_id_default);

        // Use API to get cloud provider (Digital Ocean)
        $cloud_providers = $this->getApiRequest('/v1/admin/cloud/providers?filter[slug]=ocean&include=cloud-regions,cloud-image-flavors,cloud-image-sizes,cloud-regions');

        // Loop through cloud providers (single record)
        foreach($cloud_providers->data as $cloud_provider) {

            // Just stating the obvious since it might not be apparent as a loop
            // variable. If you don't like this approach, then make it better :)
            $cloud_provider = $cloud_provider;

            // Create variables for child relationships that allow these to be
            // accessed like normal Eloquent models (Ex. $cloud_image_flavor->name)
            $cloud_image_flavors = $this->getApiRequest('/v1/admin/cloud/image/flavors?filter[cloud_provider_id]='.$cloud_provider->id.'&sort=slug');
            $cloud_image_sizes = $this->getApiRequest('/v1/admin/cloud/image/sizes?filter[cloud_provider_id]='.$cloud_provider->id.'&sort=vcpu,memory,disk');
            $cloud_regions = $this->getApiRequest('/v1/admin/cloud/regions?filter[cloud_provider_id]='.$cloud_provider->id.'&sort=name');

        }

        // Show the create template page
        return view('user.image.templates.create', compact([
            'request',
            'tenant_account',
            'default_cloud_domain',
            'default_cloud_image_size',
            'default_cloud_region',
            'cloud_provider',
            'cloud_image_flavors',
            'cloud_image_sizes',
            'cloud_regions',
        ]));

    }

    public function store(Request $request)
    {

        //
        // Input Validation using Laravel Validator
        // --------------------------------------------------------------------
        // The validation rules below are the same ones that we have in our
        // API. If validation fails, Laravel will redirect the user back to
        // the account creation page with the error message(s).
        //

        // Validate input
        $validator = Validator::make($request->all(), [
            'cloud_provider_id' => 'required|uuid|exists:cloud_providers,id',
            'cloud_account_id' => 'nullable|uuid|exists:cloud_accounts,id',
            'cloud_region_id' => 'required|uuid|exists:cloud_regions,id',
            'cloud_image_size_id' => 'required|uuid|exists:cloud_image_sizes,id',
            'cloud_image_flavor_id' => 'required|uuid|exists:cloud_image_flavors,id',
            'cloud_ssh_key_id' => 'required|uuid|exists:cloud_ssh_keys,id',
            'tenant_account_id' => 'required|uuid|exists:tenant_accounts,id',
            'name' => 'required|string|max:55',
            'version' => 'nullable|string|max:55',
            'description' => 'nullable|string',
        ],
        [
            //'field.rule' => 'Custom message',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route('user.image.templates.create')
                ->withErrors($validator)
                ->withInput();
        }

        //
        // Use API to save record in the database
        // --------------------------------------------------------------------
        // All of the logic for storing the record in the database is located
        // in the API.
        //

        // Create array of request data that will be used in array_get below
        // and make handling null values easier than throwing exceptions
        $request_data = $request->all();

        // Use API to get image flavor for determining api_snapshot_id below
        $cloud_image_flavor = $this->getApiRequest('/v1/admin/cloud/image/flavors/'.$request_data['cloud_image_flavor_id']);

        // Perform API call to store data
        $api_request = $this->postApiRequest('/v1/admin/cloud/image/templates', [
            'cloud_provider_id' => array_get($request_data, 'cloud_provider_id'),
            'cloud_account_id' => array_get($request_data, 'cloud_account_id'),
            'cloud_region_id' => array_get($request_data, 'cloud_region_id'),
            'cloud_image_size_id' => array_get($request_data, 'cloud_image_size_id'),
            'cloud_image_flavor_id' => array_get($request_data, 'cloud_image_flavor_id'),
            'cloud_ssh_key_id' => array_get($request_data, 'cloud_ssh_key_id'),
            'tenant_account_id' => array_get($request_data, 'tenant_account_id'),
            'api_snapshot_id' => $cloud_image_flavor->data->api_flavor_id,
            'name' => array_get($request_data, 'name'),
            'version' => array_get($request_data, 'version'),
            'description' => array_get($request_data, 'description'),
            'flag_is_snapshot' => 0,
            'flag_is_active' => 1,
            'flag_is_public' => 0,
            'flag_is_restricted_to_account' => 1,
            'status' => 'active',
        ]);

        // If API request failed, there is likely a system error that can
        // not be handled by the user. We will create a log entry and return
        // a 500 error to the user for additional investigation.
        if($api_request->meta->result != 'success') {
            Log::debug(
                'UserImageTemplateController store() API request did not return success result',
                [
                    'request-data' => $request->all(),
                    'api-request' => $api_request
                ]
            );

            abort(500, 'There was an error creating your image template. Please contact the support team for assistance.');
        }

        // TODO Evaluate if any actions need to be taken with image creation

        return redirect()->route('user.image.templates.show', ['image_template_id' => $api_request->data->id]);

    }

    public function edit($cloud_image_template_id, Request $request)
    {

        // Use API to get image template
        $cloud_image_template = $this->getApiRequest('/v1/admin/cloud/image/templates/'.$cloud_image_template_id);

        // Show the ___ page
        return view('user.image.templates.edit', compact([
            'request',
            'cloud_image_template'
        ]));

    }

    public function update($cloud_image_template_id, Request $request)
    {

        //
        // Input Validation using Laravel Validator
        // --------------------------------------------------------------------
        // The validation rules below are the same ones that we have in our
        // API. If validation fails, Laravel will redirect the user back to
        // the account creation page with the error message(s).
        //

        // Validate input
        $validator = Validator::make($request->all(), [
            'name' => 'nullable|string|max:55',
            'version' => 'nullable|string|max:55',
            'description' => 'nullable|string',
        ],
        [
            //'field.rule' => 'Custom message',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route('user.image.templates.show')
                ->withErrors($validator)
                ->withInput();
        }

        //
        // Use API to save record in the database
        // --------------------------------------------------------------------
        // All of the logic for storing the record in the database is located
        // in the API.
        //

        // Create array of request data that will be used in array_get below
        // and make handling null values easier than throwing exceptions
        $request_data = $request->all();

        // Perform API call to store data
        $api_request = $this->patchApiRequest('/v1/admin/cloud/image/templates/'.$cloud_image_template_id, [
            'name' => array_get($request_data, 'name'),
            'version' => array_get($request_data, 'version'),
            'description' => array_get($request_data, 'description'),
        ]);

        // If API request failed, there is likely a system error that can
        // not be handled by the user. We will create a log entry and return
        // a 500 error to the user for additional investigation.
        if($api_request->meta->result != 'success') {
            Log::debug(
                'UserImageTemplateController update() API request did not return success result',
                [
                    'request-data' => $request->all(),
                    'api-request' => $api_request
                ]
            );

            abort(500, 'There was an error updating your image template. Please contact the support team for assistance.');
        }

        // TODO Evaluate if any actions need to be taken with image creation

        return redirect()->route('user.image.templates.index');

    }

    public function delete($cloud_image_template_id, Request $request)
    {

        // Perform API call to store data
        $api_request = $this->deleteApiRequest('/v1/admin/cloud/image/templates/'.$cloud_image_template_id);

        return redirect()->route('user.image.templates.index');

    }



}
