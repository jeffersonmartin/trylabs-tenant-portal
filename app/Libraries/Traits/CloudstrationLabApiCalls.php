<?php

namespace App\Libraries\Traits;

use GuzzleHttp\Client;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use RuntimeException;

trait CloudstrationLabApiCalls
{

    /**
     *   GET Request for API
     *
     *   @param  string $uri        The URI of the request, not including the
     *                              base URI in config/cloudstration-api.php
     *                              Ex. `/v1/admin/cloud/instances`
     *                              Ex. `/v1/admin/cloud/instances/a1b2c3..`
     *
     *   @param  string $api_token  (optional) The API token of the account to
     *                              use for this request. This is only used in
     *                              rare cases where you do not want to use the
     *                              API key set in the .env file.
     *
     *   @return object             The data will be returned as a PHP object in
     *                              JSON:API format. The json_decode has already
     *                              been performed so you can access the data
     *                              using object notation. Remember that the
     *                              JSON:API format specifies that attributes
     *                              are returned in the `data` array. You can
     *                              access meta data in the `meta` array.
     *                              Ex. $request->data->column_name.
     *                              Ex. foreach($request->data as $request_row)
     */
    public function getApiRequest($uri, $api_token = null)
    {

        // Initialize the Guzzle API Client with the Base URI that has been
        // defined in config/cloudstration-api.php
        $client = new Client([
            'base_uri' => config('trylabs.api.url')
        ]);

        // Create an array of headers that will be passed with the HTTP request
        // when accessing the REST API.
        $headers = [
            // TODO Authorization Bearer Token
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
            'stream' => 'false'
        ];

        // If performing API call debugging, you can set the `.env` variable for
        // `CS_API_LABS_DEBUG` to `TRUE` and the API call will be outputted at
        // the top of the browser when you perform a request.
        if(config('trylabs.api.debug') == true) {
            try {
                $request = $client->request('GET', $uri, [
                    'debug' => true,
                    'headers' => $headers,
                ]);
            } catch (\GuzzleHttp\Exception\ClientException $exception) {
                Bugsnag::notifyException($exception);
                abort(500);
            }

        // If API debug is not enabled, send an API call using the URI specified
        // in the method parameters and the header variables that we defined in
        // the array above.
        } else {
            try {
                $request = $client->request('GET', $uri, [
                    'headers' => $headers,
                ]);
            } catch (\GuzzleHttp\Exception\ClientException $exception) {
                Bugsnag::notifyException($exception);
                abort(500);
            }
        }

        // Use the Guzzle getBody() helper method to get the response body of the
        // API call. We perform the json_decode which will convert this into a
        // PHP object so that you can access it similar to Eloquent model syntax.
        // The false parameter is for disabling the streaming mode with HTTP API
        // calls (just leave this be).
        $response_body = json_decode($request->getBody(), false);

        // If HTTP Status code is 200, then we received a successful request and
        // we will return the response body.
        if($request->getStatusCode() == 200) {

            return $response_body;

        // If we did not get a successful HTTP status code, we will abort and
        // show the error that we received.
        } else {

            Bugsnag::notifyError('Get API Failed', $uri, function ($report) {
                $report->setSeverity('error');
                $report->setMetaData([
                    'response_body' => $response_body
                ]);
            });

            // TODO Implement better exception handling
            abort($response_body->meta->status, $response_body->meta->message);

        }

    }

    /**
     *   POST (Store) Request for API
     *
     *   @param  string $uri        The URI of the request, not including the
     *                              base URI in config/cloudstration-api.php
     *                              Ex. `/v1/admin/cloud/instances`
     *
     *   @param array $request_data A column['value'] array of each of the form
     *                              fields that should be saved to the database.
     *                              The allowed (fillable) fields and validation
     *                              rules can be found in the [labs-api] repo in
     *                              the `~/app/Http/Controllers/Api/V1/...` file
     *                              that corresponds to the URI you're using.
     *
     *   @param  string $api_token  (optional) The API token of the account to
     *                              use for this request. This is only used in
     *                              rare cases where you do not want to use the
     *                              API key set in the .env file.
     *
     *   @return object             The data will be returned as a PHP object in
     *                              JSON:API format. The json_decode has already
     *                              been performed so you can access the data
     *                              using object notation. Remember that the
     *                              JSON:API format specifies that attributes
     *                              are returned in the `data` array. You can
     *                              access meta data in the `meta` array.
     *                              Ex. $request->data->column_name.
     *
     *                              If any errors are returned, they will be in
     *                              the `errors` array.
     */
    protected function postApiRequest($uri, $request_data, $api_token = null)
    {

        // Initialize the Guzzle API Client with the Base URI that has been
        // defined in config/cloudstration-api.php
        $client = new Client([
            'base_uri' => config('trylabs.api.url')
        ]);

        // Create an array of headers that will be passed with the HTTP request
        // when accessing the REST API.
        $headers = [
            // TODO Authorization Bearer Token
            'Content-Type' => 'application/x-www-form-urlencoded',
            'Accept' => 'application/json',
            'stream' => 'false'
        ];

        // If performing API call debugging, you can set the `.env` variable for
        // `CS_API_LABS_DEBUG` to `TRUE` and the API call will be outputted at
        // the top of the browser when you perform a request.
        if(config('trylabs.api.debug') == true) {

            try {
                $request = $client->request('POST', $uri, [
                    'debug' => true,
                    'headers' => $headers,
                    'form_params' => $request_data
                ]);
            } catch (\GuzzleHttp\Exception\ClientException $exception) {
                Bugsnag::notifyException($exception);
                abort(500);
            }

        // If API debug is not enabled, send an API call using the URI specified
        // in the method parameters and the header variables that we defined in
        // the array above.
        } else {

            try {
                $request = $client->request('POST', $uri, [
                    'headers' => $headers,
                    'form_params' => $request_data
                ]);
            } catch (\GuzzleHttp\Exception\ClientException $exception) {
                Bugsnag::notifyException($exception);
                abort(500);
            }
        }

        // Use the Guzzle getBody() helper method to get the response body of the
        // API call. We perform the json_decode which will convert this into a
        // PHP object so that you can access it similar to Eloquent model syntax.
        // The false parameter is for disabling the streaming mode with HTTP API
        // calls (just leave this be).
        $response_body = json_decode($request->getBody(), false);

        // If HTTP Status code is 200 or 201, then we received a successful
        // request and we will return the response body.
        if($request->getStatusCode() == 200 || $request->getStatusCode() == 201) {

            return $response_body;

        // If we did not get a successful HTTP status code, we will abort and
        // show the error that we received.
        } else {

            Bugsnag::notifyError('Post API Failed', $uri, function ($report) {
                $report->setSeverity('error');
                $report->setMetaData([
                    'request_data' => $request_data,
                    'response_body' => $response_body
                ]);
            });

            // TODO if 400 response, okay to return form data. Error out on 404 or 500.
            // TODO Implement better exception handling
            abort($response_body->meta->status, $response_body->meta->message);

        }

    }

    /**
     *   PATCH (Update) Request for API
     *
     *   @param  string $uri        The URI of the request, not including the
     *                              base URI in config/cloudstration-api.php
     *                              Ex. `/v1/admin/cloud/instances/a1b2c3...`
     *
     *   @param array $request_data A column['value'] array of each of the form
     *                              fields that should be saved to the database.
     *                              The allowed (fillable) fields and validation
     *                              rules can be found in the [labs-api] repo in
     *                              the `~/app/Http/Controllers/Api/V1/...` file
     *                              that corresponds to the URI you're using.
     *
     *   @param  string $api_token  (optional) The API token of the account to
     *                              use for this request. This is only used in
     *                              rare cases where you do not want to use the
     *                              API key set in the .env file.
     *
     *   @return object             The data will be returned as a PHP object in
     *                              JSON:API format. The json_decode has already
     *                              been performed so you can access the data
     *                              using object notation. Remember that the
     *                              JSON:API format specifies that attributes
     *                              are returned in the `data` array. You can
     *                              access meta data in the `meta` array.
     *                              Ex. $request->data->column_name.
     *
     *                              If any errors are returned, they will be in
     *                              the `errors` array.
     */
    protected function patchApiRequest($uri, $request_data, $api_token = null)
    {

        // Initialize the Guzzle API Client with the Base URI that has been
        // defined in config/cloudstration-api.php
        $client = new Client([
            'base_uri' => config('trylabs.api.url')
        ]);

        // Create an array of headers that will be passed with the HTTP request
        // when accessing the REST API.
        $headers = [
            // Authorization Bearer Token
            'Content-Type' => 'application/x-www-form-urlencoded',
            'Accept' => 'application/json',
            'stream' => 'false'
        ];

        // If performing API call debugging, you can set the `.env` variable for
        // `CS_API_LABS_DEBUG` to `TRUE` and the API call will be outputted at
        // the top of the browser when you perform a request.
        if(config('trylabs.api.debug') == true) {

            try {
                $request = $client->request('PATCH', $uri, [
                    'debug' => true,
                    'headers' => $headers,
                    'form_params' => $request_data
                ]);
            } catch (\GuzzleHttp\Exception\ClientException $exception) {
                Bugsnag::notifyException($exception);
                abort(500);
            }

        // If API debug is not enabled, send an API call using the URI specified
        // in the method parameters and the header variables that we defined in
        // the array above.
        } else {

            try {
                $request = $client->request('PATCH', $uri, [
                    'headers' => $headers,
                    'form_params' => $request_data
                ]);
            } catch (\GuzzleHttp\Exception\ClientException $exception) {
                Bugsnag::notifyException($exception);
                abort(500);
            }

        }

        // Use the Guzzle getBody() helper method to get the response body of the
        // API call. We perform the json_decode which will convert this into a
        // PHP object so that you can access it similar to Eloquent model syntax.
        // The false parameter is for disabling the streaming mode with HTTP API
        // calls (just leave this be).
        $response_body = json_decode($request->getBody(), false);

        // If HTTP Status code is 200 or 201, then we received a successful
        // request and we will return the response body.
        if($request->getStatusCode() == 200) {

            return $response_body;

        // If we did not get a successful HTTP status code, we will abort and
        // show the error that we received.
        } else {

            Bugsnag::notifyError('Patch API Failed', $uri, function ($report) {
                $report->setSeverity('error');
                $report->setMetaData([
                    'request_data' => $request_data,
                    'response_body' => $response_body
                ]);
            });

            // TODO if 400 response, okay to return form data. Error out on 404 or 500.
            // TODO Implement better exception handling
            abort($response_body->meta->status, $response_body->meta->message);

        }

    }

    protected function deleteApiRequest($uri, $api_token = null)
    {

        // Initialize the Guzzle API Client with the Base URI that has been
        // defined in config/cloudstration-api.php
        $client = new Client([
            'base_uri' => config('trylabs.api.url')
        ]);

        // Create an array of headers that will be passed with the HTTP request
        // when accessing the REST API.
        $headers = [
            // Authorization Bearer Token
            'Content-Type' => 'application/x-www-form-urlencoded',
            'Accept' => 'application/json',
            'stream' => 'false'
        ];

        // If performing API call debugging, you can set the `.env` variable for
        // `CS_API_LABS_DEBUG` to `TRUE` and the API call will be outputted at
        // the top of the browser when you perform a request.
        if(config('trylabs.api.debug') == true) {
            try {
                $request = $client->request('DELETE', $uri, [
                    'debug' => true,
                    'headers' => $headers,
                ]);
            } catch (\GuzzleHttp\Exception\ClientException $exception) {
                Bugsnag::notifyException($exception);
                abort(500);
            }

        // If API debug is not enabled, send an API call using the URI specified
        // in the method parameters and the header variables that we defined in
        // the array above.
        } else {
            try {
                $request = $client->request('DELETE', $uri, [
                    'headers' => $headers,
                ]);
            } catch (\GuzzleHttp\Exception\ClientException $exception) {
                Bugsnag::notifyException($exception);
                abort(500);
            }
        }

        // Use the Guzzle getBody() helper method to get the response body of the
        // API call. We perform the json_decode which will convert this into a
        // PHP object so that you can access it similar to Eloquent model syntax.
        // The false parameter is for disabling the streaming mode with HTTP API
        // calls (just leave this be).
        $response_body = json_decode($request->getBody(), false);

        // If HTTP Status code is 200 or 201, then we received a successful
        // request and we will return the response body.
        if($request->getStatusCode() == 200 || $request->getStatusCode() == 204) {

            return $response_body;

        // If we did not get a successful HTTP status code, we will abort and
        // show the error that we received.
        } else {

            Bugsnag::notifyError('Delete API Failed', $uri, function ($report) {
                $report->setSeverity('error');
                $report->setMetaData([
                    'response_body' => $response_body
                ]);
            });

            // TODO if 400 response, okay to return form data. Error out on 404 or 500.
            // TODO Implement better exception handling
            abort($response_body->meta->status, $response_body->meta->message);

        }

    }

}
