<?php

namespace App\Models\Tenant;

use App\Libraries;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Auth as AuthModels;
use App\Models\Cloud as CloudModels;

class TenantAccount extends Libraries\BaseModel
{
    use SoftDeletes;

    public $table = 'tenant_accounts';

    //
    // Date Fields
    // ------------------------------------------------------------------------
    // Each of the date fields below are automatically formatted as datetime
    // and have the added benefit of being Carbon date objects so you do not
    // need to Carbon::parse($value), and can use $record->created_at->addDay()
    //
    // The created_at, updated_at, deleted_at fields should be in the array by
    // default. If there are additional date columns, add them to the array.
    //

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    //
    // Fillable Fields (for Creating/Updating records)
    // ------------------------------------------------------------------------
    // The array below specifies the column name of the fields that are allowed
    // to be included in a POST (store) or PATCH (update) request. This array
    // is used in the Service class for this model in App\Services.
    //

    public $fillable = [
        'tenant_category_id',
        'auth_user_account_id',
        'cloud_domain_id_default',
        'cloud_image_size_id_default',
        'cloud_region_id_default',
        'cloud_ssh_key_id_default',
        'chargebee_customer_id',
        'chargebee_plan_id',
        'chargebee_subscription_id',
        'name',
        'slug',
        'user_support_email',
        'custom_fqdn',
        'status'
    ];

    //
    // Casts (Format for Returned Values)
    // ------------------------------------------------------------------------
    // The array below specifies how the database values will be formatted when
    // returned in Eloquent model, Resource class or JSON response.
    //

    protected $casts = [
        'id' => 'string',
        'tenant_category_id' => 'string',
        'auth_user_account_id' => 'string',
        'cloud_domain_id_default' => 'string',
        'cloud_image_size_id_default' => 'string',
        'cloud_region_id_default' => 'string',
        'cloud_ssh_key_id_default' => 'string',
        'chargebee_customer_id' => 'string',
        'chargebee_plan_id' => 'string',
        'chargebee_subscription_id' => 'string',
        'name' => 'string',
        'slug' => 'string',
        'user_support_email' => 'string',
        'custom_fqdn' => 'string',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
        'deleted_at' => 'datetime',
        'status' => 'string'
    ];

    //
    // Relationships
    // ------------------------------------------------------------------------
    // To define a relationship, create a public function with the name of the
    // table that you are defining a foreign relationship for. For a parent
    // relationship, use a singular table name (`package_widget`). For a child
    // or many-to-many relationship, use a plural name (`package_widgets`). You
    // can then copy and paste the respective return statement in.
    //

    //
    // Parent Relationships
    // ------------------------------------------------------------------------
    // return $this->belongsTo(ForeignModel::class, 'foreign_table_id');
    //

    /*
    public function tenantCategory() {
        return $this->belongsTo(TenantCategory::class, 'tenant_category_id');
    }
    */

    public function authUserAccount() {
        return $this->belongsTo(AuthModels\AuthUserAccount::class, 'auth_user_account_id');
    }

    /*
    public function defaultCloudDomain() {
        return $this->belongsTo(CloudModels\CloudDomain::class, 'cloud_domain_id_default');
    }

    public function defaultCloudImageSize() {
        return $this->belongsTo(CloudModels\CloudImageSize::class, 'cloud_image_size_id_default');
    }

    public function defaultCloudRegion() {
        return $this->belongsTo(CloudModels\CloudRegion::class, 'cloud_region_id_default');
    }
    */

    public function defaultCloudSshKey() {
        return $this->belongsTo(CloudModels\CloudSshKey::class, 'cloud_ssh_key_id_default');
    }

    //
    // Child Relationships
    // ------------------------------------------------------------------------
    // return $this->hasMany(ForeignModel::class, 'this_table_id');
    //

    /*
    public function tenantGroups() {
        return $this->hasMany(TenantGroup::class, 'tenant_account_id');
    }

    public function tenantNotes() {
        return $this->hasMany(TenantNote::class, 'tenant_account_id');
    }

    public function tenantReservationCategories() {
        return $this->hasMany(TenantReservationCategory::class, 'tenant_account_id');
    }

    public function tenantReservationTokens() {
        return $this->hasMany(TenantReservationToken::class, 'tenant_account_id');
    }

    public function tenantReservations() {
        return $this->hasMany(TenantReservation::class, 'tenant_account_id');
    }

    public function tenantUsers() {
        return $this->hasMany(TenantUser::class, 'tenant_account_id');
    }

    public function cloudAccounts() {
        return $this->hasMany(CloudModels\CloudAccount::class, 'tenant_account_id');
    }

    public function cloudImageSchedules() {
        return $this->hasMany(CloudModels\CloudImageSchedule::class, 'tenant_account_id');
    }

    public function cloudImageTemplates() {
        return $this->hasMany(CloudModels\CloudImageTemplate::class, 'tenant_account_id');
    }

    public function cloudInstanceCycles() {
        return $this->hasMany(CloudModels\CloudInstanceCycle::class, 'tenant_account_id');
    }

    public function cloudInstances() {
        return $this->hasMany(CloudModels\CloudInstance::class, 'tenant_account_id');
    }
    */

    public function cloudSshKeys() {
        return $this->hasMany(CloudModels\CloudSshKey::class, 'tenant_account_id');
    }

    //
    // Distant Child Relationships (Has Many Through)
    // ------------------------------------------------------------------------
    // return $this->hasManyThrough(
    //      DistantModel::class, IntermediateModel::class,
    //      'intermediate_key_id', 'distant_key_id', 'id'
    //      );
    //

    //
    // Many-to-Many Relationships
    // ------------------------------------------------------------------------
    // return $this->belongsToMany(ForeignModel::class, 'package_this_foreign_table', 'this_table_id', 'foreign_table_id');
    //

    //
    // Custom Attribute Setters
    // ------------------------------------------------------------------------
    // When storing attributes in the database, these methods add additional
    // business logic to be invoked when a field is stored or updated.
    //

}
