<?php

namespace App\Models\Auth;

use App\Libraries;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\Access\Authorizable;
//use App\Models\Tenant as TenantModels;

class AuthUserAccount extends Libraries\BaseModel implements
    AuthenticatableContract,
    AuthorizableContract,
    CanResetPasswordContract
{
    use Authenticatable;
    use Authorizable;
    use CanResetPassword;
    use SoftDeletes;

    protected $table = 'auth_user_accounts';

    protected $hidden = [
        'password',
        'remember_token'
    ];

    //
    // Date Fields
    // ------------------------------------------------------------------------
    // Each of the date fields below are automatically formatted as datetime
    // and have the added benefit of being Carbon date objects so you do not
    // need to Carbon::parse($value), and can use $record->created_at->addDay()
    //
    // The created_at, updated_at, deleted_at fields should be in the array by
    // default. If there are additional date columns, add them to the array.
    //

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
        'expired_at',
        'locked_at',
        'verified_at',
        'terms_accepted_at',
        'privacy_accepted_at',
        'password_changed_at',
        'last_logged_in_at',
        'last_failed_login_at',
    ];

    //
    // Fillable Fields (for Creating/Updating records)
    // ------------------------------------------------------------------------
    // The array below specifies the column name of the fields that are allowed
    // to be included in a POST (store) or PATCH (update) request. This array
    // is used in the Service class for this model in App\Services.
    //

    public $fillable = [
        'tenant_account_id',
        'first_name',
        'last_name',
        'full_name',
        'job_title',
        'organization_name',
        'email',
        'email_recovery',
        'password',
        'remember_token',
        'authy_id',
        'phone_number',
        'phone_country_code',
        'count_current_failed_logins',
        'flag_passwordless',
        'flag_2fa_enabled',
        'flag_must_change_password',
        'flag_account_expired',
        'flag_account_locked',
        'flag_account_verified',
        'flag_terms_accepted',
        'flag_privacy_accepted',
        'expires_at',
        'locked_at',
        'verified_at',
        'terms_accepted_at',
        'privacy_accepted_at',
        'password_changed_at',
        'last_successful_login_at',
        'last_failed_login_at',
        'status'
    ];

    //
    // Casts (Format for Returned Values)
    // ------------------------------------------------------------------------
    // The array below specifies how the database values will be formatted when
    // returned in Eloquent model, Resource class or JSON response.
    //

    protected $casts = [
        'id' => 'string',
        'tenant_account_id' => 'string',
        'first_name' => 'string',
        'last_name' => 'string',
        'full_name' => 'string',
        'job_title' => 'string',
        'organization_name' => 'string',
        'email' => 'string',
        'email_recovery' => 'string',
        'password' => 'string',
        'remember_token' => 'string',
        'authy_id' => 'string',
        'phone_number' => 'integer',
        'phone_country_code' => 'integer',
        'count_current_failed_logins' => 'integer',
        'flag_passwordless' => 'integer',
        'flag_2fa_enabled' => 'integer',
        'flag_must_change_password' => 'integer',
        'flag_account_expired' => 'integer',
        'flag_account_locked' => 'integer',
        'flag_account_verified' => 'integer',
        'flag_terms_accepted' => 'integer',
        'flag_privacy_accepted' => 'integer',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
        'deleted_at' => 'datetime',
        'expires_at' => 'datetime',
        'locked_at' => 'datetime',
        'verified_at' => 'datetime',
        'terms_accepted_at' => 'datetime',
        'privacy_accepted_at' => 'datetime',
        'password_changed_at' => 'datetime',
        'last_successful_login_at' => 'datetime',
        'last_failed_login_at' => 'datetime',
        'status' => 'string'
    ];

    /*
    //
    // Relationships
    // ------------------------------------------------------------------------
    // To define a relationship, create a public function with the name of the
    // table that you are defining a foreign relationship for. For a parent
    // relationship, use a singular table name (`package_widget`). For a child
    // or many-to-many relationship, use a plural name (`package_widgets`). You
    // can then copy and paste the respective return statement in.
    //

    //
    // Parent Relationships
    // ------------------------------------------------------------------------
    // return $this->belongsTo(ForeignModel::class, 'foreign_table_id');
    //

    public function tenantAccount() {
        return $this->belongsTo(TenantModels\TenantAccount::class, 'tenant_account_id');
    }

    //
    // Child Relationships
    // ------------------------------------------------------------------------
    // return $this->hasMany(ForeignModel::class, 'this_table_id');
    //

    // TODO many-to-many
    public function authRoles() {
        return $this->hasMany(AuthRole::class, 'auth_user_account_id');
    }

    // TODO distant child
    public function authPermissions() {
        return $this->hasMany(AuthPermission::class, 'auth_user_account_id');
    }

    public function authUserDevices() {
        return $this->hasMany(AuthUserDevice::class, 'auth_user_account_id');
    }

    public function authUserNotes() {
        return $this->hasMany(AuthUserNote::class, 'auth_user_account_id');
    }

    public function tenantUsers() {
        return $this->hasMany(TenantModels\TenantUser::class, 'auth_user_account_id');
    }

    //
    // Distant Child Relationships (Has Many Through)
    // ------------------------------------------------------------------------
    // return $this->hasManyThrough(
    //      DistantModel::class, IntermediateModel::class,
    //      'intermediate_key_id', 'distant_key_id', 'id'
    //      );
    //

    public function tenantReservations() {
        return $this->hasManyThrough(
            TenantModels\TenantReservation::class, TenantModels\TenantUser::class,
            'auth_user_account_id', 'tenant_user_id', 'id'
        );
    }

    //
    // Many-to-Many Relationships
    // ------------------------------------------------------------------------
    // return $this->belongsToMany(ForeignModel::class, 'package_this_foreign_table', 'this_table_id', 'foreign_table_id');
    //

    //
    // Custom Attribute Setters
    // ------------------------------------------------------------------------
    // When storing attributes in the database, these methods add additional
    // business logic to be invoked when a field is stored or updated.
    //

    public static function boot()
    {
        parent::boot();

        self::creating(function($model){
            //$model->full_name = 'bah';
            $model->full_name = $model->first_name.' '.$model->last_name;
        });

        self::created(function($model){

        });

        self::updating(function($model){
            // ... code here
            $model->full_name = $model->first_name.' '.$model->last_name;
        });

        self::updated(function($model){
            // ... code here
        });

        self::deleting(function($model){
            // ... code here
        });

        self::deleted(function($model){
            // ... code here
        });
    }

    public function setFirstNameAttribute($value)
    {
        $this->attributes['first_name'] = $value;
    }

    public function setLastNameAttribute($value)
    {
        $this->attributes['last_name'] = $value;
    }

    public function setPasswordAttribute($value)
    {
        if($this->password != $value) {

            $this->attributes['password'] = $value;
            $this->attributes['password_changed_at'] = now();

        }
    }

    public function setFlagAccountExpiredAttribute($value)
    {
        // If value is different than current value
        if($this->flag_account_expired != $value) {

            // Update flag and date values
            if($value == 1) {
                $this->attributes['flag_account_expired'] = 1;
                $this->attributes['expired_at'] = now();
            } elseif($value == 0) {
                $this->attributes['flag_account_expired'] = 0;
                $this->attributes['expired_at'] = null;
            }

        }
    }

    public function setFlagAccountLockedAttribute($value)
    {
        // If value is different than current value
        if($this->flag_account_locked != $value) {

            // Update flag and date values
            if($value == 1) {
                $this->attributes['flag_account_locked'] = 1;
                $this->attributes['locked_at'] = now();
            } elseif($value == 0) {
                $this->attributes['flag_account_locked'] = 0;
                $this->attributes['locked_at'] = null;
            }

        }
    }

    public function setFlagAccountVerifiedAttribute($value)
    {
        // If value is different than current value
        if($this->flag_account_verified != $value) {

            // Update flag and date values
            if($value == 1) {
                $this->attributes['flag_account_verified'] = 1;
                $this->attributes['verified_at'] = now();
            } elseif($value == 0) {
                $this->attributes['flag_account_verified'] = 0;
                $this->attributes['verified_at'] = null;
            }

        }
    }

    public function setFlagTermsAcceptedAttribute($value)
    {
        // If value is different than current value
        if($this->flag_terms_accepted != $value) {

            // Update flag and date values
            if($value == 1) {
                $this->attributes['flag_terms_accepted'] = 1;
                $this->attributes['terms_accepted_at'] = now();
            } elseif($value == 0) {
                $this->attributes['flag_terms_accepted'] = 0;
                $this->attributes['terms_accepted_at'] = null;
            }

        }
    }

    public function setFlagPrivacyAcceptedAttribute($value)
    {
        // If value is different than current value
        if($this->flag_privacy_accepted != $value) {

            // Update flag and date values
            if($value == 1) {
                $this->attributes['flag_privacy_accepted'] = 1;
                $this->attributes['privacy_accepted_at'] = now();
            } elseif($value == 0) {
                $this->attributes['flag_privacy_accepted'] = 0;
                $this->attributes['privacy_accepted_at'] = null;
            }

        }
    }

    */

}
