<?php

namespace App\Models\Cloud;

use App\Libraries;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Tenant as TenantModels;

class CloudSshKey extends Libraries\BaseModel
{
    use SoftDeletes;

    public $table = 'cloud_ssh_keys';

    protected $hidden = [
        'public_key',
        'private_key'
    ];

    //
    // Date Fields
    // ------------------------------------------------------------------------
    // Each of the date fields below are automatically formatted as datetime
    // and have the added benefit of being Carbon date objects so you do not
    // need to Carbon::parse($value), and can use $record->created_at->addDay()
    //
    // The created_at, updated_at, deleted_at fields should be in the array by
    // default. If there are additional date columns, add them to the array.
    //

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    //
    // Fillable Fields (for Creating/Updating records)
    // ------------------------------------------------------------------------
    // The array below specifies the column name of the fields that are allowed
    // to be included in a POST (store) or PATCH (update) request. This array
    // is used in the Service class for this model in App\Services.
    //

    public $fillable = [
        'cloud_provider_id',
        'cloud_account_id',
        'tenant_account_id',
        'api_key_id',
        'name',
        'description',
        'fingerprint',
        'public_key',
        'private_key',
        'flag_is_default',
        'provisioned_at',
        'destroyed_at',
        'status'
    ];

    //
    // Casts (Format for Returned Values)
    // ------------------------------------------------------------------------
    // The array below specifies how the database values will be formatted when
    // returned in Eloquent model, Resource class or JSON response.
    //

    protected $casts = [
        'id' => 'string',
        'cloud_provider_id' => 'string',
        'cloud_account_id' => 'string',
        'tenant_account_id' => 'string',
        'api_key_id' => 'string',
        'name' => 'string',
        'description' => 'string',
        'fingerprint' => 'string',
        'flag_is_default' => 'integer',
        'provisioned_at' => 'datetime',
        'destroyed_at' => 'datetime',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
        'deleted_at' => 'datetime',
        'status' => 'string'
    ];

    //
    // Relationships
    // ------------------------------------------------------------------------
    // To define a relationship, create a public function with the name of the
    // table that you are defining a foreign relationship for. For a parent
    // relationship, use a singular table name (`package_widget`). For a child
    // or many-to-many relationship, use a plural name (`package_widgets`). You
    // can then copy and paste the respective return statement in.
    //

    //
    // Parent Relationships
    // ------------------------------------------------------------------------
    // return $this->belongsTo(ForeignModel::class, 'foreign_table_id');
    //

    /*
    public function cloudProvider() {
        return $this->belongsTo(CloudProvider::class, 'cloud_provider_id');
    }

    public function cloudAccount() {
        return $this->belongsTo(CloudAccount::class, 'cloud_account_id');
    }
    */

    public function tenantAccount() {
        return $this->belongsTo(TenantModels\TenantAccount::class, 'tenant_account_id');
    }

    //
    // Child Relationships
    // ------------------------------------------------------------------------
    // return $this->hasMany(ForeignModel::class, 'this_table_id');
    //

    /*
    public function cloudImageTemplates() {
        return $this->hasMany(CloudImageTemplate::class, 'cloud_ssh_key_id');
    }
    */

    //
    // Distant Child Relationships (Has Many Through)
    // ------------------------------------------------------------------------
    // return $this->hasManyThrough(
    //      DistantModel::class, IntermediateModel::class,
    //      'intermediate_key_id', 'distant_key_id', 'id'
    //      );
    //

    //
    // Many-to-Many Relationships
    // ------------------------------------------------------------------------
    // return $this->belongsToMany(ForeignModel::class, 'package_this_foreign_table', 'this_table_id', 'foreign_table_id');
    //

    //
    // Custom Attribute Setters
    // ------------------------------------------------------------------------
    // When storing attributes in the database, these methods add additional
    // business logic to be invoked when a field is stored or updated.
    //

}
