<!doctype html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">

        <title>TryLabs</title>

        <meta name="description" content="Simplifying virtual labs for training and software demos.">
        <meta name="author" content="Cloudstration">
        <meta name="robots" content="noindex, nofollow">

        {{-- CSRF Token --}}
        <meta name="csrf-token" content="{{ csrf_token() }}">

        {{-- Icons --}}
        <link rel="icon" href="{{ asset('images/favicons/favicon.ico') }}">
        <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('images/favicons/apple-touch-icon.png') }}">

        {{-- Fonts and Styles --}}
        @yield('css_before')
        <link rel="stylesheet" id="css-main" href="https://fonts.googleapis.com/css?family=Nunito+Sans:300,400,400i,600,700">
        <link rel="stylesheet" id="css-theme" href="{{ mix('css/dashmix.css') }}">
        <link rel="stylesheet" href="{{ mix('css/themes/xsmooth.css') }}">
        <link rel="stylesheet" href="{{ asset('css/fonts/icomoon.css') }}">
        <link rel="stylesheet" href="{{ asset('fonts/fontlogos/font-logos.css') }}">
        <link rel="stylesheet" href="{{ asset('fonts/mfizz/font-mfizz.css') }}">
        @yield('css_after')

        <!-- Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-133688728-4"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());
          gtag('config', 'UA-133688728-4');
        </script>

        {{-- Scripts --}}
        <script>window.Laravel = {!! json_encode(['csrfToken' => csrf_token(),]) !!};</script>
    </head>
    <body>

        <div id="page-container" class="enable-page-overlay side-scroll page-header-fixed page-header-dark main-content-boxed bg-gray-lighter">

            {{-- Header --}}
            <header id="page-header" class="bg-gd-xsmooth-dark">
                {{-- Header Content --}}
                <div class="content-header" style="max-width: 100%;">
                    {{-- Left Section --}}
                    <div>
                        <a class="h4 text-white mt-3" href="{{ route('user.dashboard.index') }}">TryLabs</a>
                        <span class="badge badge-info ml-1">BETA</span>
                    </div>
                    {{-- END Left Section --}}

                    <div class="p-1">{{-- Horizontal Navigation --}}

                        <div class="d-lg-none">{{-- Toggle Navigation --}}
                            {{-- Class Toggle, functionality initialized in Helpers.coreToggleClass() --}}
                            <button type="button" class="btn btn-block btn-light d-flex justify-content-between align-items-center" data-toggle="class-toggle" data-target="#horizontal-navigation-click-normal" data-class="d-none">
                                <i class="fa fa-bars"></i>
                            </button>
                        </div>{{-- END Toggle Navigation --}}

                        <div id="horizontal-navigation-click-normal" class="d-none d-lg-block mt-2 mt-lg-0">
                            <ul class="nav-main nav-main-horizontal">

                                <li class="nav-main-item">
                                    <a class="nav-main-link {{ Request::is('user/dashboard*') ? 'active' : '' }}" href="{{ route('user.dashboard.index') }}">
                                        <i class="nav-main-link-icon si si-home fa-lg"></i>
                                        <span class="nav-main-link-name">Dashboard</span>
                                    </a>
                                </li>
                                <li class="nav-main-item">
                                    <a class="nav-main-link {{ Request::is('user/image*') ? 'active' : '' }}" href="{{ route('user.image.templates.index') }}">
                                    <i class="nav-main-link-icon si si-disc fa-lg"></i>
                                        <span class="nav-main-link-name">Images</span>
                                    </a>
                                </li>
                                <li class="nav-main-item">
                                    <a class="nav-main-link {{ Request::is('user/reservations*') ? 'active' : '' }}" href="{{ route('user.reservations.index') }}">
                                        <i class="nav-main-link-icon si si-screen-desktop fa-lg"></i>
                                        <span class="nav-main-link-name">Reservations</span>
                                    </a>
                                </li>
                                <li class="nav-main-item">
                                    <a class="nav-main-link {{ Request::is('user/tokens*') ? 'active' : '' }}" href="{{ route('user.tokens.index') }}">
                                        <i class="nav-main-link-icon si si-link fa-lg"></i>
                                        <span class="nav-main-link-name">Tokens</span>
                                    </a>
                                </li>
                                <li class="nav-main-item">
                                    <a class="nav-main-link {{ Request::is('user/billing/usage*') ? 'active' : '' }}" href="{{ route('user.billing.usage.index') }}">
                                        <i class="nav-main-link-icon si si-bar-chart fa-lg"></i>
                                        <span class="nav-main-link-name">Usage</span>
                                    </a>
                                </li>
                                <li class="nav-main-item">
                                    <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="false" href="#">
                                        <i class="nav-main-link-icon si si-support fa-lg"></i>
                                        <span class="nav-main-link-name">Support</span>
                                    </a>
                                    <ul class="nav-main-submenu">
                                        <li class="nav-main-item">
                                            <a class="nav-main-link" target="_blank" href="{{ route('support.kb.index') }}">
                                                <i class="nav-main-link-icon si si-docs"></i>
                                                <span class="nav-main-link-name">Knowledge Base</span>
                                            </a>
                                        </li>
                                        <li class="nav-main-item">
                                            <a class="nav-main-link {{ Request::is('admin/auth/user/accounts/.../support*') ? 'active' : '' }}" href="mailto:support@trylabs.cloud">
                                                <i class="nav-main-link-icon si si-speech"></i>
                                                <span class="nav-main-link-name">Open a Support Ticket</span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>

                            </ul>
                        </div>
                    </div>{{-- END Horizontal Navigation --}}

                    <div>{{-- Right Section --}}
                        <div class="dropdown d-inline-block">{{-- User Dropdown --}}
                            <button type="button" class="btn btn-dual" id="page-header-user-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img class="img-avatar img-avatar48 mr-2" src="https://www.gravatar.com/avatar/{!! md5($request->user()->email) !!}?d={{ asset('images/avatar-wizard-sm.png') }}" alt="Avatar">
                                {{--<img class="img-avatar img-avatar48 mr-2" src="{{ asset('images/avatar-wizard-sm.png') }}" />--}}
                                {{--
                                <i class="fa fa-fw fa-user d-sm-none"></i>
                                <span class="d-none d-xl-inline-block">{{ $request->user()->full_name }}</span>
                                --}}
                                <i class="fa fa-fw fa-angle-down ml-1 d-none d-xl-inline-block"></i>
                            </button>
                            <div class="dropdown-menu dropdown-menu-right p-0" aria-labelledby="page-header-user-dropdown" style="width: 270px;">

                                {{-- User Info --}}
                                <div class="smini-hidden">
                                    <div class="content-side content-side-full align-items-center">
                                        <div class="ml-3">
                                            <a class="font-w600" href="javascript:void(0)">{{ $request->user()->first_name }} {{ $request->user()->last_name }}</a>
                                            <div class="font-size-sm font-italic">

                                                @if($request->user()->organization_name != null)
                                                    {{ $request->user()->organization_name}}<br />
                                                @else
                                                    <span class="text-muted">No Organization Associated</span><br />
                                                @endif

                                                {{ str_limit($request->user()->email, 25, '...') }}

                                            </div>

                                        </div>
                                    </div>
                                </div>
                                {{-- END User Info --}}

                                <div class="pb-2">
                                    {{--
                                    <div role="separator" class="dropdown-divider"></div>
                                    <a class="dropdown-item {{ Request::is('user/billing/usage*') ? 'active' : '' }}" href="{{ route('user.billing.usage.index' )}}">
                                        <i class="si si-hourglass fa-fw mr-2"></i>
                                        <span class="nav-main-link-name">## Credits Available</span>
                                    </a>
                                    --}}
                                    <div role="separator" class="dropdown-divider"></div>
                                    <a class="dropdown-item {{ Request::is('user/profile*') ? 'active' : '' }}" href="{{ route('user.profile.show' )}}">
                                        <i class="si si-user fa-fw mr-2"></i>
                                        <span class="nav-main-link-name">My Profile</span>
                                    </a>
                                    <a class="dropdown-item {{ Request::is('user/billing') ? 'active' : '' }}" href="{{ route('user.billing.index') }}">
                                        <i class="si si-credit-card fa-fw mr-2"></i>
                                        <span class="">Subscription & Payment</span>
                                    </a>
                                    {{-- TODO Tenant Accounts --}}
                                    <div role="separator" class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="{{ route('auth.logout') }}">
                                        <i class="si si-logout fa-fw mr-2"></i>
                                        <span class="">Sign Out</span>
                                    </a>
                                </div>
                            </div>
                        </div>{{-- END User Dropdown --}}
                    </div>{{-- END Right Section --}}
                </div>
                {{-- END Header Content --}}

                {{-- Header Loader --}}
                {{-- Please check out the Loaders page under Components category to see examples of showing/hiding it --}}
                <div id="page-header-loader" class="overlay-header bg-primary-darker">
                    <div class="content-header">
                        <div class="w-100 text-center">
                            <i class="fa fa-fw fa-2x fa-sun fa-spin text-white"></i>
                        </div>
                    </div>
                </div>
                {{-- END Header Loader --}}
            </header>
            {{-- END Header --}}

            {{-- Main Container --}}
            <main id="main-container">

                <div id="app">

                    @if(config('trylabs.motd.enabled') == true)
                        <div class="alert alert-{{ config('trylabs.motd.color') }} border-bottom border-{{ config('trylabs.motd.color') }}" style="max-width: 100%; text-align: center;">
                            {{ config('trylabs.motd.text') }}
                        </div>
                    @endif

                    @yield('main_container')

                </div>

            </main>
            {{-- END Main Container --}}

            {{-- Footer --}}
            <footer id="page-footer" class="bg-body-light border-top">
                <div class="content py-0">
                    <div class="row font-size-sm">
                        <div class="col-sm-6 order-sm-2 mb-1 mb-sm-0 text-center text-sm-right">
                            TryLabs is a managed service from <a class="font-w600" href="https://www.cloudstration.com" target="_blank">Cloudstration</a>
                        </div>
                        <div class="col-sm-6 order-sm-1 text-center text-sm-left">
                            &copy; <span data-toggle="year-copy">2019</span> <a class="font-w600" href="https://www.cloudstration.com" target="_blank">Cloudstration Inc.</a> All Rights Reserved.
                        </div>
                    </div>
                </div>
            </footer>
            {{-- END Footer --}}
        </div>

        {{-- END Page Container --}}

        @yield('js_before')

        {{-- JS Scripts --}}
        <script src="{{ mix('js/laravel.app.js') }}"></script>
        <script src="{{ mix('js/dashmix.app.js') }}"></script>

        @yield('js_after')
    </body>
</html>
