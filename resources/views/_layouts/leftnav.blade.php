<div class="content-side content-side-full">
    <ul class="nav-main">
        {{--
        <li class="nav-main-item">
            <a class="nav-main-link{{ request()->is('dashboard') ? ' active' : '' }}" href="/dashboard">
                <i class="nav-main-link-icon icon-Dashboard"></i>
                <span class="nav-main-link-name">Dashboard</span>
                <span class="nav-main-link-badge badge badge-pill badge-success">5</span>
            </a>
        </li>
        <li class="nav-main-item">
            <a class="nav-main-link" href="/">
                <i class="nav-main-link-icon icon-Business-Man"></i>
                <span class="nav-main-link-name">User Accounts</span>
            </a>
        </li>
        <li class="nav-main-item">
            <a class="nav-main-link" href="/">
                <i class="nav-main-link-icon icon-URL-Window"></i>
                <span class="nav-main-link-name">Domain Names</span>
            </a>
        </li>

        <li class="nav-main-heading">Reservations</li>

        <li class="nav-main-item">
            <a class="nav-main-link" href="">
                <i class="nav-main-link-icon far icon-Movie-Ticket"></i>
                <span class="nav-main-link-name">Reservation Tokens</span>
            </a>
        </li>
        <li class="nav-main-item">
            <a class="nav-main-link" href="db_social_compact.html">
                <i class="nav-main-link-icon far icon-Calendar-4"></i>
                <span class="nav-main-link-name">Provisioning Schedule</span>
            </a>
        </li>
        <li class="nav-main-item">
            <a class="nav-main-link" href="">
                <i class="nav-main-link-icon far icon-Cloud-Computer"></i>
                <span class="nav-main-link-name">Reservation History</span>
            </a>
        </li>
        <li class="nav-main-item">
            <a class="nav-main-link" href="">
                <i class="nav-main-link-icon far icon-Data-Center"></i>
                <span class="nav-main-link-name">Virtual Machines</span>
            </a>
        </li>

        <li class="nav-main-heading">Settings</li>
        <li class="nav-main-item">
            <a class="nav-main-link active" href="{{ route('admin.auth.user.accounts.index') }}">
                <i class="nav-main-link-icon far icon-Business-Mens"></i>
                <span class="nav-main-link-name">Users and Groups</span>
            </a>
        </li>
        <li class="nav-main-item">
            <a class="nav-main-link" href="db_social_compact.html">
                <i class="nav-main-link-icon far icon-Lock-2"></i>
                <span class="nav-main-link-name">Security Settings</span>
            </a>
        </li>
        <li class="nav-main-item">
            <a class="nav-main-link" href="db_social_compact.html">
                <i class="nav-main-link-icon far icon-URL-Window"></i>
                <span class="nav-main-link-name">Domain Names</span>
            </a>
        </li>
        <li class="nav-main-item">
            <a class="nav-main-link" href="">
                <i class="nav-main-link-icon far icon-Credit-Card2"></i>
                <span class="nav-main-link-name">Billing & Usage Credits</span>
            </a>
        </li>
        <li class="nav-main-item">
            <a class="nav-main-link" href="db_social_compact.html">
                <i class="nav-main-link-icon far icon-Gear"></i>
                <span class="nav-main-link-name">Account Settings</span>
            </a>
        </li>

    </ul>--}}

    <ul class="nav-main">



        <li class="nav-main-heading">My Lab</li>
        <li class="nav-main-item">
            <a class="nav-main-link {{ Request::is('user/dashboard*') ? 'active' : '' }}" href="{{ route('user.dashboard.index') }}">
                <i class="nav-main-link-icon far icon-Monitor-Analytics"></i>
                <span class="nav-main-link-name">Dashboard</span>
            </a>
        </li>
        <li class="nav-main-item">
            <a class="nav-main-link {{ Request::is('user/reservations*') ? 'active' : '' }}" href="{{ route('user.reservations.index') }}">
                <i class="nav-main-link-icon far icon-Data-Clock"></i>
                <span class="nav-main-link-name">Reservations</span>
            </a>
        </li>
        <li class="nav-main-item">
            <a class="nav-main-link {{ Request::is('user/image/templates*') ? 'active' : '' }}" href="{{ route('user.image.templates.index') }}">
                <i class="nav-main-link-icon far icon-Data-Copy"></i>
                <span class="nav-main-link-name">Image Templates</span>
            </a>
        </li>

        <li class="nav-main-heading">Settings</li>
        <li class="nav-main-item">
            <a class="nav-main-link {{ Request::is('user/profile*') ? 'active' : '' }}" href="{{ route('user.profile.show' )}}">
                <!--<i class="nav-main-link-icon far fa-user-circle"></i>-->
                <i class="nav-main-link-icon far icon-Profile"></i>
                <span class="nav-main-link-name">User Profile</span>
            </a>
        </li>
        <li class="nav-main-item">
            <a class="nav-main-link {{ Request::is('user/billing') ? 'active' : '' }}" href="{{ route('user.billing.index') }}">
                <i class="nav-main-link-icon far icon-Credit-Card2"></i>
                <span class="nav-main-link-name">Billing & Plan</span>
            </a>
        </li>
        <li class="nav-main-item">
            <a class="nav-main-link {{ Request::is('user/billing/usage*') ? 'active' : '' }}" href="{{ route('user.billing.usage.index') }}">
                <i class="nav-main-link-icon far icon-Monitor-Analytics"></i>
                <span class="nav-main-link-name">Usage Analytics</span>
            </a>
        </li>
        <li class="nav-main-item">
            <a class="nav-main-link {{ Request::is('user/image/settings*') ? 'active' : '' }}" href="{{ route('user.image.settings.edit') }}">
                <i class="nav-main-link-icon far icon-Data-Copy"></i>
                <span class="nav-main-link-name">Image Settings</span>
            </a>
        </li>

        <li class="nav-main-heading">Help &amp; Support</li>
        <li class="nav-main-item">
            <a class="nav-main-link {{ Request::is('user/tutorial*') ? 'active' : '' }}" href="{{ route('user.tutorial.index') }}">
                <!--<i class="nav-main-link-icon far fa-envelope-open"></i>-->
                <i class="nav-main-link-icon far icon-Professor"></i>
                <span class="nav-main-link-name">Tutorials</span>
                <span class="nav-main-link-badge badge badge-pill badge-info">1</span>
            </a>
        </li>
        <li class="nav-main-item">
            <a class="nav-main-link {{ Request::is('admin/auth/user/accounts/.../support*') ? 'active' : '' }}" href="{{ route('support.dashboard.index') }}">
                <!--<i class="nav-main-link-icon far fa-envelope-open"></i>-->
                <i class="nav-main-link-icon far icon-Spell-Check"></i>
                <span class="nav-main-link-name">Knowledge Base</span>
                <span class="nav-main-link-badge badge badge-pill badge-info">1</span>
            </a>
        </li>
        <li class="nav-main-item">
            <a class="nav-main-link {{ Request::is('admin/auth/user/accounts/.../support*') ? 'active' : '' }}" href="{{ route('support.dashboard.index') }}">
                <!--<i class="nav-main-link-icon far fa-envelope-open"></i>-->
                <i class="nav-main-link-icon far icon-Speach-BubbleDialog"></i>
                <span class="nav-main-link-name">Support Tickets</span>
                <span class="nav-main-link-badge badge badge-pill badge-info">1</span>
            </a>
        </li>
        <li class="nav-main-item">
            <a class="nav-main-link {{ Request::is('admin/auth/user/accounts/.../support*') ? 'active' : '' }}" href="{{ route('support.dashboard.index') }}">
                <!--<i class="nav-main-link-icon far fa-envelope-open"></i>-->
                <i class="nav-main-link-icon far icon-Idea-2"></i>
                <span class="nav-main-link-name">Features and Bugs</span>
                <span class="nav-main-link-badge badge badge-pill badge-info">1</span>
            </a>
        </li>

        {{--<li class="nav-main-heading">Team Accounts<span class="badge badge-info ml-2">Coming Soon</span></li>--}}

        {{--
        @foreach($auth_user_account->data->relationships->tenant_users->data as $tenant_user)

            <li class="nav-main-item {{ Request::is('admin/auth/user/accounts/'.$auth_user_account->data->id.'/tenants/'.$tenant_user->tenant_account_id.'*') ? 'open' : '' }}">
                <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="{{ Request::is('admin/auth/user/accounts/'.$auth_user_account->data->id.'/tenants/'.$tenant_user->tenant_account_id.'*') ? 'true' : 'false' }}" href="#">
                    <i class="nav-main-link-icon fa icon-People-onCloud"></i>
                    <span class="nav-main-link-name">{{ $tenant_user->relationships->tenant_account->data->name }}</span>
                </a>
                <ul class="nav-main-submenu">

                    <li class="nav-main-item">
                        <a class="nav-main-link {{ Request::is('admin/auth/user/accounts/'.$auth_user_account->data->id.'/tenants/'.$tenant_user->tenant_account_id) ? 'active' : '' }}" href="{{ route('admin.auth.user.accounts.tenants.show', ['id' => $auth_user_account->data->id, 'tenant_account_id' => $tenant_user->tenant_account_id]) }}" >
                            <i class="nav-main-link-icon far icon-Dashboard"></i>
                            <span class="nav-main-link-name">Dashboard</span>
                        </a>
                    </li>

                    <li class="nav-main-heading">Reservations <span class="badge badge-info ml-1">Coming in v1.0</span></li>

                    <li class="nav-main-item">
                        <a class="nav-main-link" href="#">
                            <i class="nav-main-link-icon far icon-Data-Copy"></i>
                            <span class="nav-main-link-name">Image Templates</span>
                        </a>
                    </li>
                    <li class="nav-main-item">
                        <a class="nav-main-link {{ Request::is('admin/auth/user/accounts/'.$auth_user_account->data->id.'/tenants/'.$tenant_user->tenant_account_id.'/reservation/tokens*') ? 'active' : '' }}" href="{{ route('admin.auth.user.accounts.tenants.reservation.tokens.index', ['auth_user_account_id' => $auth_user_account->data->id, 'tenant_account_id' => $tenant_user->tenant_account_id]) }}">
                            <i class="nav-main-link-icon far icon-Movie-Ticket"></i>
                            <span class="nav-main-link-name">Reservation Tokens</span>
                        </a>
                    </li>
                    <li class="nav-main-item">
                        <a class="nav-main-link {{ Request::is('admin/auth/user/accounts/'.$auth_user_account->data->id.'/tenants/'.$tenant_user->tenant_account_id.'/image/schedules*') ? 'active' : '' }}" href="{{ route('admin.auth.user.accounts.tenants.image.schedules.index', ['auth_user_account_id' => $auth_user_account->data->id, 'tenant_account_id' => $tenant_user->tenant_account_id]) }}">
                            <i class="nav-main-link-icon far icon-Calendar-4"></i>
                            <span class="nav-main-link-name">Provisioning Schedule</span>
                        </a>
                    </li>
                    <li class="nav-main-item">
                        <a class="nav-main-link {{ Request::is('admin/auth/user/accounts/'.$auth_user_account->data->id.'/tenants/'.$tenant_user->tenant_account_id.'/reservations*') ? 'active' : '' }}" href="{{ route('admin.auth.user.accounts.tenants.reservations.index', ['auth_user_account_id' => $auth_user_account->data->id, 'tenant_account_id' => $tenant_user->tenant_account_id]) }}">
                            <i class="nav-main-link-icon far icon-Cloud-Computer"></i>
                            <span class="nav-main-link-name">Reservation History</span>
                        </a>
                    </li>

                    <li class="nav-main-heading">Content <span class="badge badge-secondary ml-1">Coming Soon</span></li>
                    <li class="nav-main-item">
                        <a class="nav-main-link text-muted">
                            <i class="nav-main-link-icon far icon-Open-Book"></i>
                            <span class="nav-main-link-name">User Libraries</span>
                        </a>
                    </li>
                    <li class="nav-main-item">
                        <a class="nav-main-link text-muted">
                            <i class="nav-main-link-icon far icon-Sidebar-Window"></i>
                            <span class="nav-main-link-name">Sidebar Tutorials</span>
                        </a>
                    </li>
                    <li class="nav-main-item">
                        <a class="nav-main-link text-muted">
                            <i class="nav-main-link-icon far icon-Movie"></i>
                            <span class="nav-main-link-name">Video Tutorials</span>
                        </a>
                    </li>

                    <li class="nav-main-heading">Settings <span class="badge badge-info ml-1">Coming in v1.0</span></li>
                    <li class="nav-main-item">
                        <a class="nav-main-link {{ Request::is('admin/auth/user/accounts/'.$auth_user_account->data->id.'/tenants/'.$tenant_user->tenant_account_id.'/users*') ? 'active' : '' }}" href="{{ route('admin.auth.user.accounts.tenants.users.index', ['auth_user_account_id' => $auth_user_account->data->id, 'tenant_account_id' => $tenant_user->tenant_account_id]) }}">
                            <i class="nav-main-link-icon far icon-Business-Mens"></i>
                            <span class="nav-main-link-name">Users and Groups</span>
                        </a>
                    </li>
                    <li class="nav-main-item">
                        <a class="nav-main-link {{ Request::is('admin/auth/user/accounts/'.$auth_user_account->data->id.'/tenants/'.$tenant_user->tenant_account_id.'/security*') ? 'active' : '' }}" href="{{ route('admin.auth.user.accounts.tenants.security.index', ['auth_user_account_id' => $auth_user_account->data->id, 'tenant_account_id' => $tenant_user->tenant_account_id]) }}">
                            <i class="nav-main-link-icon far icon-Lock-2"></i>
                            <span class="nav-main-link-name">Security Settings</span>
                        </a>
                    </li>
                    <li class="nav-main-item">
                        <a class="nav-main-link {{ Request::is('admin/auth/user/accounts/'.$auth_user_account->data->id.'/tenants/'.$tenant_user->tenant_account_id.'/domains*') ? 'active' : '' }}" href="{{ route('admin.auth.user.accounts.tenants.domains.index', ['auth_user_account_id' => $auth_user_account->data->id, 'tenant_account_id' => $tenant_user->tenant_account_id]) }}">
                            <i class="nav-main-link-icon far icon-URL-Window"></i>
                            <span class="nav-main-link-name">Custom Domain Names</span>
                        </a>
                    </li>
                    <li class="nav-main-item">
                        <a class="nav-main-link {{ Request::is('admin/auth/user/accounts/'.$auth_user_account->data->id.'/tenants/'.$tenant_user->tenant_account_id.'/billing*') ? 'active' : '' }}" href="{{ route('admin.auth.user.accounts.tenants.billing.index', ['auth_user_account_id' => $auth_user_account->data->id, 'tenant_account_id' => $tenant_user->tenant_account_id]) }}">
                            <i class="nav-main-link-icon far icon-Credit-Card2"></i>
                            <span class="nav-main-link-name">Billing & Usage Credits</span>
                        </a>
                    </li>
                    <li class="nav-main-item">
                        <a class="nav-main-link {{ Request::is('admin/auth/user/accounts/'.$auth_user_account->data->id.'/tenants/'.$tenant_user->tenant_account_id.'/settings*') ? 'active' : '' }}" href="{{ route('admin.auth.user.accounts.tenants.settings.index', ['auth_user_account_id' => $auth_user_account->data->id, 'tenant_account_id' => $tenant_user->tenant_account_id]) }}">
                            <i class="nav-main-link-icon far icon-Gear"></i>
                            <span class="nav-main-link-name">Account Settings</span>
                        </a>
                    </li>
                </ul>
            </li>

        @endforeach
        --}}

        {{--
        <li class="nav-main-item open mt-2">
            <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="true" href="#">
                <i class="nav-main-link-icon fa icon-People-onCloud"></i>
                <span class="nav-main-link-name">My Team Name (Demo)</span>
            </a>

            <ul class="nav-main-submenu">

                <li class="nav-main-item">
                    <a class="nav-main-link" href="#">
                        <i class="nav-main-link-icon far icon-Dashboard"></i>
                        <span class="nav-main-link-name">Dashboard</span>
                    </a>
                </li>

                <li class="nav-main-heading">Reservations</li>

                <li class="nav-main-item">
                    <a class="nav-main-link" href="#">
                        <i class="nav-main-link-icon far icon-Data-Copy"></i>
                        <span class="nav-main-link-name">Image Templates</span>
                    </a>
                </li>
                <li class="nav-main-item">
                    <a class="nav-main-link" href="#">
                        <i class="nav-main-link-icon far icon-Movie-Ticket"></i>
                        <span class="nav-main-link-name">Reservation Tokens</span>
                    </a>
                </li>
                <li class="nav-main-item">
                    <a class="nav-main-link" href="#">
                        <i class="nav-main-link-icon far icon-Calendar-4"></i>
                        <span class="nav-main-link-name">Provisioning Schedule</span>
                    </a>
                </li>
                <li class="nav-main-item">
                    <a class="nav-main-link" href="#">
                        <i class="nav-main-link-icon far icon-Cloud-Computer"></i>
                        <span class="nav-main-link-name">Reservation History</span>
                    </a>
                </li>

                <li class="nav-main-heading">Content</li>
                <li class="nav-main-item">
                    <a class="nav-main-link text-muted">
                        <i class="nav-main-link-icon far icon-Open-Book"></i>
                        <span class="nav-main-link-name">User Libraries</span>
                    </a>
                </li>
                <li class="nav-main-item">
                    <a class="nav-main-link text-muted">
                        <i class="nav-main-link-icon far icon-Sidebar-Window"></i>
                        <span class="nav-main-link-name">Sidebar Tutorials</span>
                    </a>
                </li>
                <li class="nav-main-item">
                    <a class="nav-main-link text-muted">
                        <i class="nav-main-link-icon far icon-Movie"></i>
                        <span class="nav-main-link-name">Video Tutorials</span>
                    </a>
                </li>

                <li class="nav-main-heading">Settings</li>
                <li class="nav-main-item">
                    <a class="nav-main-link" href="#">
                        <i class="nav-main-link-icon far icon-Business-Mens"></i>
                        <span class="nav-main-link-name">Users and Groups</span>
                    </a>
                </li>
                <li class="nav-main-item">
                    <a class="nav-main-link" href="#">
                        <i class="nav-main-link-icon far icon-Lock-2"></i>
                        <span class="nav-main-link-name">Security Settings</span>
                    </a>
                </li>
                <li class="nav-main-item">
                    <a class="nav-main-link" href="#">
                        <i class="nav-main-link-icon far icon-URL-Window"></i>
                        <span class="nav-main-link-name">Custom Domain Names</span>
                    </a>
                </li>
                <li class="nav-main-item">
                    <a class="nav-main-link" href="#">
                        <i class="nav-main-link-icon far icon-Credit-Card2"></i>
                        <span class="nav-main-link-name">Billing & Usage Credits</span>
                    </a>
                </li>
                <li class="nav-main-item">
                    <a class="nav-main-link" href="#">
                        <i class="nav-main-link-icon far icon-Gear"></i>
                        <span class="nav-main-link-name">Account Settings</span>
                    </a>
                </li>
            </ul>
        </li>
        --}}

    </ul>
</div>
