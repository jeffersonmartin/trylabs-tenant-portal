@extends('_layouts.core')

@section('page_container_class', 'sidebar-o')

@section('main_container')

    {{-- Page Content --}}
    <div class="row no-gutters flex-md-10-auto">
        <div class="col-12">

            <div class="bg-gray-lighter">
                <div class="content content-full">
                    <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                        <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">@yield('page_title')</h1>
                        <div class="flex-sm-00-auto ml-sm-3">
                            @yield('page_actions')
                        </div>
                    </div>
                </div>
            </div>

            {{-- Main Content --}}
            <div class="content content-full pt-3">

                @if($request->session()->has('meta'))

                    @php
                        $meta = $request->session()->get('meta');

                        if($meta->alert == 'green') {
                            $alert_color = 'success';
                        } elseif($meta->alert == 'yellow') {
                            $alert_color = 'warning';
                        } elseif($meta->alert == 'red') {
                            $alert_color = 'danger';
                        } elseif($meta->alert == 'blue') {
                            $alert_color = 'info';
                        }
                    @endphp

                    <div class="alert alert-{{ $alert_color }}">
                        {{ $meta->message }}
                    </div>

                @endif

                @yield('content')

            </div>
            {{-- END Main Content --}}
        </div>
    </div>
    {{-- END Page Content --}}

    @yield('modals')

@endsection
