@extends('_layouts.tenant_lite')

@section('page_title', 'Create a Lab Reservation')

@section('page_actions')
    <a class="btn btn-outline-info mr-2" href="#modal-create-reservation" data-toggle="modal">Change Image Template</a>
    <a class="btn btn-outline-primary mr-2" href="{{ route('user.reservations.index') }}">Back to My Reservations</a>
@endsection

@section('content')

    <form method="post" action="{{ route('user.reservations.store') }}">
        @csrf

        <input type="hidden" name="tenant_account_id" value="{{ $request->user()->tenant_account_id }}" />
        <input type="hidden" name="tenant_user_id" value="{{ $tenant_user->data->id }}" />
        <input type="hidden" name="tenant_reservation_category_id" value="{{ $tenant_reservation_category->data->id }}" />
        <input type="hidden" name="cloud_image_template_id" value="{{ $request->cloud_image_template_id }}" />
        <input type="hidden" name="terminate_action" value="discard" />

        @if($errors->any())
            <div class="alert alert-warning">
                <strong>You'll need to make a few changes before your magic spells will work here.</strong>
                <ul class="mb-0">
                    @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="block block-bordered block-rounded">
            <div class="block-header bg-body-light border-bottom">
                <h3 class="block-title">
                    {{ $image_template->data->name }}
                    {!! $image_template->data->version != null ? '<span class="badge text-primary-darker bg-primary-lighter ml-2">Version '.$image_template->data->version.'</span>': '' !!}
                    <span class="badge badge-secondary ml-2">{{ $image_template->data->short_id }}</span>
                </h3>
                <div class="block-options">
                    Created on {{ \Carbon\Carbon::parse($image_template->data->timestamps->created_at)->toFormattedDateString() }}
                </div>
            </div>
            <div class="block-content">
                <div class="row mb-3">
                    <div class="col-lg-6">
                        <div class="font-size-h4 font-w600">Description</div>
                        <p>
                            @if($image_template->data->description != null)
                                {{ $image_template->data->description }}
                            @else
                                You have not added a description for this image template.<br/>
                                <a href="{{ route('user.image.templates.edit', ['cloud_image_template_id' => $image_template->data->id]) }}">Would you like to add a description?</a>
                            @endif
                        </p>
                    </div>
                    <div class="col-lg-6">
                        <div class="font-size-h4 font-w600">Internal Notes</div>
                        <p>
                            @if($image_template->data->notes != null)
                                {{ $image_template->data->notes }}
                            @else
                                You have not added any internal notes for this image template.<br />
                                <a href="{{ route('user.image.templates.edit', ['cloud_image_template_id' => $image_template->data->id]) }}">Would you like to add some notes?</a>
                            @endif
                        </p>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">

            <div class="col-lg-3">
                <div class="block block-bordered block-rounded">
                    <div class="block-content ribbon ribbon-light">
                        <div class="ribbon-box">
                            <small>{{ $image_template->data->relationships->cloud_image_flavor->data->version }}</small>
                        </div>
                        <div class="row text-center">
                            <div class="col-12">
                                @if($image_template->data->relationships->cloud_image_flavor->data->name == 'Ubuntu')
                                    <span class="d-block fl-ubuntu my-1" style="font-size: 3em;"></span>
                                @elseif($image_template->data->relationships->cloud_image_flavor->data->name == 'CentOS')
                                    <span class="d-block fl-centos my-1" style="font-size: 3em;"></span>
                                @elseif($image_template->data->relationships->cloud_image_flavor->data->name == 'CoreOS')
                                    <span class="d-block fl-coreos my-1" style="font-size: 3em;"></span>
                                @elseif($image_template->data->relationships->cloud_image_flavor->data->name == 'Fedora')
                                    <span class="d-block fl-fedora my-1" style="font-size: 3em;"></span>
                                @elseif($image_template->data->relationships->cloud_image_flavor->data->name == 'Debian')
                                    <span class="d-block fl-debian my-1" style="font-size: 3em;"></span>
                                @else
                                    <span class="d-block fl-tux my-1" style="font-size: 3em;"></span>
                                @endif
                                <div class="font-size-h4 font-w600 mb-4">{{ $image_template->data->relationships->cloud_image_flavor->data->name }}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-6">
                <div class="block block-bordered block-rounded">
                    <div class="block-content ribbon ribbon-light">
                        <div class="ribbon-box">
                            <small>{{ $image_template->data->relationships->cloud_image_size->data->name }}</small>
                        </div>
                        <div class="row text-center mb-4">
                            <div class="col-4">
                                <span class="d-block icon-CPU my-3" style="font-size: 3em;"></span>
                                <div class="font-size-h4 font-w600">{{ $image_template->data->relationships->cloud_image_size->data->vcpu }} vCPU</div>
                            </div>
                            <div class="col-4">
                                <span class="d-block icon-Ram my-3" style="font-size: 3em;"></span>
                                <div class="font-size-h4 font-w600">{{ $image_template->data->relationships->cloud_image_size->data->memory }}GB RAM</div>
                            </div>
                            <div class="col-4">
                                <span class="d-block icon-Data-Storage my-3" style="font-size: 3em;"></span>
                                <div class="font-size-h4 font-w600">{{ $image_template->data->relationships->cloud_image_size->data->disk }}GB SSD</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-3">
                <div class="block block-bordered block-rounded">
                    <div class="block-content">
                        <div class="row text-center mb-4">
                            <div class="col-12">
                                @if($image_template->data->relationships->cloud_region->data->name == 'Amsterdam')
                                    <span class="d-block icon-Bicycle my-3" style="font-size: 3em;"></span>
                                @elseif($image_template->data->relationships->cloud_region->data->name == 'Frankfurt')
                                    <span class="d-block icon-Beer-Glass my-3" style="font-size: 3em;"></span>
                                @elseif($image_template->data->relationships->cloud_region->data->name == 'India')
                                    <span class="d-block icon-Taj-Mahal my-3" style="font-size: 3em;"></span>
                                @elseif($image_template->data->relationships->cloud_region->data->name == 'London')
                                    <span class="d-block icon-United-Kingdom my-3" style="font-size: 3em;"></span>
                                @elseif($image_template->data->relationships->cloud_region->data->name == 'New York' || $image_template->data->relationships->cloud_region->data->name == 'New York City')
                                    <span class="d-block icon-Chrysler-Building my-3" style="font-size: 3em;"></span>
                                @elseif($image_template->data->relationships->cloud_region->data->name == 'San Francisco')
                                    <span class="d-block icon-Bridge my-3" style="font-size: 3em;"></span>
                                @elseif($image_template->data->relationships->cloud_region->data->name == 'Singapore')
                                    <span class="d-block icon-Singapore my-3" style="font-size: 3em;"></span>
                                @elseif($image_template->data->relationships->cloud_region->data->name == 'Toronto')
                                    <span class="d-block icon-Canada my-3" style="font-size: 3em;"></span>
                                @else
                                    <span class="d-block icon-Location-2 my-3" style="font-size: 3em;"></span>
                                @endif
                                <div class="font-size-h4 font-w600">{{ $image_template->data->relationships->cloud_region->data->name }}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="row">

            <div class="col-6">
                <div class="block block-bordered block-rounded bg-info-lighter border-info">
                    <div class="block-content">
                        <div class="row text-center mb-4">
                            <div class="col-12">
                                <div class="font-size-h4 font-w600 mb-3">
                                    How long should your lab reservation last?
                                </div>

                                <div class="form-group mb-4">
                                    <select name="count_mins" class="form-control border-info form-control-lg">

                                        {{-- 30 Minutes --}}
                                        @if($tenant_user->data->relationships->tenant_account->data->credit_balance_qty > $image_template->data->relationships->cloud_image_size->data->qty_credits_per_hour * 0.5)
                                            <option value="{{ 0.5 * 60 }}">
                                                {!! $image_template->data->relationships->cloud_image_size->data->qty_credits_per_hour * 0.5 !!} Credits - 30 Mins - Ends {{ now()->timezone($request->user()->timezone)->addMinutes(30)->format('D') }} {{ now()->timezone($request->user()->timezone)->addMinutes(30)->format('g:i A T') }}
                                            </option>
                                        @else
                                            <option value="0">
                                                No Lab Credits Available
                                            </option>
                                            <option value="{{ 0.5 * 60 }}" disabled="disabled">
                                                {!! $image_template->data->relationships->cloud_image_size->data->qty_credits_per_hour * 0.5 !!} Credits - 30 Mins - Not Enough Lab Credits
                                            </option>
                                        @endif

                                        {{-- 60 Minutes --}}
                                        @if($tenant_user->data->relationships->tenant_account->data->credit_balance_qty > $image_template->data->relationships->cloud_image_size->data->qty_credits_per_hour * 1.0)
                                            <option value="{{ 1.0 * 60 }}">
                                                {!! $image_template->data->relationships->cloud_image_size->data->qty_credits_per_hour * 1.0 !!} Credits - 60 Mins - Ends {{ now()->timezone($request->user()->timezone)->addMinutes(60)->format('D') }} {{ now()->timezone($request->user()->timezone)->addMinutes(60)->format('g:i A T') }}
                                            </option>
                                        @else
                                            <option value="{{ 1.0 * 60 }}" disabled="disabled">
                                                {!! $image_template->data->relationships->cloud_image_size->data->qty_credits_per_hour * 1.0 !!} Credits - 60 Mins - Not Enough Lab Credits
                                            </option>
                                        @endif

                                        {{-- 90 Minutes --}}
                                        @if($tenant_user->data->relationships->tenant_account->data->credit_balance_qty > $image_template->data->relationships->cloud_image_size->data->qty_credits_per_hour * 1.5)
                                            <option value="{{ 1.5 * 60 }}">
                                                {!! $image_template->data->relationships->cloud_image_size->data->qty_credits_per_hour * 1.5 !!} Credits - 90 Mins - Ends {{ now()->timezone($request->user()->timezone)->addMinutes(90)->format('D') }} {{ now()->timezone($request->user()->timezone)->addMinutes(90)->format('g:i A T') }}
                                            </option>
                                        @else
                                            <option value="{{ 1.5 * 60 }}" disabled="disabled">
                                                {!! $image_template->data->relationships->cloud_image_size->data->qty_credits_per_hour * 1.5 !!} Credits - 90 Mins - Not Enough Lab Credits
                                            </option>
                                        @endif

                                        {{-- Loop through hours 2 to 30 --}}
                                        @for($hour = 2; $hour <= 30; $hour++)

                                            @if($tenant_user->data->relationships->tenant_account->data->credit_balance_qty > $image_template->data->relationships->cloud_image_size->data->qty_credits_per_hour * $hour)
                                                <option value="{{ $hour * 60 }}">
                                                    {!! $image_template->data->relationships->cloud_image_size->data->qty_credits_per_hour * $hour !!} Credits - {{ $hour }} Hours - Ends {{ now()->timezone($request->user()->timezone)->addHours($hour)->format('D') }} {{ now()->timezone($request->user()->timezone)->addHours($hour)->format('g:i A T') }}
                                                </option>
                                            @else
                                                <option value="{{ $hour * 60 }}" disabled="disabled">
                                                    {!! $image_template->data->relationships->cloud_image_size->data->qty_credits_per_hour * $hour !!} Credits - {{ $hour }} Hours - Not Enough Lab Credits
                                                </option>
                                            @endif
                                        @endfor
                                    </select>
                                </div>
                                <p class="text-center mb-0">
                                    We will deduct lab credits from your account when you start your reservation. The number of credits is based on the image size that you selected for this image template. If you terminate your reservation before it expires, any unused hours will be credited to your account.<br />
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="col-6">
                <div class="block block-bordered block-rounded bg-warning-lighter border-warning">
                    <div class="block-content">
                        <div class="row text-center mb-4">
                            <div class="col-12">
                                <p class="text-center mb-0">
                                    <div class="font-size-h3 font-w600 mb-3">You have {{ number_format($tenant_user->data->relationships->tenant_account->data->credit_balance_qty, 1) }} lab credits available.</div>
                                    <strong>Need more lab credits?</strong> We'll deposit additional lab credits in your account when your subscription renews or you can <a href="{{ route('user.billing.index') }}">upgrade your plan</a> retroactively for this month to get more lab credits today.<br />
                                    <br />
                                    <a class="btn btn-warning btn-lg" href="{{ route('user.billing.index') }}">View Available Plans</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

        <div class="row">
            <div class="col-12">
                <div class="block block-bordered">
                    <div class="block-content d-flex">
                        <div class="flex-fill mr-3">
                            <p>
                                When you click Launch Lab, lab credits will be deducted from your account and we will begin provisioning a VM instance using your image template. You'll be able to access your lab environment in 30-90 seconds.
                            </p>
                        </div>
                        <div class="flex-00-auto mb-3">
                            <a class="btn btn-outline-secondary btn-lg mr-1" href="{{ route('user.reservations.index') }}">Cancel</a>
                            @if($tenant_user->data->relationships->tenant_account->data->credit_balance_qty > $image_template->data->relationships->cloud_image_size->data->qty_credits_per_hour * 0.5)
                                <button type="submit" class="btn btn-success btn-lg">Launch Lab</button>
                            @else
                                <button type="button" class="btn btn-outline-success btn-lg disabled" disabled="disabled" v-b-tooltip.hover title="You do not have enough lab credits to launch a lab reservation. Please upgrade your plan to get more lab credits.">Launch Lab</button>
                            @endif

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </form>

@endsection

@section('modals')
    @include('user.reservations._modals.create_reservation')
@endsection
