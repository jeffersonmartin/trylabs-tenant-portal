@extends('_layouts.tenant_lite')

@section('page_title', 'My Reservations')
@section('page_actions')
    <a class="btn btn-outline-primary mr-2" data-toggle="modal" href="#modal-create-reservation">Create a New Lab Reservation</a>
    {{--<b-dropdown id="page-actions-dropdown" right variant="outline-primary" text="Actions" class="m-md-2">
        <b-dropdown-item>Verify Account</b-dropdown-item>
        <b-dropdown-item>Reset Password</b-dropdown-item>
        <b-dropdown-item>Lock Account</b-dropdown-item>
        <b-dropdown-item>Unlock Account</b-dropdown-item>
        <b-dropdown-divider></b-dropdown-divider>
        <b-dropdown-item>Delete User</b-dropdown-item>
    </b-dropdown>--}}
@endsection

@section('content')

    @if($errors->any())
        <div class="alert alert-warning">
            <strong>You'll need to make a few changes before your magic spells will work here.</strong>
            <ul class="mb-0">
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    @if(!empty($active_reservations->data))

        <div class="block block-rounded block-bordered">

            <table class="table table-condensed mb-0">
                <tbody>
                    {{-- TODO Empty reservations table --}}
                    @foreach($active_reservations->data as $reservation)

                        @if($reservation->timestamps->terminated_at == null)

                            <tr>
                                <td class="p-3" {!! $loop->first ? 'style="border-top: none; width: 35%;"': 'style="width: 35%;"' !!}>{{-- Image Template --}}
                                    <p class="mb-0">
                                        <span class="font-w700">{{ $reservation->relationships->cloud_image_template->data->name }}</span>
                                        {!! $reservation->relationships->cloud_image_template->data->version != null ? '<span class="badge text-primary-darker bg-primary-lighter ml-2">Version '.$reservation->relationships->cloud_image_template->data->version.'</span>': '' !!}
                                        <span class="badge ml-2 badge-secondary">{{ $reservation->relationships->cloud_image_template->data->short_id }}</span><br />
                                        <span class="">{{ config('trylabs.portal.reservation.url') }}/r/{{ $reservation->short_id }}</span><br />
                                    </p>
                                </td>
                                <td class="p-3" {!! $loop->first ? 'style="border-top: none; width: 40%;"': 'style="width: 40%;"' !!}>{{-- Reservation Details --}}

                                    <span class="font-w700">{{ \Carbon\Carbon::parse($reservation->timestamps->created_at)->timezone($request->user()->timezone)->format('l, M j g:i A T') }}</span>
                                    <span class="badge ml-2 badge-success">{{ number_format($reservation->qty_credits, 1) }} Credits</span>
                                    <span class="badge ml-2 badge-secondary">{{ $reservation->short_id }}</span><br />
                                    @if($reservation->timestamps->terminated_at == null)
                                        Expires {{ \Carbon\Carbon::parse($reservation->timestamps->expires_at)->timezone($request->user()->timezone)->format('D') }} at {{ \Carbon\Carbon::parse($reservation->timestamps->expires_at)->timezone($request->user()->timezone)->format('g:i A T') }} <span class="text-success">({{ \Carbon\Carbon::parse($reservation->timestamps->expires_at)->diffForHumans() }})</span><br />
                                    @elseif($reservation->timestamps->terminated_at != null)
                                        Terminated {{ \Carbon\Carbon::parse($reservation->timestamps->terminated_at)->timezone($request->user()->timezone)->format('M j g:i A T') }} <span class="text-muted">({{ \Carbon\Carbon::parse($reservation->timestamps->terminated_at)->diffForHumans() }})</span><br />
                                    @endif
                                    {{-- Usage Credits --}}
                                    {{--
                                    <span class="small">
                                        {{ $image_template->relationships->cloud_image_size->data->name }}&nbsp;-&nbsp;
                                        {{ $image_template->relationships->cloud_image_size->data->vcpu }} vCPU&nbsp;|&nbsp;
                                        {{ $image_template->relationships->cloud_image_size->data->memory }}GB Memory&nbsp;|&nbsp;
                                        {{ $image_template->relationships->cloud_image_size->data->disk }}GB Disk
                                    </span>
                                    --}}
                                </td>

                                <td class="p-3 text-right" {!! $loop->first ? 'style="border-top: none; width: 25%;"': 'style="width: 25%;"' !!}>{{-- Actions --}}
                                    <div class="btn-group btn-group-lg">
                                        <a class="btn btn-success" target="_blank" href="{{ config('trylabs.portal.reservation.url') }}/r/{{ $reservation->short_id }}" data-toggle="tooltip" data-placement="top" title data-original-title="Access Lab">
                                            <i class="si si-control-play"></i>
                                        </a>
                                        {{--
                                        <a class="btn btn-outline-info" href="#modal-extend-{{ $reservation->short_id }}" data-toggle="modal" v-b-tooltip.hover title="Extend Reservation" >
                                            <i class="si si-hourglass"></i>
                                        </a>
                                        --}}
                                        @if(\Carbon\Carbon::parse($reservation->timestamps->created_at)->addSeconds(90) < now())
                                            <a class="btn btn-outline-info border-right-0" href="#modal-snapshot-{{ $reservation->short_id }}" data-toggle="modal" {{--v-b-tooltip.hover title="Save as Image Template"--}}>
                                                <i class="si si-disc"></i>
                                            </a>
                                            <a class="btn btn-outline-danger" href="#modal-terminate-{{ $reservation->short_id }}" data-toggle="modal" v-b-tooltip.hover title="Terminate Lab">
                                                <i class="si si-trash"></i>
                                            </a>
                                        @else
                                            <a class="btn btn-outline-secondary border-right-0" href="#" v-b-tooltip.hover title="Snapshot Unavailable until Reservation is Provisioned (after 90 seconds)">
                                                <i class="si si-disc"></i>
                                            </a>
                                            <a class="btn btn-outline-secondary" href="#" v-b-tooltip.hover title="Terminate Unavailable until Reservation is Provisioned (after 90 seconds)">
                                                <i class="si si-trash"></i>
                                            </a>
                                        @endif
                                    </div>
                                </td>
                            </tr>

                        @endif

                    @endforeach
                </tbody>
            </table>

        </div>

    @else

        @if(!empty($historical_reservations->data))


            <div class="alert alert-info">

                <h3 class="alert-heading font-size-h4 my-2">No Active Reservations</h3>

                <div class="d-flex">

                    <div class="flex-fill">
                        <p class="mb-1">
                            It doesn't look like you have any active reservations. If you were looking for a recent reservation, it may have expired and can be found in the historical reservations table below. If you want to save your work from a reservation, you can create a snapshot of your active reservation to create a new image template that can be launched for future reservations without needing to leave the virtual machine running all the time.
                        </p>
                    </div>

                    <div class="flex-00-auto ml-5 mr-4">
                        <a class="btn btn-info btn-lg font-w400" data-toggle="modal" href="#modal-create-reservation">Create a Lab Reservation</a>
                    </div>
                </div>
            </div>

        @else

            <div class="alert alert-info">

                <h3 class="alert-heading font-size-h4 my-2">No Reservations Found</h3>

                <div class="d-flex">

                    <div class="flex-fill">
                        <p class="mb-1">
                            It doesn't look like you have created a lab reservation yet. A lab reservation allows you to access a virtual machine that was deployed using an image template in a sandbox environment. After you create an image template, you will be able to create a new lab reservation.
                        </p>
                    </div>

                    <div class="flex-00-auto ml-5 mr-4">
                        <a class="btn btn-info btn-lg font-w400" data-toggle="modal" href="#modal-create-reservation">Create a Lab Reservation</a>
                    </div>
                </div>
            </div>

        @endif

    @endif

    <div class="row">

        <div class="col-sm-4">
            <div class="block block-bordered block-rounded">
                <div class="block-header border-bottom">
                    <h3 class="block-title">How To Access A Reservation</h3>
                </div>
                <div class="block-content block-content-full">
                    <div data-toggle="tooltip" data-placement="top" title data-original-title="Coming Soon" style="width: 100%; color: #fff; background-color: #111; text-align: center;">
                        <i class="fab fa-youtube fa-3x my-6"></i>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-8">
            <div class="block block-bordered block-rounded">
                <div class="block-header border-bottom">
                    <h3 class="block-title">SSH Key for Accessing Virtual Machines</h3>
                </div>
                <div class="block-content block-content-full">
                    <p>
                        You will usually access your lab reservations using your web browser with the HTML5 <a target="_blank" href="https://guacamole.apache.org">Apache Guacamole client</a> for VNC or web-based SSH access. Alternatively, if you would like to log in to a virtual machine using SSH from your local machine, you will need to download the SSH key pair that we generated for you and configure your SSH/SFTP client to use the key pair.
                    </p>
                    <div class="col-12 text-center my-4">
                        <a target="_blank" class="btn btn-outline-success mr-2" href="{{ route('user.profile.ssh.key.download') }}">Download SSH Key Pair</a>
                        <a target="_blank" class="btn btn-outline-info" href="{{ route('support.kb.trylabs.reservations.ssh-keys')}}">View Instructions</a>
                    </div>
                </div>
            </div>
        </div>

    </div>

    @if(!empty($historical_reservations->data))

        <div class="block block-bordered block-rounded">
            <div class="block-header">
                <h3 class="block-title">Historical Reservations</h3>
            </div>
            <div class="block-content">

                <table class="table table-condensed">
                    <thead>
                        <tr>
                            <th style="width: 10%;">ID</th>
                            <th style="width: 25%;">Started</th>
                            <th style="width: 20%;">Terminated</th>
                            <th style="width: 35%;">Image Template</th>
                            <th class="text-right" style="width: 10%;"># Credits</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($historical_reservations->data as $reservation)
                            @if($reservation->timestamps->terminated_at != null)
                                <tr class="small">
                                    <td>{{ $reservation->short_id }}</td>
                                    <td>{{ \Carbon\Carbon::parse($reservation->timestamps->created_at)->timezone($request->user()->timezone)->format('D, M j, Y g:i A T') }}</td>
                                    <td>{{ \Carbon\Carbon::parse($reservation->timestamps->terminated_at)->timezone($request->user()->timezone)->format('D g:i A') }} ({{ $reservation->count_mins }} Mins)</td>
                                    <td>
                                        {{ $reservation->relationships->cloud_image_template->data->name }}
                                        {!! $reservation->relationships->cloud_image_template->data->version != null ? '<span class="badge text-primary-darker bg-primary-lighter ml-2">Version '.$reservation->relationships->cloud_image_template->data->version.'</span>': '' !!}
                                        <span class="badge ml-2 badge-secondary">{{ $reservation->relationships->cloud_image_template->data->short_id }}</span><br />
                                    </td>
                                    <td class="text-right">
                                        {{ number_format($reservation->qty_credits, 1) }} Credits
                                    </td>
                                </tr>
                            @endif
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

    @endif

@endsection

@section('modals')
    @include('user.reservations._modals.create_reservation')

    @foreach($active_reservations->data as $reservation)
        {{--@include('user.reservations._modals.extend')--}}
        @include('user.reservations._modals.snapshot')
        @include('user.reservations._modals.terminate')
    @endforeach

@endsection
