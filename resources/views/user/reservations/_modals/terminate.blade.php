<!-- Reservation Category Modal -->
<div class="modal fade" id="modal-terminate-{{ $reservation->short_id }}" tabindex="-1" role="dialog" aria-labelledby="modal-terms" aria-hidden="true">
    <form method="post" action="{{ route('user.reservations.terminate', ['tenant_reservation_id' => $reservation->id]) }}">
        @csrf

        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="block block-transparent mb-0">
                    <div class="block-header bg-light border-bottom">
                        <h3 class="block-title">Terminate Reservation<span class="badge ml-2 badge-secondary">{{ $reservation->short_id }}</span></h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="fa fa-fw fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content">

                        <input type="hidden" name="terminate_action" value="discard" />

                        <p>
                            When you terminate a reservation, your virtual machine will be destroyed and all changes will be lost. Any unused time will be credited back to your account.<br />
                            <br />
                            <strong>Want to save your changes?</strong> You should snapshot your reservation instead so you can choose to replace the existing image template or save as a new image template.<br />
                            <br />
                        </p>

                    </div>
                    <div class="block-content block-content-full text-right bg-light border-top">
                        <button type="button" class="btn btn-outline-secondary mr-2" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-danger">Discard Changes and Terminate</button>
                    </div>

                </div>
            </div>
        </div>
    </form>
</div>
<!-- END Terms Modal -->
