{{--
<!-- Reservation Category Modal -->
<div class="modal fade" id="modal-extend-{{ $reservation->short_id }}" tabindex="-1" role="dialog" aria-labelledby="modal-extend-{{ $reservation->short_id }}" aria-hidden="true">
    <form method="get" action="{{ route('user.reservations.extend', ['tenant_reservation_id', $reservation->id]) }}">

        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="block block-transparent mb-0">
                    <div class="block-header bg-light border-bottom">
                        <h3 class="block-title">Extend Reservation<span class="badge ml-2 badge-secondary">{{ $reservation->short_id }}</span></h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="fa fa-fw fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content">

                    </div>
                    <div class="block-content block-content-full text-right bg-light border-top">
                        <button type="button" class="btn btn-outline-secondary mr-2" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-success">Extend Reservation</button>
                    </div>

                </div>
            </div>
        </div>
    </form>
</div>
<!-- END Terms Modal -->

--}}
