<!-- Snapshot Modal -->
<div class="modal fade" id="modal-snapshot-{{ $reservation->short_id }}" tabindex="-1" role="dialog" aria-labelledby="modal-snapshot-{{ $reservation->short_id }}" aria-hidden="true">
    <form method="post" action="{{ route('user.reservations.terminate', ['tenant_reservation_id' => $reservation->id]) }}">
        @csrf

        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="block block-transparent mb-0">
                    <div class="block-header bg-light border-bottom">
                        <h3 class="block-title">Snapshot Reservation<span class="badge ml-2 badge-secondary">{{ $reservation->short_id }}</span></h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="fa fa-fw fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content">

                        <div class="row mb-3">

                            <div class="col-sm-12 col-md-6">{{-- Redeem Plan Hours --}}
                                <div class="custom-control custom-block custom-control-warning mb-1">
                                    <input type="radio" class="custom-control-input" id="modal_action_replace_{{ $reservation->short_id }}" name="terminate_action" value="replace">
                                    <label class="custom-control-label" for="modal_action_replace_{{ $reservation->short_id }}">
                                        <span class="d-block font-w400 text-center my-3">
                                            {{--<i class="icon-Stopwatch-2" style="font-size: 3em;"></i><br />--}}
                                            <div class="font-size-h3 font-w600 mt-1 mb-3">Replace Image Template</div>
                                            <p class="mb-3">
                                                Any Lab Access Tokens (shareable links) that use this template will use the updated image template (hint: breaking changes). If you do not want to affect existing Lab Access Tokens, you should save as a new image template.
                                            </p>
                                        </span>
                                    </label>

                                    <span class="custom-block-indicator">
                                        <i class="fa fa-check"></i>
                                    </span>
                                </div>
                            </div>{{-- END Redeem Plan Hours --}}

                            <div class="col-sm-12 col-md-6">{{-- End Users Pay Per Lab --}}
                                <div class="custom-control custom-block custom-control-info mb-1">
                                    <input type="radio" class="custom-control-input" id="modal_action_save_{{ $reservation->short_id }}" name="terminate_action" value="save">
                                    <label class="custom-control-label" for="modal_action_save_{{ $reservation->short_id }}">
                                        <span class="d-block font-w400 text-center my-3">
                                            {{--<i class="icon-Cash-Register" style="font-size: 3em;"></i><br />--}}
                                            {{-- TODO disable this option if no image slots remaining --}}
                                            <div class="font-size-h3 font-w600 mt-1 mb-3">Save as New Image Template</div>
                                            <p class="mb-3">
                                                If you save as a new image template, one of your image template slots will be consumed. After the new image template has been created, you will be able to launch a new reservation or create lab access tokens.
                                            </p>
                                        </span>
                                    </label>

                                    <span class="custom-block-indicator">
                                        <i class="fa fa-check"></i>
                                    </span>
                                </div>
                            </div>{{-- END Users Pay Per Lab --}}

                        </div>{{-- END row --}}


                    </div>
                    <div class="block-content block-content-full text-right bg-light border-top">
                        <button type="button" class="btn btn-outline-secondary mr-2" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-success">Snapshot and Terminate</button>
                    </div>

                </div>
            </div>
        </div>
    </form>
</div>
<!-- END Snapshot Modal -->
