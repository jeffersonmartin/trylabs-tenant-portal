@extends('_layouts.tenant_lite')

@section('page_title', 'My Profile')
@section('page_actions')
    <a class="btn btn-outline-primary mr-2" href="{{ route('user.billing.index') }}">Manage Subscription & Payment Method</a>
    <a class="btn btn-outline-info mr-2" href="#modal-change-password" data-toggle="modal">Change Password</a>
@endsection

@section('content')

    @if($errors->any())
        <div class="alert alert-warning">
            <strong>You'll need to make a few changes before your magic spells will work here.</strong>
            <ul class="mb-0">
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="row">
        <div class="col-3">
            <div class="block block-rounded block-bordered">
                <div class="block-header border-bottom bg-body-light">
                    <h3 class="block-title">Profile Picture</h3>
                </div>
                <div class="block-content text-center">
                    <img class="img-avatar img-avatar128 mr-2" src="https://www.gravatar.com/avatar/{!! md5($request->user()->email) !!}?d={{ asset('images/avatar-wizard-lg.png') }}" alt="Avatar">
                    {{--<img class="img-avatar img-avatar128 mx-3 my-3" src="{{ asset('images/avatar-wizard-lg.png') }}" />--}}

                    <p>
                        You can update your profile picture on <a href="http://www.gravatar.com" target="_blank">Gravatar</a>.
                    </p>
                </div>
            </div>
        </div>
        <div class="col-9">
            <form method="post" action="{{ route('user.profile.update') }}">
                @csrf
                @method('PATCH')
                <div class="block block-rounded block-bordered">
                    <div class="block-header border-bottom bg-body-light">
                        <h3 class="block-title">Account Information<span class="badge badge-secondary ml-2">{{ $user_account->data->short_id }}</span></h3>
                        <div class="block-options">
                            <a class="mr-2" data-toggle="tooltip" data-placement="top" title data-original-title="For billing and security reasons, please email support@trylabs.cloud to change your email address."><i class="si si-question js-tooltip-enabled text-warning" ></i></a>
                            {{ $user_account->data->email }}
                        </div>
                    </div>
                    <div class="block-content pb-3">
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="example-text-input">First Name</label>
                                    <input type="text" class="form-control" id="first_name" name="first_name" value="{{ $user_account->data->first_name }}">
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="example-text-input">Last Name</label>
                                    <input type="text" class="form-control" id="last_name" name="last_name" value="{{ $user_account->data->last_name }}">
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group">
                                    <label for="example-text-input">Organization Name</label>
                                    <input type="text" class="form-control" id="organization_name" name="organization_name" placeholder="Not Associated with Organization" value="{{ $user_account->data->organization_name }}">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="block-content border-top bg-body-light text-right pt-3 pb-3">
                        <a class="btn btn-outline-secondary mr-1" href="{{ route('user.dashboard.index') }}">Cancel</a>
                        <button type="submit" class="btn btn-success">Save Changes</button>
                    </div>
                </div>

            </form>

            <div class="block block-bordered block-rounded">
                <div class="block-header border-bottom">
                    <h3 class="block-title">SSH Key for Accessing Virtual Machines</h3>
                </div>
                <div class="block-content block-content-full">
                    <p>
                        You will usually access your lab reservations using your web browser with the HTML5 <a target="_blank" href="https://guacamole.apache.org">Apache Guacamole client</a> for VNC or web-based SSH access. Alternatively, if you would like to log in to a virtual machine using SSH from your local machine, you will need to download the SSH key pair that we generated for you and configure your SSH/SFTP client to use the key pair.
                    </p>
                    <div class="col-12 text-center my-4">
                        <a target="_blank" class="btn btn-outline-success mr-2" href="{{ route('user.profile.ssh.key.download') }}">Download SSH Key Pair</a>
                        <a target="_blank" class="btn btn-outline-info" href="{{ route('support.kb.trylabs.reservations.ssh-keys')}}">View Instructions</a>
                    </div>
                </div>
            </div>

            <div class="block block-rounded block-bordered">
                <div class="block-content d-flex pb-4">
                    <div class="flex-fill">
                        <p class="mr-4 mb-0">
                            If you delete your account, your subscription will be cancelled immediately and you will not be charged again. We will delete all of your image templates and configuration data after 10 days, just in case you decide to resubscribe and you don't want to lose your work.
                        </p>
                    </div>
                    <div class="flex-00-auto">
                        <a class="btn btn-outline-danger my-3" href="#modal-delete-account" data-toggle="modal">Delete Account</a>
                    </div>
                </div>
            </div>

        </div>
    </div>

@endsection

@section('modals')
    @include('user.profile._modals.change_password')
    @include('user.profile._modals.delete_account')
@endsection
