<!-- Change Password Modal -->
<div class="modal fade" id="modal-change-password" tabindex="-1" role="dialog" aria-labelledby="modal-change-password" aria-hidden="true">
    <form method="post" action="{{ route('user.profile.update') }}">
        @csrf
        @method('PATCH')

        <div class="modal-dialog modal-md modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="block block-transparent mb-0">
                    <div class="block-header bg-light border-bottom">
                        <h3 class="block-title">Change Password</span></h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="fa fa-fw fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content">

                        <div class="alert alert-info">
                            Your new password must be at least 8 characters and contain at least one uppercase letter, lowercase letter, number, and symbol (#?!@$%^&*-).
                        </div>

                        <div class="form-group">
                            <label for="example-text-input">Current Password</label>
                            <input type="password" class="form-control" id="current_password" name="current_password">
                        </div>

                        <div class="form-group">
                            <label for="example-text-input">New Password</label>
                            <input type="password" class="form-control" id="new_password" name="new_password">
                        </div>

                        <div class="form-group">
                            <label for="example-text-input">Confirm New Password</label>
                            <input type="password" class="form-control" id="confirm_new_password" name="confirm_new_password">
                        </div>

                    </div>
                    <div class="block-content block-content-full text-right bg-light border-top mt-3">
                        <button type="button" class="btn btn-outline-secondary mr-2" data-dismiss="modal">Cancel</button>
                        <button type="button" readonly="readonly" class="btn btn-success" data-toggle="tooltip" data-placement="top" title data-original-title="Coming Soon">Change Password</button>
                        {{-- TODO Fix Change Password --}}
                    </div>

                </div>
            </div>
        </div>
    </form>
</div>
<!-- END Change Password Modal -->
