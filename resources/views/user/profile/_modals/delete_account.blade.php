<!-- Change Password Modal -->
<div class="modal fade" id="modal-delete-account" tabindex="-1" role="dialog" aria-labelledby="modal-delete-account" aria-hidden="true">
    <form method="post" action="{{ route('user.profile.update') }}">
        @csrf
        @method('PATCH')

        <div class="modal-dialog modal-md modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="block block-themed block-transparent mb-0">
                    <div class="block-header bg-danger">
                        <h3 class="block-title">Delete Account</span></h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="fa fa-fw fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content">

                        <div class="alert alert-danger mb-3">
                            If you delete your account, your subscription will be cancelled immediately and you will not be charged again.<br />
                            <br />
                            We will delete all of your image templates and configuration data after 10 days, just in case you decide to resubscribe and you don't want to lose your work.
                        </div>

                        <div class="form-group">
                            <label for="example-text-input">Enter Password to Confirm</label>
                            <input type="password" class="form-control" id="password" name="password">
                        </div>

                    </div>
                    <div class="block-content block-content-full text-right bg-light border-top mt-3">
                        <button type="button" class="btn btn-outline-secondary mr-2" data-dismiss="modal">Cancel</button>
                        <button type="button" readonly="readonly" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title data-original-title="Coming Soon">Delete Account</button>
                        {{-- TODO Fix Change Password --}}
                    </div>

                </div>
            </div>
        </div>
    </form>
</div>
<!-- END Change Password Modal -->
