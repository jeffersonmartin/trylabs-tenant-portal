@extends('_layouts.tenant_lite')

@section('page_title', 'Billing Dashboard')
@section('page_actions')

@endsection

@section('content')

    <div class="row">

        <!-- Pricing Tables -->
                <div class="col-md-6 col-xl-3">
                    <!-- Imagination Plan -->
                    <div class="block block-rounded block-bordered block-themed text-center">
                        <div class="block-header bg-primary-light">
                            <h3 class="block-title">
                                <i class="icon-Idea-3" style="font-size: 3em;"></i><br />
                                <span class="font-w700">Imagination</span><br />
                                <small>Free Feature Preview</small>
                            </h3>
                        </div>
                        <div class="block-content bg-white">
                            <div class="py-2">
                                <p class="h1 font-w700 mb-2">FREE</p>
                                <p class="h6 text-muted">hands-on feature preview</p>
                            </div>
                        </div>
                        <div class="block-content" style="min-height: 270px;">
                            <div class="py-1">
                                <p class="mb-0">
                                    You have access to all of the features with demo data to explore the platform and decide if it's right for your needs.<br />
                                    <br />
                                    You will not be able to power up virtual machines until you upgrade to a paid plan.<br />
                                </p>
                            </div>
                        </div>
                        <div class="block-content block-content-full bg-white">
                            @if($chargebee_active_subscription->plan->id == 'try1u-plan-free-mo')
                                <span class="btn btn-secondary btn-lg disabled px-4">
                                    <i class="fa fa-check mr-2"></i>Active Plan
                                </span>
                            @else
                                <a class="btn btn-outline-danger btn-lg px-4" href="mailto:support@trylabs.cloud?s=Request to Downgrade to Free Plan - Customer ID {{ $chargebee_customer->id}}" v-b-tooltip.hover title="We are making some improvements to self-service plan changes. Please email support@trylabs.cloud if you want to downgrade your plan."><i class="fa fa-arrow-down mr-2"></i>Downgrade</a>
                            @endif
                        </div>
                    </div>
                    <!-- END Imagination Plan -->
                </div>
                <div class="col-md-6 col-xl-3">
                    <!-- Werewolf Plan -->
                    <div class="block block-rounded block-bordered block-themed text-center">
                        <div class="block-header bg-primary">
                            <h3 class="block-title">
                                <i class="icon-Hat" style="font-size: 3em;"></i><br />
                                <span class="font-w700">Magician</span><br />
                                <small>~2-3 Lab Days per Month</small>
                            </h3>
                        </div>
                        <div class="block-content bg-white">
                            <div class="py-2">
                                <p class="h1 font-w700 mb-2">$15</p>
                                <p class="h6 text-muted">per month</p>
                            </div>
                        </div>
                        <div class="block-content" style="min-height: 270px;">
                            <div class="py-1">
                                <p>
                                    <strong>20</strong> Hours Usage
                                </p>
                                <p>
                                    <strong>5</strong> Image Templates
                                </p>
                                <p>
                                    Access to Community Forums
                                </p>
                                <p>
                                    Email Support
                                </p>
                            </div>
                        </div>
                        <div class="block-content block-content-full bg-white">
                            @if($chargebee_active_subscription->plan->id == 'try1u-plan-20-5-mo' || $chargebee_active_subscription->plan->id == 'try1u-plan-20-5-yr')
                                <span class="btn btn-secondary btn-lg disabled px-4">
                                    <i class="fa fa-check mr-2"></i>Active Plan
                                </span>
                            @elseif($chargebee_active_subscription->plan->id == 'try1u-plan-free-mo')
                                <button data-toggle="modal" data-target="#modal-upgrade-plan-magician" class="btn btn-success btn-lg px-4"><i class="fa fa-arrow-up mr-2"></i>Upgrade</button>
                            {{--
                            @elseif($chargebee_active_subscription->plan->id == 'try1u-plan-xx-xx-mo' || $chargebee_active_subscription->plan->id == 'try1u-plan-xx-xx-yr')
                                <button type="button" class="btn btn-outline-success btn-lg px-4" readonly="readonly" v-b-tooltip.hover title="We are making some improvements to self-service plan changes. Please email support@trylabs.cloud if you want to upgrade your plan."><i class="fa fa-arrow-up mr-2"></i>Upgrade</button>
                            --}}
                            @elseif($chargebee_active_subscription->plan->id == 'try1u-plan-80-10-mo' || $chargebee_active_subscription->plan->id == 'try1u-plan-80-10-yr' || $chargebee_active_subscription->plan->id == 'try1u-plan-250-25-mo' || $chargebee_active_subscription->plan->id == 'try1u-plan-250-25-mo')
                                <a class="btn btn-outline-danger btn-lg px-4" href="mailto:support@trylabs.cloud?s=Request to Downgrade to Magician Plan - Customer ID {{ $chargebee_customer->id}}" v-b-tooltip.hover title="We are making some improvements to self-service plan changes. Please email support@trylabs.cloud if you want to downgrade your plan."><i class="fa fa-arrow-down mr-2"></i>Downgrade</a>
                            @endif
                        </div>
                    </div>
                    <!-- END Werewolf Plan -->
                </div>
                <div class="col-md-6 col-xl-3">
                    <!-- Wizard Plan -->
                    <div class="block block-rounded block-bordered block-themed text-center">
                        <div class="block-header bg-primary-dark">
                            <h3 class="block-title">
                                <i class="icon-Witch-Hat" style="font-size: 3em;"></i><br />
                                <span class="font-w700">Wizard</span><br />
                                <small>~10 Lab Days per Month</small>
                            </h3>
                        </div>
                        <div class="block-content bg-white">
                            <div class="py-2">
                                <p class="h1 font-w700 mb-2">$50</p>
                                <p class="h6 text-muted">per month</p>
                            </div>
                        </div>
                        <div class="block-content" style="min-height: 270px;">
                            <div class="py-1">
                                <p>
                                    <strong>80</strong> Hours Usage
                                </p>
                                <p>
                                    <strong>10</strong> Image Templates
                                </p>
                                <p>
                                    Access to Community Forums
                                </p>
                                <p>
                                    Email Support
                                </p>
                            </div>
                        </div>
                        <div class="block-content block-content-full bg-white">
                            @if($chargebee_active_subscription->plan->id == 'try1u-plan-80-10-mo' || $chargebee_active_subscription->plan->id == 'try1u-plan-80-10-yr')
                                <span class="btn btn-secondary btn-lg disabled px-4">
                                    <i class="fa fa-check mr-2"></i>Active Plan
                                </span>
                            @elseif($chargebee_active_subscription->plan->id == 'try1u-plan-free-mo')
                                <button data-toggle="modal" data-target="#modal-upgrade-plan-wizard" class="btn btn-success btn-lg px-4"><i class="fa fa-arrow-up mr-2"></i>Upgrade</button>
                            @elseif($chargebee_active_subscription->plan->id == 'try1u-plan-20-5-mo' || $chargebee_active_subscription->plan->id == 'try1u-plan-20-5-yr')
                                <a class="btn btn-success btn-lg px-4" href="mailto:support@trylabs.cloud?s=Request to Upgrade to Wizard Plan - Customer ID {{ $chargebee_customer->id}}" v-b-tooltip.hover title="We are making some improvements to self-service plan changes. Please email support@trylabs.cloud if you want to upgrade your plan."><i class="fa fa-arrow-up mr-2"></i>Upgrade</a>
                            @elseif($chargebee_active_subscription->plan->id == 'try1u-plan-250-25-mo' || $chargebee_active_subscription->plan->id == 'try1u-plan-250-25-mo')
                                <a class="btn btn-outline-danger btn-lg px-4" href="mailto:support@trylabs.cloud?s=Request to Downgrade to Wizard Plan - Customer ID {{ $chargebee_customer->id}}" v-b-tooltip.hover title="We are making some improvements to self-service plan changes. Please email support@trylabs.cloud if you want to downgrade your plan."><i class="fa fa-arrow-down mr-2"></i>Downgrade</a>
                            @endif
                        </div>
                    </div>
                    <!-- END Wizard Plan -->
                </div>
                <div class="col-md-6 col-xl-3">
                    <!-- Sorcerer Plan -->
                    <div class="block block-rounded block-bordered block-themed text-center">
                        <div class="block-header bg-primary-darker">
                            <h3 class="block-title">
                                <i class="icon-Wizard" style="font-size: 3em;"></i><br />
                                <span class="font-w700">Sorcerer</span><br />
                                <small>Lab'ing Full Time</small>
                            </h3>
                        </div>
                        <div class="block-content bg-white">
                            <div class="py-2">
                                <p class="h1 font-w700 mb-2">$150</p>
                                <p class="h6 text-muted">per month</p>
                            </div>
                        </div>
                        <div class="block-content" style="min-height: 270px;">
                            <div class="py-1">
                                <p>
                                    <strong>250</strong> Hours Usage
                                </p>
                                <p>
                                    <strong>25</strong> Image Templates
                                </p>
                                <p>
                                    Access to Community Forums
                                </p>
                                <p>
                                    Priority Email Support
                                </p>
                                <p>
                                    Feature Roadmap Voting
                                </p>
                            </div>
                        </div>
                        <div class="block-content block-content-full bg-white">
                            @if($chargebee_active_subscription->plan->id == 'try1u-plan-250-25-mo' || $chargebee_active_subscription->plan->id == 'try1u-plan-250-25-yr')
                                <span class="btn btn-secondary btn-lg disabled px-4">
                                    <i class="fa fa-check mr-2"></i>Active Plan
                                </span>
                            @elseif($chargebee_active_subscription->plan->id == 'try1u-plan-free-mo')
                                <button data-toggle="modal" data-target="#modal-upgrade-plan-sorcerer" class="btn btn-success btn-lg px-4"><i class="fa fa-arrow-up mr-2"></i>Upgrade</button>
                            @elseif($chargebee_active_subscription->plan->id == 'try1u-plan-20-5-mo' || $chargebee_active_subscription->plan->id == 'try1u-plan-20-5-yr' || $chargebee_active_subscription->plan->id == 'try1u-plan-80-10-mo' || $chargebee_active_subscription->plan->id == 'try1u-plan-80-10-yr')
                                <a class="btn btn-success btn-lg px-4" href="mailto:support@trylabs.cloud?s=Request to Upgrade to Sorcerer Plan - Customer ID {{ $chargebee_customer->id}}" v-b-tooltip.hover title="We are making some improvements to self-service plan changes. Please email support@trylabs.cloud if you want to upgrade your plan."><i class="fa fa-arrow-up mr-2"></i>Upgrade</a>
                            {{--
                            @elseif($chargebee_active_subscription->plan->id == 'try1u-plan-250-25-mo' || $chargebee_active_subscription->plan->id == 'try1u-plan-250-25-mo')
                                <button type="button" class="btn btn-outline-danger btn-lg px-4" readonly="readonly" v-b-tooltip.hover title="We are making some improvements to self-service plan changes. Please email support@trylabs.cloud if you want to downgrade your plan."><i class="fa fa-arrow-down mr-2"></i>Downgrade</button>
                            --}}
                            @endif
                        </div>
                    </div>
                    <!-- END Sorcerer Plan -->
                </div>
        <!-- END Pricing Tables -->

    </div>

    <h2 class="content-heading">
        My Subscription
    </h2>

    <div class="row">

        <b-col sm="4">
            <div class="block block-bordered">
                <div class="block-content text-center block-content-full" style="min-height: 180px;">

                    {{--<h1 class="h1 text-success mt-2">${{ $chargebee_customer->active_subscription->plan->price }} <span class="small">per {{ $chargebee_customer->active_subscription->plan->period_unit }}</span></h1>--}}
                    {{-- TODO Plan value is 0, total comes from addons. Need to find a way to calculate --}}

                    <div style="min-height: 100px;">
                        <h4 class="mb-1">{{ $chargebee_active_subscription->plan->invoice_name }}</h4>
                        <p class="mb-0">
                            I.D. {{ $chargebee_active_subscription->id }}<br />
                            @if($chargebee_active_subscription->status == 'in_trial')
                                Trial Ends on {{ \Carbon\Carbon::parse($chargebee_active_subscription->trial_end)->toFormattedDateString() }}
                            @elseif($chargebee_active_subscription->status == 'active')
                                Renews on {{ \Carbon\Carbon::parse($chargebee_active_subscription->next_billing_at)->toFormattedDateString() }}
                            @elseif($chargebee_active_subscription->status == 'non_renewing')
                                Cancels on {{ \Carbon\Carbon::parse($chargebee_active_subscription->current_term_end)->toFormattedDateString() }}
                            @elseif($chargebee_active_subscription->status == 'cancelled')
                                Cancelled on {{ \Carbon\Carbon::parse($chargebee_active_subscription->cancelled_at)->toFormattedDateString() }}
                            @endif
                        </p>
                    </div>
                    {{--
                    <a class="btn btn-outline-danger my-0" href="mailto:support@trylabs.cloud?s=Request to Cancel Plan">Cancel Plan</a>
                    --}}
                    @if($chargebee_active_subscription->status == 'in_trial' || $chargebee_active_subscription->status == 'active')
                        <a class="btn btn-outline-danger my-0" href="#modal-cancel-plan" data-toggle="modal">Cancel Plan</a>
                    @endif

                </div>
            </div>
        </b-col>

        <b-col sm="4">
            <div class="block block-bordered">
                <div class="block-content text-center block-content-full" style="min-height: 180px;">

                    @if($chargebee_customer->card_status != 'no_card')
                        <div class="row" style="min-height: 100px;">
                            <div class="col-5 text-right">
                                @if($chargebee_payment_method->card->brand == 'visa')
                                    <i class="fab fa-cc-visa fa-4x my-1"></i>
                                @elseif($chargebee_payment_method->card->brand == 'mastercard')
                                    <i class="fab fa-cc-mastercard fa-4x my-1"></i>
                                @elseif($chargebee_payment_method->card->brand == 'american_express')
                                    <i class="fab fa-cc-amex fa-4x my-1"></i>
                                @elseif($chargebee_payment_method->card->brand == 'discover')
                                    <i class="fab fa-cc-discover fa-4x my-1"></i>
                                @else
                                    <i class="si si-credit-card fa-4x my-1"></i>
                                @endif
                            </div>
                            <div class="col-7 text-left">
                                <h5 class="h5 my-2">
                                    <span class="text-secondary">Ending in </span>{{ $chargebee_payment_method->card->last4 }}<br />
                                    <small class="text-gray-dark">Expires {{ $chargebee_payment_method->card->expiry_month}}/{{ $chargebee_payment_method->card->expiry_year }}</small>
                                </h5>
                            </div>
                        </div>

                        <form method="post" action="{{ route('user.billing.payment.update') }}">
                            @csrf
                            @method('PATCH')
                            <button type="submit" class="btn btn-outline-info my-0">Update Payment Card</button>
                        </form>

                    @else

                        {{-- Placeholder for no card --}}
                        <div class="alert alert-info">
                            We do not have a payment card on file. You will be prompted to enter your payment card when you upgrade to a paid plan.
                        </div>

                    @endif

                </div>
            </div>
        </b-col>


        <b-col sm="4">
            <div class="block block-bordered">
                <div class="block-content text-center block-content-full" style="min-height: 180px;">
                    @if(isset($chargebee_customer->billing_address))
                        <div style="min-height: 100px;">
                            <p class="my-0">
                                {!! $chargebee_customer->first_name != null ? $chargebee_customer->first_name.' ' : '' !!}
                                {!! $chargebee_customer->last_name != null ? $chargebee_customer->last_name.'<br />' : '' !!}
                                {!! $chargebee_customer->billing_address->line1 != null ? $chargebee_customer->billing_address->line1.'<br />' : '' !!}
                                {!! $chargebee_customer->billing_address->city != null ? $chargebee_customer->billing_address->city.', ' : '' !!}
                                {!! $chargebee_customer->billing_address->state_code != null ? $chargebee_customer->billing_address->state_code.' ' : '' !!}
                                {!! $chargebee_customer->billing_address->zip != null ? $chargebee_customer->billing_address->zip.' ' : '' !!}
                                {!! $chargebee_customer->billing_address->country != null ? '('.$chargebee_customer->billing_address->country.')<br />' : '' !!}

                            </p>
                        </div>
                        <a class="btn btn-outline-info my-0" href="#" v-b-tooltip.hover title="We are making some improvements to self-service billing address changes. Please email support@trylabs.cloud if you want to change your billing address.">Update Billing Address</a>{{-- TODO --}}
                    @else

                        <div class="alert alert-info">
                            We do not have a billing address on file. You will be prompted to enter your billing address when you upgrade to a paid plan.
                        </div>

                    @endif
                </div>
            </div>
        </b-col>

        <b-col sm="12">

            <div class="block block-bordered">
                <div class="block-header">
                    <h3 class="block-title">Invoice History</h3>
                </div>
                <div class="block-content">
                    <table class="table table-condensed">
                        <thead>
                            <tr>
                                <th>Billing Date</th>
                                <th>Invoice ID</th>
                                <th>Plan</th>
                                <th>Term</th>
                                <th class="text-right">Amount</th>
                                <th>&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($chargebee_invoices as $invoice)
                                <tr>
                                    <td>{{ \Carbon\Carbon::parse($invoice->date)->toFormattedDateString() }}</td>
                                    <td>{{ $invoice->id }}</td>
                                    @foreach($invoice->line_items as $line_item)
                                        @if($line_item->entity_type == 'plan')
                                            <td>{{ $line_item->description }}</td>
                                            <td>{{ \Carbon\Carbon::parse($line_item->date_from)->toFormattedDateString() }} to {{ \Carbon\Carbon::parse($line_item->date_to)->toFormattedDateString() }}</td>
                                        @endif
                                    @endforeach
                                    <td class="text-right">${{ number_format($invoice->total/100, 2) }}</td>
                                    <td class="text-right">
                                        <a target="_blank" class="btn btn-sm btn-outline-primary" href="{{ route('user.billing.invoice.download', ['chargebee_customer_id' => $chargebee_customer->id, 'chargebee_invoice_id' => $invoice->id]) }}"><i class="fa fa-download mr-2"></i>Download PDF</a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

        </b-col>

    </div>

@endsection

@section('modals')
    @include('user.billing._modals.upgrade_plan_magician')
    @include('user.billing._modals.upgrade_plan_wizard')
    @include('user.billing._modals.upgrade_plan_sorcerer')
    @include('user.billing._modals.downgrade_plan_free')
    @include('user.billing._modals.downgrade_plan_magician')
    @include('user.billing._modals.downgrade_plan_wizard')
    @include('user.billing._modals.cancel_plan')

@endsection
