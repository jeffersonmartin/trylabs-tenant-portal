<!-- Snapshot Modal -->
<div class="modal fade" id="modal-upgrade-plan-magician" tabindex="-1" role="dialog" aria-labelledby="modal-upgrade-plan-magician" aria-hidden="true">
    <form method="post" action="{{ route('user.billing.plan.upgrade') }}">
        @csrf

        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="block block-transparent mb-0">
                    <div class="block-header bg-light border-bottom">
                        <h3 class="block-title">Upgrade to Magician Plan</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="fa fa-fw fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content">

                        <div class="alert alert-info">
                            With an annual plan, you get 2 months free (save $30) and can redeem lab credits at any time throughout the year without worrying about how many credits you have each month.<br />
                            <br />
                            When you pay month-to-month, you have the flexibility to cancel at any time, however your unused lab credit hours will expire at the end of each monthly billing cycle.
                        </div>

                        <div class="row mb-3">

                            <div class="col-sm-12 col-md-6">{{-- Redeem Plan Hours --}}
                                <div class="custom-control custom-block custom-control-info mb-1">
                                    <input type="radio" class="custom-control-input" id="try1u-plan-20-5-mo" name="plan_id" value="try1u-plan-20-5-mo">
                                    @include('auth.register.plan._partials.magician_mo')
                                    <span class="custom-block-indicator">
                                        <i class="fa fa-check"></i>
                                    </span>
                                </div>
                            </div>{{-- END Redeem Plan Hours --}}

                            <div class="col-sm-12 col-md-6">{{-- End Users Pay Per Lab --}}
                                <div class="custom-control custom-block custom-control-info mb-1">
                                    <input type="radio" class="custom-control-input" id="try1u-plan-20-5-yr" name="plan_id" value="try1u-plan-20-5-yr" checked="checked">
                                    @include('auth.register.plan._partials.magician_yr')
                                    <span class="custom-block-indicator">
                                        <i class="fa fa-check"></i>
                                    </span>
                                </div>
                            </div>{{-- END Users Pay Per Lab --}}

                        </div>{{-- END row --}}


                    </div>
                    <div class="block-content block-content-full text-right bg-light border-top">
                        <button type="button" class="btn btn-lg btn-outline-secondary mr-2" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-lg btn-success">Proceed to Checkout</button>
                    </div>

                </div>
            </div>
        </div>
    </form>
</div>
<!-- END Snapshot Modal -->
