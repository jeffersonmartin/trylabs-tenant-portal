<!-- Snapshot Modal -->
<div class="modal fade" id="modal-cancel-plan" tabindex="-1" role="dialog" aria-labelledby="modal-cancel-plan" aria-hidden="true">
    <form method="post" action="{{ route('user.billing.plan.cancel') }}">
        @csrf

        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="block block-transparent mb-0">
                    <div class="block-header bg-light border-bottom">
                        <h3 class="block-title">Cancel Plan</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="fa fa-fw fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content">

                        <div class="alert alert-info">
                            <div class="d-flex">
                                <div class="flex-fill">
                                    If still believe in us but are disappointed, we are happy to offer a personal touch and schedule a call with our CTO to discuss what we can do.
                                </div>
                                <div class="flex-00-auto">
                                    <a class="btn btn-info my-1" href="https://calendly.com/jeff-martin-cs/45min" target="_blank"><i class="si si-call-in mr-2"></i>Schedule a Call</a>
                                </div>
                            </div>
                        </div>

                        <div class="row my-5">

                            <div class="col-10 offset-1">

                                <div class="form-group">
                                    <label>Why do you want to cancel your plan?<span class="small text-muted ml-2">(Optional)</span></label>
                                    <div class="custom-control custom-checkbox custom-control-lg mb-1">
                                        <input type="checkbox" class="custom-control-input" id="reason_expensive" name="reason_expensive">
                                        <label class="custom-control-label" for="reason_expensive">Too expensive</label>
                                    </div>
                                    <div class="custom-control custom-checkbox custom-control-lg mb-1">
                                        <input type="checkbox" class="custom-control-input" id="reason_competitor" name="reason_competitor">
                                        <label class="custom-control-label" for="reason_competitor">Found another product</label>
                                    </div>
                                    <div class="custom-control custom-checkbox custom-control-lg mb-1">
                                        <input type="checkbox" class="custom-control-input" id="reason_byo" name="reason_byo">
                                        <label class="custom-control-label" for="reason_competitor">Building my own lab</label>
                                    </div>
                                    <div class="custom-control custom-checkbox custom-control-lg mb-1">
                                        <input type="checkbox" class="custom-control-input" id="reason_limiteduse" name="reason_limiteduse">
                                        <label class="custom-control-label" for="reason_limiteduse">I don't use it enough</label>
                                    </div>
                                    <div class="custom-control custom-checkbox custom-control-lg mb-1">
                                        <input type="checkbox" class="custom-control-input" id="reason_limitations" name="reason_limitations">
                                        <label class="custom-control-label" for="reason_limitations">Technical limitations</label>
                                    </div>
                                    <div class="custom-control custom-checkbox custom-control-lg mb-1">
                                        <input type="checkbox" class="custom-control-input" id="reason_bugs" name="reason_bugs">
                                        <label class="custom-control-label" for="reason_bugs">Bugs or Issues</label>
                                    </div>
                                    <div class="custom-control custom-checkbox custom-control-lg mb-1">
                                        <input type="checkbox" class="custom-control-input" id="reason_other" name="reason_other">
                                        <label class="custom-control-label" for="reason_other">Other</label>
                                    </div>
                                </div>

                                <div class="form-group mt-4">
                                    <label for="example-textarea-input">Do you have any feedback to share?<span class="small text-muted ml-2">(Optional)</span></label>
                                    <textarea class="form-control" id="cancel_feedback" name="cancel_feedback" rows="5"></textarea>
                                </div>

                                <p class="mt-3 text-danger">
                                    If you cancel your plan, you will be able to access your image templates and reservations until the last day of your current billing term. We will delete your image templates 10 days after your billing term ends unless you re-subscribe.
                                </p>

                            </div>

                        </div>


                    </div>
                    <div class="block-content block-content-full text-right bg-light border-top">
                        <button type="button" class="btn btn-lg btn-outline-secondary mr-2" data-dismiss="modal">Keep My Plan</button>
                        <button type="submit" class="btn btn-lg btn-danger">Cancel Plan and Delete All Images</button>
                    </div>

                </div>
            </div>
        </div>
    </form>
</div>
<!-- END Snapshot Modal -->
