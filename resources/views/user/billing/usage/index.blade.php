@extends('_layouts.tenant_lite')

@section('page_title', 'Usage Dashboard')
@section('page_actions')
    <a class="btn btn-outline-info mr-2" href="#" data-toggle="tooltip" data-placement="top" title data-original-title="Coming Soon">
        <i class="si si-doc mr-2"></i>Export as CSV
    </a>
@endsection

@section('content')

    <div class="alert alert-warning">
        <div class="d-flex">
            <div class="flex-00-auto mt-2">
                <i class="si si-chemistry fa-2x mr-3 mt-2"></i>
            </div>
            <div class="flex-fill">
                <strong>Redesign In Progress</strong>. We are collecting a lot of great metrics and are rebuilding the usage dashboard to provide you with an amazing analytics experience and pivotable data set. In the mean time, below is some of the raw data that you may find useful.
            </div>
        </div>
    </div>

    <div class="block block-bordered block-rounded">
        <div class="block-header">
            <h3 class="block-title">My Lab Reservations</h3>
        </div>
        <div class="block-content">

            @if(!empty($reservations->data))
                <table class="table table-condensed">
                    <thead>
                        <tr>
                            <th>Reservation ID</th>
                            <th>Date</th>
                            <th>Image Template</th>
                            <th># Credits</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($reservations->data as $reservation)
                            @if($reservation->tenant_reservation_token_id == null)
                                <tr class="small">
                                    <td>{{ $reservation->short_id }}</td>
                                    <td>{{ \Carbon\Carbon::parse($reservation->timestamps->created_at)->timezone($request->user()->timezone)->format('Y-m-d H:i:s T') }}</td>
                                    <td>
                                        {{ $reservation->relationships->cloud_image_template->data->name }}
                                        {!! $reservation->relationships->cloud_image_template->data->version != null ? '<span class="badge text-primary-darker bg-primary-lighter ml-2">Version '.$reservation->relationships->cloud_image_template->data->version.'</span>': '' !!}
                                        <span class="badge ml-2 badge-secondary">{{ $reservation->relationships->cloud_image_template->data->short_id }}</span><br />
                                    </td>
                                    <td>
                                        {{ number_format($reservation->qty_credits, 1) }} Credits
                                    </td>
                                    <td>
                                        {{ $reservation->status }}
                                    </td>
                                </tr>
                            @endif
                        @endforeach
                    </tbody>
                </table>
            @else
                <div class="alert alert-secondary mb-4">
                    It does not look like you have launched any lab reservations yet.
                </div>
            @endif
        </div>
    </div>

    <div class="block block-bordered block-rounded">
        <div class="block-header">
            <h3 class="block-title">Lab Access Token Redemption History</h3>
        </div>
        <div class="block-content">

            @if(!empty($reservations->data))
                <table class="table table-condensed">
                    <thead>
                        <tr>
                            <th>Reservation ID</th>
                            <th>Date</th>
                            <th>Redemption Information</th>
                            <th># Credits</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($reservations->data as $reservation)
                            @if($reservation->tenant_reservation_token_id != null)
                                <tr class="small">
                                    <td>{{ $reservation->short_id }}</td>
                                    <td>{{ \Carbon\Carbon::parse($reservation->timestamps->created_at)->timezone($request->user()->timezone)->format('Y-m-d H:i:s T') }}</td>
                                    <td>
                                        {{ $reservation->relationships->tenant_user->data->auth_user_account->data->full_name }}<br />
                                        {!! $reservation->relationships->tenant_user->data->auth_user_account->data->organization != null ? $reservation->relationships->tenant_user->data->auth_user_account->data->organization.'<br />' : '' !!}
                                        {{ $reservation->relationships->tenant_user->data->auth_user_account->data->email }}<br />
                                    </td>
                                    <td>
                                        {{ number_format($reservation->qty_credits, 1) }} Credits
                                    </td>
                                    <td>
                                        {{ $reservation->status }}
                                    </td>
                                </tr>
                            @endif
                        @endforeach
                    </tbody>
                </table>
            @else
                <div class="alert alert-secondary mb-4">
                    It does not look like this token has been redeemed yet.
                </div>
            @endif
        </div>
    </div>

@endsection
