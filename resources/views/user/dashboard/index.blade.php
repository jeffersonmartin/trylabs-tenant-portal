@extends('_layouts.tenant_lite')

@section('page_title', 'My Dashboard')
@section('page_actions')
    <a class="btn btn-outline-info mr-2" href="{{ route('user.billing.index') }}"><i class="si si-credit-card mr-2"></i>Manage Subscription and Plan</a>
@endsection

@section('content')

    @if($errors->any())
        <div class="alert alert-warning">
            <strong>You'll need to make a few changes before your magic spells will work here.</strong>
            <ul class="mb-0">
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="block block-bordered block-rounded">
        <div class="row text-center">
            <div class="col-4 border-right py-4">
                <div class="text-primary text-center">
                    {{--<div class="item mx-auto">
                        <i class="si si-disc fa-3x text-primary"></i>
                    </div>
                    --}}
                    <p class="font-size-h1 font-w300 mt-3 mb-0">
                        {{ count($image_templates->data) }}{{-- TODO &nbsp;<span class="font-size-h3">of 0</span>--}}
                    </p>
                    <p class="font-size-h4 mb-0">
                        Image Templates
                    </p>
                    <a class="btn btn-primary btn-lg mt-4 mb-3 font-w400" href="{{ route('user.image.templates.create') }}"><i class="si si-disc mr-3"></i>Create an Image</a>
                </div>
            </div>
            <div class="col-4 border-right py-4">
                <div class="text-info text-center">
                    {{--<div class="item mx-auto">
                        <i class="si si-screen-desktop fa-3x"></i>
                    </div>
                    --}}
                    <p class="font-size-h1 font-w300 mt-3 mb-0">
                        {{ number_format($tenant_account->data->credit_balance_qty, 1) }}
                    </p>
                    <p class="font-size-h4 mb-0">
                        VM Usage Credits Available
                    </p>
                    <a class="btn btn-info btn-lg mt-4 mb-3 font-w400" href="#modal-create-reservation" data-toggle="modal"><i class="si si-screen-desktop mr-3"></i>Launch a Lab</a>
                </div>
            </div>
            <div class="col-4 py-4">
                <div class="text-success text-center">
                    {{--<div class="item mx-auto">
                        <i class="si si-link fa-3x"></i>
                    </div>
                    --}}
                    <p class="font-size-h1 font-w300 mt-3 mb-0">
                        {{ count($reservation_tokens->data) }}</span>
                    </p>
                    <p class="font-size-h4 mb-0">
                        Lab Access Tokens
                    </p>
                    <a class="btn btn-success btn-lg mt-4 mb-3 font-w400" href="#modal-create-token" data-toggle="modal"><i class="si si-link mr-3"></i>Share a Lab</a>
                </div>
            </div>
        </div>
    </div>

    @if(!empty($reservations->data))

        <div class="block block-rounded block-bordered">

            <table class="table table-condensed mb-0">
                <tbody>
                    @foreach($reservations->data as $reservation)

                        @if($reservation->timestamps->terminated_at == null)

                            <tr>
                                <td class="p-3" {!! $loop->first ? 'style="border-top: none; width: 35%;"': 'style="width: 35%;"' !!}>{{-- Image Template --}}
                                    <p class="mb-0" style="height: 55px;">
                                        <span class="font-w700">{{ $reservation->relationships->cloud_image_template->data->name }}</span>
                                        {!! $reservation->relationships->cloud_image_template->data->version != null ? '<span class="badge text-primary-darker bg-primary-lighter ml-2">Version '.$reservation->relationships->cloud_image_template->data->version.'</span>': '' !!}
                                        <span class="badge ml-2 badge-secondary">{{ $reservation->relationships->cloud_image_template->data->short_id }}</span><br />
                                        <span class="">{{ config('trylabs.portal.reservation.url') }}/r/{{ $reservation->short_id }}</span><br />
                                    </p>
                                </td>
                                <td class="p-3" {!! $loop->first ? 'style="border-top: none; width: 40%;"': 'style="width: 40%;"' !!}>{{-- Reservation Details --}}

                                    <span class="font-w700">{{ \Carbon\Carbon::parse($reservation->timestamps->created_at)->format('l, M j g:i A T') }}</span>
                                    <span class="badge ml-2 badge-success">## Credits</span>
                                    <span class="badge ml-2 badge-secondary">{{ $reservation->short_id }}</span><br />
                                    @if($reservation->timestamps->terminated_at == null)
                                        Expires {{ \Carbon\Carbon::parse($reservation->timestamps->expires_at)->format('D') }} at {{ \Carbon\Carbon::parse($reservation->timestamps->expires_at)->format('g:i A T') }} <span class="text-success">({{ \Carbon\Carbon::parse($reservation->timestamps->expires_at)->diffForHumans() }})</span><br />
                                    @elseif($reservation->timestamps->terminated_at != null)
                                        Terminated {{ \Carbon\Carbon::parse($reservation->timestamps->terminated_at)->format('M j g:i A T') }} <span class="text-muted">({{ \Carbon\Carbon::parse($reservation->timestamps->terminated_at)->diffForHumans() }})</span><br />
                                    @endif
                                    {{-- Usage Credits --}}
                                    {{--
                                    <span class="small">
                                        {{ $image_template->relationships->cloud_image_size->data->name }}&nbsp;-&nbsp;
                                        {{ $image_template->relationships->cloud_image_size->data->vcpu }} vCPU&nbsp;|&nbsp;
                                        {{ $image_template->relationships->cloud_image_size->data->memory }}GB Memory&nbsp;|&nbsp;
                                        {{ $image_template->relationships->cloud_image_size->data->disk }}GB Disk
                                    </span>
                                    --}}
                                </td>

                                <td class="p-3 text-right" {!! $loop->first ? 'style="border-top: none; width: 25%;"': 'style="width: 25%;"' !!}>{{-- Actions --}}
                                    <div class="btn-group btn-group-lg">
                                        <a class="btn btn-success" target="_blank" href="{{ config('trylabs.portal.reservation.url') }}/r/{{ $reservation->short_id }}" data-toggle="tooltip" data-placement="top" title data-original-title="Access Lab">
                                            <i class="si si-control-play"></i>
                                        </a>
                                        {{--
                                        <a class="btn btn-outline-info" href="#modal-extend-{{ $reservation->short_id }}" data-toggle="modal" v-b-tooltip.hover title="Extend Reservation" >
                                            <i class="si si-hourglass"></i>
                                        </a>
                                        --}}
                                        @if(\Carbon\Carbon::parse($reservation->timestamps->created_at)->addSeconds(90) < now())
                                            <a class="btn btn-outline-info border-right-0" href="#modal-snapshot-{{ $reservation->short_id }}" data-toggle="modal" {{--v-b-tooltip.hover title="Save as Image Template"--}}>
                                                <i class="si si-disc"></i>
                                            </a>
                                            <a class="btn btn-outline-danger" href="#modal-terminate-{{ $reservation->short_id }}" data-toggle="modal" v-b-tooltip.hover title="Terminate Lab">
                                                <i class="si si-trash"></i>
                                            </a>
                                        @else
                                            <a class="btn btn-outline-secondary border-right-0" href="#" v-b-tooltip.hover title="Snapshot Unavailable until Reservation is Provisioned (after 90 seconds)">
                                                <i class="si si-disc"></i>
                                            </a>
                                            <a class="btn btn-outline-secondary" href="#" v-b-tooltip.hover title="Terminate Unavailable until Reservation is Provisioned (after 90 seconds)">
                                                <i class="si si-trash"></i>
                                            </a>
                                        @endif
                                    </div>
                                </td>
                            </tr>



                        @endif

                    @endforeach
                </tbody>
            </table>

        </div>

    @endif

    @include('user.tutorial._partials.getting_started')

    {{--
    <div class="alert alert-warning">
        <div class="d-flex">
            <div class="flex-00-auto mt-2">
                <i class="si si-chemistry fa-2x mr-3 mt-2"></i>
            </div>
            <div class="flex-fill">
                <strong>Redesign In Progress</strong>. We are collecting a lot of great metrics and are rebuilding the dashboard to provide you with an amazing analytics experience and pivotable data set. If you have suggestions, we'd love to hear from you!
            </div>
        </div>
    </div>
    --}}


@endsection

@section('modals')
    @include('user.reservations._modals.create_reservation')
    @include('user.tokens._modals.create_token')

    @foreach($reservations->data as $reservation)
        @include('user.reservations._modals.extend')
        @include('user.reservations._modals.snapshot')
        @include('user.reservations._modals.terminate')
    @endforeach
@endsection
