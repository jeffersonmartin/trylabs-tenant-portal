@extends('_layouts.tenant_lite')

@section('page_title', 'Image Settings')
@section('page_actions')
    <a class="btn btn-outline-primary mr-2" href="{{ route('user.image.templates.index') }}">Back to Image Templates</a>
    {{--<b-dropdown id="page-actions-dropdown" right variant="outline-primary" text="Actions" class="m-md-2">
        <b-dropdown-item>Verify Account</b-dropdown-item>
        <b-dropdown-item>Reset Password</b-dropdown-item>
        <b-dropdown-item>Lock Account</b-dropdown-item>
        <b-dropdown-item>Unlock Account</b-dropdown-item>
        <b-dropdown-divider></b-dropdown-divider>
        <b-dropdown-item>Delete User</b-dropdown-item>
    </b-dropdown>--}}
@endsection

@section('content')

    <!--
    <create-image-template-component>
</create-image-template-component>
-->

<form method="post" action="{{ route('user.image.settings.update') }}">
    @csrf
    @method('PATCH')

    <div class="row">

        <div class="col-md-6 col-sm-12">
            <div class="block block-bordered">
                <div class="block-header">
                    <h3 class="block-title">
                        SSH Key
                        <a class="ml-1" data-toggle="tooltip" data-placement="top" title data-original-title="We have created an SSH key pair for your account. You will need to use the key pair to log in to each of your image templates using SSH."><i class="si si-question js-tooltip-enabled text-warning" ></i></a>
                    </h3>
                </div>
                <div class="block-content">
                    <div class="col-12 text-center mb-3">
                        <a class="btn btn-outline-success mr-2" href="{{ route('user.profile.ssh.key.download') }}">Download SSH Key Pair</a>
                        <a class="btn btn-outline-info mr-2" href="{{ route('support.kb.trylabs.reservations.ssh-keys')}}">View Instructions</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6 col-sm-12">
            <div class="block block-bordered">
                <div class="block-header">
                    <h3 class="block-title">
                        Domain Name
                        <a class="ml-1" data-toggle="tooltip" data-placement="top" title data-original-title="When you create a reservation for an image template, your virtual machine will be accessible using a FQDN instead of an IP address. You can choose one of our white label domain names or you can leave the default selected."><i class="si si-question js-tooltip-enabled text-warning" ></i></a>
                    </h3>
                </div>
                <div class="block-content">
                    <div class="col-12">
                        <div class="form-group row items-push">
                            <input type="text" readonly class="form-control-plaintext mr-1" id="#" name="#" value="r-a1b2c3d4." style="width: 120px; text-align: right;" />
                            <select class="form-control" name="cloud_domain_id_default" style="width: 270px;">
                                @foreach($cloud_domains->data as $domain)
                                    <option value="{{ $domain->id }}" {!! $tenant_account->data->cloud_domain_id_default == $domain->id ? 'selected="selected"' : 'disabled="disabled"' !!}>{{ $domain->fqdn }} {!! $domain->flags->flag_is_default == 1 ? ' (default)' : '' !!}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="block block-bordered">
                <div class="block-header">
                    <h3 class="block-title">
                        Cloud Region
                        <a class="ml-1" data-toggle="tooltip" data-placement="top" title data-original-title="This is the location where your virtual machine will be provisioned. Simply select the location that you're closest to for the lowest latency."><i class="si si-question js-tooltip-enabled text-warning" ></i></a>
                    </h3>
                </div>
                <div class="block-content">
                    <div class="form-group row items-push mb-0">
                        @foreach($cloud_regions->data as $region)
                            <div class="col-md-3">
                                <div class="custom-control custom-block custom-control-primary mb-1">
                                    <input type="radio" class="custom-control-input" id="cloud_region_{{ $region->short_id }}" name="cloud_region_id_default" {!! $tenant_account->data->cloud_region_id_default == $region->id ? 'checked="checked"' : '' !!} value="{{ $region->id }}">
                                    <label class="custom-control-label" for="cloud_region_{{ $region->short_id }}">
                                        <span class="d-block font-w400 text-center my-3">
                                            @if($region->name == 'Amsterdam')
                                                <span class="d-block icon-Bicycle my-4" style="font-size: 4em;"></span>
                                            @elseif($region->name == 'Frankfurt')
                                                <span class="d-block icon-Beer-Glass my-4" style="font-size: 4em;"></span>
                                            @elseif($region->name == 'India')
                                                <span class="d-block icon-Taj-Mahal my-4" style="font-size: 4em;"></span>
                                            @elseif($region->name == 'London')
                                                <span class="d-block icon-United-Kingdom my-4" style="font-size: 4em;"></span>
                                            @elseif($region->name == 'New York' || $region->name == 'New York City')
                                                <span class="d-block icon-Chrysler-Building my-4" style="font-size: 4em;"></span>
                                            @elseif($region->name == 'San Francisco')
                                                <span class="d-block icon-Bridge my-4" style="font-size: 4em;"></span>
                                            @elseif($region->name == 'Singapore')
                                                <span class="d-block icon-Singapore my-4" style="font-size: 4em;"></span>
                                            @elseif($region->name == 'Toronto')
                                                <span class="d-block icon-Canada my-4" style="font-size: 4em;"></span>
                                            @else
                                                <span class="d-block icon-Location-2 my-4" style="font-size: 4em;"></span>
                                            @endif
                                            <div class="font-size-h4 font-w600 mb-3">{{ $region->name }}</div>
                                        </span>
                                    </label>
                                    <span class="custom-block-indicator">
                                        <i class="fa fa-check"></i>
                                    </span>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="block block-bordered">
                <div class="block-content d-flex">
                    <div class="flex-fill mr-3">
                        <p>
                            The default settings configured on this page will only apply to new image templates. <br />You can edit an existing image template to change the image size or region.
                        </p>
                    </div>
                    <div class="flex-00-auto mb-3">
                        <a class="btn btn-outline-secondary btn-lg mr-1" href="{{ route('user.image.templates.index') }}">Cancel</a>
                        <button type="submit" class="btn btn-success btn-lg">Update Settings</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

</form>

@endsection

@section('modals')
@endsection
