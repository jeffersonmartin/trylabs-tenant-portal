<!-- Reservation Category Modal -->
<div class="modal fade" id="modal-delete-{{ $template->id }}" tabindex="-1" role="dialog" aria-labelledby="modal-terms" aria-hidden="true">
    <form method="post" action="{{ route('user.image.templates.delete', ['cloud_image_template_id' => $template->id]) }}">

        @csrf
        @method('DELETE')

        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="block block-transparent mb-0">
                    <div class="block-header bg-light border-bottom">
                        <h3 class="block-title">{{ $template->name }}<span class="badge ml-2 badge-secondary">{{ $template->short_id }}</span></h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="fa fa-fw fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content">

                        <p>
                            If you destroy this image template, the virtual machine image and any data that was configured on the operating system will be immediately destroyed and cannot be recovered. We will preserve the meta data for the image and analytics on the usage of the image template.<br />
                            <br />
                            Any Lab Access Tokens that have been configured with this image template will be deactivated.<br />
                        </p>

                    </div>
                    <div class="block-content block-content-full text-right bg-light border-top">
                        <button type="button" class="btn btn-outline-secondary mr-2" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-danger">Destroy Image Template</button>
                    </div>

                </div>
            </div>
        </div>
    </form>
</div>
<!-- END Terms Modal -->
