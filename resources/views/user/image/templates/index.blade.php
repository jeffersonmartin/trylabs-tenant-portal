@extends('_layouts.tenant_lite')

@section('page_title', 'My Image Templates')
@section('page_actions')
    <a class="btn btn-outline-primary mr-2" href="{{ route('user.image.templates.create') }}">Create an Image Template</a>
    <a class="btn btn-outline-primary" href="{{ route('user.image.settings.edit') }}">Manage Default Settings</a>
    {{--<b-dropdown id="page-actions-dropdown" right variant="outline-primary" text="Actions" class="m-md-2">
        <b-dropdown-item>Verify Account</b-dropdown-item>
        <b-dropdown-item>Reset Password</b-dropdown-item>
        <b-dropdown-item>Lock Account</b-dropdown-item>
        <b-dropdown-item>Unlock Account</b-dropdown-item>
        <b-dropdown-divider></b-dropdown-divider>
        <b-dropdown-item>Delete User</b-dropdown-item>
    </b-dropdown>--}}
@endsection

@section('content')

    @if($errors->any())
        <div class="alert alert-warning">
            <strong>You'll need to make a few changes before your magic spells will work here.</strong>
            <ul class="mb-0">
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    @if(!empty($image_templates->data))

        <div class="block block-rounded block-bordered">

                <table class="table table-condensed mb-0">
                    <tbody>
                        @foreach($image_templates->data as $image_template)

                            <tr>
                                <td class="p-3" {!! $loop->first ? 'style="border-top: none; width: 50%;"': 'style="width: 35%;"' !!}>{{-- Image Template --}}
                                    <span class="font-w700">{{ str_limit($image_template->name, 60, '...') }}</span>
                                    {{--<span class="badge text-primary-darker bg-primary-lighter ml-2">{{ $image_template->slug }}</span>--}}
                                    <br />
                                    <span class="badge mr-2 badge-secondary">{{ $image_template->short_id }}</span>
                                    {!! $image_template->version != null ? '<span class="badge text-primary-darker bg-primary-lighter mr-2">Version '.$image_template->version.'</span>': '' !!}
                                    @if($image_template->description != null)
                                        <br /><span>{{ str_limit($image_template->description, 100, '...') }}</span><br />
                                    @endif
                                </td>
                                <td class="p-3" {!! $loop->first ? 'style="border-top: none; width: 25%;"': 'style="width: 40%;"' !!}>{{-- Instance Type --}}
                                    @if($image_template->relationships->cloud_image_flavor->data->name == 'Ubuntu')
                                        <i class="fl-ubuntu fa-fw mr-1"></i>
                                    @elseif($image_template->relationships->cloud_image_flavor->data->name == 'CentOS')
                                        <i class="fl-centos fa-fw mr-1"></i>
                                    @elseif($image_template->relationships->cloud_image_flavor->data->name == 'CoreOS')
                                        <i class="fl-coreos fa-fw mr-1"></i>
                                    @elseif($image_template->relationships->cloud_image_flavor->data->name == 'Fedora')
                                        <i class="fl-fedora fa-fw mr-1"></i>
                                    @elseif($image_template->relationships->cloud_image_flavor->data->name == 'Debian')
                                        <i class="fl-debian fa-fw mr-1"></i>
                                    @else
                                        <i class="fl-tux fa-fw mr-1"></i>
                                    @endif
                                    {{ $image_template->relationships->cloud_image_flavor->data->name }} {{ $image_template->relationships->cloud_image_flavor->data->version }}<br />
                                    <span>
                                        {{ $image_template->relationships->cloud_image_size->data->name }}&nbsp;-&nbsp;
                                        {{ $image_template->relationships->cloud_image_size->data->vcpu }} vCPU&nbsp;/&nbsp;
                                        {{ $image_template->relationships->cloud_image_size->data->memory }}GB&nbsp;/&nbsp;
                                        {{ $image_template->relationships->cloud_image_size->data->disk }}GB
                                    </span>
                                    <br />
                                </td>
                                <td class="p-3 text-right" {!! $loop->first ? 'style="border-top: none; width: 25%;"': 'style="width: 25%;"' !!}>{{-- Actions --}}
                                    <div class="btn-group btn-group-lg">
                                        @if($image_template->status == 'active')
                                            <a class="btn btn-outline-success" href="{{ route('user.reservations.create', ['cloud_image_template_id' => $image_template->id]) }}" data-toggle="tooltip" data-placement="top" title data-original-title="Launch a Lab Reservation">
                                                <i class="si si-control-play"></i>
                                            </a>
                                            <a class="btn btn-outline-info border-right-0" href="{{ route('user.tokens.create', ['cloud_image_template_id' => $image_template->id]) }}" data-toggle="tooltip" data-placement="top" title data-original-title="Share with a Lab Access Token">
                                                <i class="si si-link"></i>
                                            </a>
                                            <a class="btn btn-outline-info" href="{{ route('user.image.templates.usage.index', ['cloud_image_template_id' => $image_template->id]) }}" data-toggle="tooltip" data-placement="top" title data-original-title="Usage and Analytics">
                                                <i class="si si-bar-chart"></i>
                                            </a>
                                            <a class="btn btn-outline-info" href="{{ route('user.image.templates.show', ['image_template_id' => $image_template->id]) }}" data-toggle="tooltip" data-placement="top" title data-original-title="View Image Template">
                                                <i class="si si-note"></i>
                                            </a>
                                        @else
                                            <a class="btn btn-outline-secondary border-right-0" href="#" v-b-tooltip.hover title="Launch Reservation Unavailable ({{ $image_template->status }})">
                                                <i class="si si-control-play"></i>
                                            </a>
                                            <a class="btn btn-outline-secondary border-right-0" href="#" v-b-tooltip.hover title="Lab Access Tokens Unavailable ({{ $image_template->status }})">
                                                <i class="si si-link"></i>
                                            </a>
                                            <a class="btn btn-outline-secondary border-right-0" href="#" v-b-tooltip.hover title="Usage and Analytics Unavailable ({{ $image_template->status }})">
                                                <i class="si si-bar-chart"></i>
                                            </a>
                                            <a class="btn btn-outline-secondary border-right-0" href="#" v-b-tooltip.hover title="View Image Template Unavailable ({{ $image_template->status }})">
                                                <i class="si si-note"></i>
                                            </a>
                                        @endif
                                    </div>
                                </td>
                            </tr>

                        @endforeach
                    </tbody>
                </table>

        </div>

    @else

        <div class="alert alert-info">

            <h3 class="alert-heading font-size-h4 my-2">No Image Templates Found</h3>

            <div class="d-flex">

                <div class="flex-fill">
                    <p class="mb-1">
                        It doesn't look like you have created an image template yet. An image template contains meta data about a virtual machine (VM) including the operating system and image size (vCPU, memory and disk space). After you create an image template, you will be able to create a new lab reservation.
                    </p>
                </div>

                <div class="flex-00-auto ml-5 mr-4">
                    <a class="btn btn-info btn-lg font-w400" href="{{ route('user.image.templates.create') }}">Create an Image Template</a>
                </div>
            </div>
        </div>

    @endif

    <div class="row">

        <div class="col-sm-4">
            <div class="block block-bordered block-rounded">
                <div class="block-header border-bottom">
                    <h3 class="block-title">How To Build an Image Template</h3>
                </div>
                <div class="block-content block-content-full">
                    <div data-toggle="tooltip" data-placement="top" title data-original-title="Coming Soon" style="width: 100%; color: #fff; background-color: #111; text-align: center;">
                        <i class="fab fa-youtube fa-3x my-6"></i>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-8">
            <div class="block block-bordered block-rounded">
                <div class="block-header border-bottom">
                    <h3 class="block-title">Tutorials for Common Configurations</h3>
                    <div class="block-options">
                        Courtesy of our partnership with <a target="_blank" href="https://www.digitalocean.com/community/tutorials/">Digital Ocean</a>
                    </div>
                </div>
                <div class="block-content block-content-full">
                    <ul class="fa-ul mb-0">
                        <li><a target="_blank" href="https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-18-04"><span class="fa-li"><i class="si si-book-open"></i></span>How To Install and Use Docker on Ubuntu 18.04</a></li>
                        <li><a target="_blank" href="https://www.digitalocean.com/community/tutorials/how-to-install-nginx-on-ubuntu-18-04"><span class="fa-li"><i class="si si-book-open"></i></span>How To Install Nginx on Ubuntu 18.04</a></li>
                        <li><a target="_blank" href="https://www.digitalocean.com/community/tutorials/how-to-set-up-a-node-js-application-for-production-on-ubuntu-18-04"><span class="fa-li"><i class="si si-book-open"></i></span>How To Set Up a Node.js Application for Production on Ubuntu 18.04</a></li>
                        <li><a target="_blank" href="https://www.digitalocean.com/community/tutorials/how-to-set-up-django-with-postgres-nginx-and-gunicorn-on-ubuntu-18-04"><span class="fa-li"><i class="si si-book-open"></i></span>How To Set Up Django with Postgres, Nginx, and Gunicorn on Ubuntu 18.04</a></li>
                        <li><a target="_blank" href="https://www.digitalocean.com/community/tutorials/how-to-set-up-laravel-nginx-and-mysql-with-docker-compose"><span class="fa-li"><i class="si si-book-open"></i></span>How To Set Up Laravel, Nginx, and MySQL with Docker Compose</a></li>
                        <li><a target="_blank" href="https://www.digitalocean.com/community/tutorials/how-to-deploy-a-go-web-application-with-docker-and-nginx-on-ubuntu-18-04"><span class="fa-li"><i class="si si-book-open"></i></span>How To Deploy a Go Web Application with Docker and Nginx on Ubuntu 18.04</a></li>
                        <li><a target="_blank" href="https://www.digitalocean.com/community/tutorials/how-to-install-wordpress-with-lemp-on-ubuntu-18-04"><span class="fa-li"><i class="si si-book-open"></i></span>How To Install WordPress with LEMP on Ubuntu 18.04</a></li>
                        <li><a target="_blank" href="https://www.digitalocean.com/community/tutorials/how-to-install-ruby-on-rails-with-rbenv-on-ubuntu-18-04"><span class="fa-li"><i class="si si-book-open"></i></span>How To Install Ruby on Rails with rbenv on Ubuntu 18.04</a></li>

                    </ul>
                </div>
            </div>
        </div>

    </div>

@endsection
