@extends('_layouts.tenant_lite')

@section('page_title', $cloud_image_template->data->name)
@section('page_actions')
    <a class="btn btn-outline-info mr-2" href="{{ route('user.reservations.create', ['cloud_image_template_id' => $cloud_image_template->data->id]) }}"><i class="si si-arrow-left mr-2"></i>Back to Image Template</a>
    <a class="btn btn-outline-danger mr-2" href="#modal-delete-{{ $cloud_image_template->data->id }}" data-toggle="modal"><i class="si si-trash mr-2"></i>Destroy Image</a>
@endsection

@section('content')

    {{--
    <div class="block block-rounded block-bordered">
        <div class="block-content block-content-full">
            <div class="row text-center">
                <div class="col-md-3 py-3">
                    <div class="font-size-h1 font-w300 text-black mb-1">
                        ###
                    </div>
                    <a class="font-size-h4 text-muted" href="javascript:void(0)">Reservations</a>
                </div>
                <div class="col-md-3 py-3">
                    <div class="font-size-h1 font-w300 text-success mb-1">
                        ###
                    </div>
                    <a class="font-size-h4 text-muted" href="javascript:void(0)">Token Redemptions</a>
                </div>
                <div class="col-md-3 py-3">
                    <div class="font-size-h1 font-w300 text-black mb-1">
                        ###
                    </div>
                    <a class="font-size-h4 text-muted" href="javascript:void(0)">Credits This Month</a>
                </div>
                <div class="col-md-3 py-3">
                    <div class="font-size-h1 font-w300 text-danger mb-1">
                        ###
                    </div>
                    <a class="font-size-h4 text-muted" href="javascript:void(0)">Usage Cost This Month</a>
                </div>
            </div>
        </div>
    </div>
    --}}

    @if($errors->any())
        <div class="alert alert-warning">
            <strong>You'll need to make a few changes before your magic spells will work here.</strong>
            <ul class="mb-0">
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form method="post" action="{{ route('user.image.templates.update', ['cloud_image_template_id' => $cloud_image_template->data->id]) }}">
    @csrf
    @method('PATCH')

    <div class="block block-bordered">
        <div class="block-header bg-body-light border-bottom">
            <h3 class="block-title">Image Template Meta Data<span class="small badge badge-secondary ml-2">{{ $cloud_image_template->data->short_id }}</span></h3>
            <div class="block-options">
                Updated {{ \Carbon\Carbon::parse($cloud_image_template->data->timestamps->updated_at)->format('M j, Y g:i A T') }}
            </div>
        </div>
        <div class="block-content">
            <div class="row">
                <div class="col-8">
                    <div class="form-group">
                        <label for="name">
                            Template Name
                            <a class="ml-1" data-toggle="tooltip" data-placement="top" title data-original-title="Required. Please enter a title (up to 55 characters) for your image template. This is simply an internal name that allows you to easily identify what this image template is for."><i class="si si-question js-tooltip-enabled text-warning" ></i></a>
                        </label>
                        <input type="text" class="form-control" id="name" name="name" value="{{ $cloud_image_template->data->name }}" maxlength="55" />
                    </div>
                </div>
                <div class="col-4">
                    <div class="form-group">
                        <label for="version">
                            Version Number
                            <a class="ml-1" data-toggle="tooltip" data-placement="top" title data-original-title="Optional. If there are multiple versions of this template, you can enter a version number to help you identify the different versions."><i class="si si-question js-tooltip-enabled text-warning" ></i></a>
                        </label>
                        <input type="text" class="form-control" id="version" name="version" value="{{ $cloud_image_template->data->version }}" />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="form-group">
                        <label for="description">
                            Description
                            <a class="ml-1" data-toggle="tooltip" data-placement="top" title data-original-title="Optional. If you need a way to provide more details about what your image template includes than what you can fit in the title, you can add a description."><i class="si si-question js-tooltip-enabled text-warning" ></i></a>
                        </label>
                        <textarea class="form-control" id="description" name="description">{{ $cloud_image_template->data->description }}</textarea>
                    </div>
                </div>
            </div>
        </div>
        <div class="block-content d-flex border-top bg-body-light">

            <div class="flex-fill mr-3">
                &nbsp;
            </div>
            <div class="flex-00-auto mb-3">
                <a class="btn btn-outline-secondary btn-lg mr-2" href="{{ route('user.image.templates.show', ['cloud_image_template_id' => $cloud_image_template->data->id]) }}">Cancel</a>
                <button type="submit" class="btn btn-success btn-lg">Save Changes</button>
            </div>

        </div>
    </div>

      <div class="row">

          <div class="col-lg-3">
              <div class="block block-bordered block-rounded">
                  <div class="block-content ribbon ribbon-light">
                      <div class="ribbon-box">
                          <small>{{ $cloud_image_template->data->relationships->cloud_image_flavor->data->version }}</small>
                      </div>
                      <div class="row text-center">
                          <div class="col-12">
                              @if($cloud_image_template->data->relationships->cloud_image_flavor->data->name == 'Ubuntu')
                                  <span class="d-block fl-ubuntu my-1" style="font-size: 3em;"></span>
                              @elseif($cloud_image_template->data->relationships->cloud_image_flavor->data->name == 'CentOS')
                                  <span class="d-block fl-centos my-1" style="font-size: 3em;"></span>
                              @elseif($cloud_image_template->data->relationships->cloud_image_flavor->data->name == 'CoreOS')
                                  <span class="d-block fl-coreos my-1" style="font-size: 3em;"></span>
                              @elseif($cloud_image_template->data->relationships->cloud_image_flavor->data->name == 'Fedora')
                                  <span class="d-block fl-fedora my-1" style="font-size: 3em;"></span>
                              @elseif($cloud_image_template->data->relationships->cloud_image_flavor->data->name == 'Debian')
                                  <span class="d-block fl-debian my-1" style="font-size: 3em;"></span>
                              @else
                                  <span class="d-block fl-tux my-1" style="font-size: 3em;"></span>
                              @endif
                              <div class="font-size-h4 font-w600 mb-4">{{ $cloud_image_template->data->relationships->cloud_image_flavor->data->name }}</div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>

          <div class="col-lg-6">
              <div class="block block-bordered block-rounded">
                  <div class="block-content ribbon ribbon-light">
                      <div class="ribbon-box">
                          <small>{{ $cloud_image_template->data->relationships->cloud_image_size->data->name }}</small>
                      </div>
                      <div class="row text-center mb-4">
                          <div class="col-4">
                              <span class="d-block icon-CPU my-3" style="font-size: 3em;"></span>
                              <div class="font-size-h4 font-w600">{{ $cloud_image_template->data->relationships->cloud_image_size->data->vcpu }} vCPU</div>
                          </div>
                          <div class="col-4">
                              <span class="d-block icon-Ram my-3" style="font-size: 3em;"></span>
                              <div class="font-size-h4 font-w600">{{ $cloud_image_template->data->relationships->cloud_image_size->data->memory }}GB RAM</div>
                          </div>
                          <div class="col-4">
                              <span class="d-block icon-Data-Storage my-3" style="font-size: 3em;"></span>
                              <div class="font-size-h4 font-w600">{{ $cloud_image_template->data->relationships->cloud_image_size->data->disk }}GB SSD</div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>

          <div class="col-lg-3">
              <div class="block block-bordered block-rounded">
                  <div class="block-content">
                      <div class="row text-center mb-4">
                          <div class="col-12">
                              @if($cloud_image_template->data->relationships->cloud_region->data->name == 'Amsterdam')
                                  <span class="d-block icon-Bicycle my-3" style="font-size: 3em;"></span>
                              @elseif($cloud_image_template->data->relationships->cloud_region->data->name == 'Frankfurt')
                                  <span class="d-block icon-Beer-Glass my-3" style="font-size: 3em;"></span>
                              @elseif($cloud_image_template->data->relationships->cloud_region->data->name == 'India')
                                  <span class="d-block icon-Taj-Mahal my-3" style="font-size: 3em;"></span>
                              @elseif($cloud_image_template->data->relationships->cloud_region->data->name == 'London')
                                  <span class="d-block icon-United-Kingdom my-3" style="font-size: 3em;"></span>
                              @elseif($cloud_image_template->data->relationships->cloud_region->data->name == 'New York' || $cloud_image_template->data->relationships->cloud_region->data->name == 'New York City')
                                  <span class="d-block icon-Chrysler-Building my-3" style="font-size: 3em;"></span>
                              @elseif($cloud_image_template->data->relationships->cloud_region->data->name == 'San Francisco')
                                  <span class="d-block icon-Bridge my-3" style="font-size: 3em;"></span>
                              @elseif($cloud_image_template->data->relationships->cloud_region->data->name == 'Singapore')
                                  <span class="d-block icon-Singapore my-3" style="font-size: 3em;"></span>
                              @elseif($cloud_image_template->data->relationships->cloud_region->data->name == 'Toronto')
                                  <span class="d-block icon-Canada my-3" style="font-size: 3em;"></span>
                              @else
                                  <span class="d-block icon-Location-2 my-3" style="font-size: 3em;"></span>
                              @endif
                              <div class="font-size-h4 font-w600">{{ $cloud_image_template->data->relationships->cloud_region->data->name }}</div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>

      </div>



      {{--
      <div class="row">
          <div class="col-md-12">
              <div class="block block-bordered">
                  <div class="block-header">
                      <h3 class="block-title">Image Size</h3>
                  </div>
                  <div class="block-content">
                      <div class="alert alert-secondary d-flex border-success">
                          <div class="flex-00-auto p-2 text-secondary">
                              <i class="si si-question fa-3x"></i>
                          </div>
                          <div class="flex-fill ml-3">
                              The standard image size provides sufficient virtual machine resources for most use cases. Keep in mind that once an image template has been upgraded to the Performance image size, it cannot be downgraded to the Standard size due to disk integrity issues with downsizing. It is recommended to only upgrade image templates to a Performance image size on a case-by-case basis.
                          </div>
                      </div>
                      <div class="form-group row items-push mb-0">

                          @foreach($cloud_image_sizes->data as $size)
                              <div class="col-6">
                                  <div class="custom-control custom-block custom-control-primary mb-1">
                                      <input type="radio" class="custom-control-input" id="image_size_{{ $size->short_id }}" name="cloud_image_size_id" {!! $size->flags->flag_is_default == 1 ? 'checked="checked"' : '' !!} value="{{ $size->id }}">
                                      <label class="custom-control-label" for="image_size_{{ $size->short_id }}">
                                          <span class="d-block font-w400 text-center my-3">
                                              <div class="row">
                                                  <div class="col-12">
                                                      <span class="d-block font-w400 text-center my-1">
                                                          <div class="font-size-h4 font-w600">{{ $size->name }}</div>
                                                          <p class="mb-0">{{ $size->description }}</p>
                                                      </span>
                                                  </div>
                                                  <div class="col-4">
                                                      <span class="d-block icon-CPU my-3" style="font-size: 3em;"></span>
                                                      <div class="font-size-h4 font-w600 mb-3">{{ $size->vcpu }} vCPU</div>
                                                  </div>
                                                  <div class="col-4">
                                                      <span class="d-block icon-Ram my-3" style="font-size: 3em;"></span>
                                                      <div class="font-size-h4 font-w600 mb-3">{{ $size->memory }}GB RAM</div>
                                                  </div>
                                                  <div class="col-4">
                                                      <span class="d-block icon-Data-Storage my-3" style="font-size: 3em;"></span>
                                                      <div class="font-size-h4 font-w600 mb-3">{{ $size->disk }}GB SSD</div>
                                                  </div>
                                              </div>
                                          </span>
                                      </label>
                                      <span class="custom-block-indicator">
                                          <i class="fa fa-check"></i>
                                      </span>
                                  </div>
                              </div>
                          @endforeach
                      </div>
                  </div>
              </div>
          </div>
      </div>
      --}}


  </form>

@endsection

@section('modals')
    @include('user.image.templates._modals.delete', ['template' => $cloud_image_template->data])
@endsection
