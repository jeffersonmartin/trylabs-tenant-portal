@extends('_layouts.tenant_lite')

@section('page_title', 'Create an Image Template')

@section('page_actions')
    <a class="btn btn-outline-primary mr-2" href="{{ route('user.image.templates.index') }}">Back to Image Templates</a>
    <a class="btn btn-outline-primary mr-2" href="{{ route('user.image.settings.edit') }}">Manage Default Settings</a>
@endsection

@section('content')

    @if($errors->any())
        <div class="alert alert-warning">
            <strong>You'll need to make a few changes before your magic spells will work here.</strong>
            <ul class="mb-0">
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form method="post" action="{{ route('user.image.templates.store') }}">
    @csrf

        <input type="hidden" name="cloud_provider_id" value="{{ $cloud_provider->id }}" />
        <input type="hidden" name="cloud_account_id" value="{{ $cloud_provider->cloud_account_id_default }}" />
        <input type="hidden" name="cloud_region_id" value="{{ $default_cloud_region->data->id }}" />
        <input type="hidden" name="tenant_account_id" value="{{ $tenant_account->data->id }}" />
        <input type="hidden" name="cloud_ssh_key_id" value="{{ $tenant_account->data->cloud_ssh_key_id_default }}" />
        <input type="hidden" name="status" value="active" />

        <div class="block block-bordered">
            <div class="block-content">
                <div class="row">
                    <div class="col-8">
                        <div class="form-group">
                            <label for="name">
                                Template Name
                                <a class="ml-1" data-toggle="tooltip" data-placement="top" title data-original-title="Required. Please enter a title (up to 55 characters) for your image template. This is simply an internal name that allows you to easily identify what this image template is for. Ex. 'My Demo Template 1'"><i class="si si-question js-tooltip-enabled text-warning" ></i></a>
                            </label>
                            <input type="text" class="form-control border-primary" id="name" name="name" maxlength="55" />
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group">
                            <label for="version">
                                Version Number
                                <a class="ml-1" data-toggle="tooltip" data-placement="top" title data-original-title="Optional. If there are multiple versions of this template, you can enter a version number to help you identify the different versions. Ex. '1.0'"><i class="si si-question js-tooltip-enabled text-warning" ></i></a>
                            </label>
                            <input type="text" class="form-control" id="version" name="version" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">
                            <label for="description">
                                Description
                                <a class="ml-1" data-toggle="tooltip" data-placement="top" title data-original-title="Optional. If you need a way to provide more details about what your image template includes than what you can fit in the title, you can add a description."><i class="si si-question js-tooltip-enabled text-warning" ></i></a>
                            </label>
                            <textarea class="form-control" id="description" name="description"></textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="block block-bordered">
                    <div class="block-header">
                        <h3 class="block-title">
                            Image Size
                            <a class="ml-1" data-toggle="tooltip" data-placement="top" title data-original-title="The standard image size provides sufficient virtual machine resources for most use cases. Keep in mind that once an image template has been upgraded to the Performance image size, it cannot be downgraded to the Standard size due to disk integrity issues with downsizing. It is recommended to only upgrade image templates to a Performance image size on a case-by-case basis."><i class="si si-question js-tooltip-enabled text-warning" ></i></a>
                        </h3>
                    </div>
                    <div class="block-content">
                        <div class="form-group row items-push mb-0">

                            @foreach($cloud_image_sizes->data as $size)
                                <div class="col-6">
                                    <div class="custom-control custom-block custom-control-primary mb-1">
                                        <input type="radio" class="custom-control-input" id="image_size_{{ $size->short_id }}" name="cloud_image_size_id" {!! $size->flags->flag_is_default == 1 ? 'checked="checked"' : '' !!} value="{{ $size->id }}">
                                        <label class="custom-control-label" for="image_size_{{ $size->short_id }}">
                                            <span class="d-block font-w400 text-center my-3">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <span class="d-block font-w400 text-center my-1">
                                                            <div class="font-size-h4 font-w600">{{ $size->name }}<span class="badge badge-success ml-2">{{ number_format($size->qty_credits_per_hour, 0) }} {!! $size->qty_credits_per_hour == 1 ? 'Credit' : 'Credits' !!} per Hour</span></div>
                                                            <p class="mb-0">{{ $size->description }}</p>
                                                        </span>
                                                    </div>
                                                    <div class="col-4">
                                                        <span class="d-block icon-CPU my-3" style="font-size: 3em;"></span>
                                                        <div class="font-size-h4 font-w600 mb-3">{{ $size->vcpu }} vCPU</div>
                                                    </div>
                                                    <div class="col-4">
                                                        <span class="d-block icon-Ram my-3" style="font-size: 3em;"></span>
                                                        <div class="font-size-h4 font-w600 mb-3">{{ $size->memory }}GB RAM</div>
                                                    </div>
                                                    <div class="col-4">
                                                        <span class="d-block icon-Data-Storage my-3" style="font-size: 3em;"></span>
                                                        <div class="font-size-h4 font-w600 mb-3">{{ $size->disk }}GB SSD</div>
                                                    </div>
                                                </div>
                                            </span>
                                        </label>
                                        <span class="custom-block-indicator">
                                            <i class="fa fa-check"></i>
                                        </span>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="block block-bordered">
            <ul class="nav nav-tabs nav-tabs-alt js-tabs-enabled" data-toggle="tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" href="#tab-operating-systems" data-toggle="tab">Operating Systems</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#tab-application-frameworks" data-toggle="tab">Application Frameworks</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#tab-containers" data-toggle="tab">Containers</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#tab-networking" data-toggle="tab">Network Simulations</a>
                </li>
                {{--
                <li class="nav-item ml-auto">
                    <a class="nav-link" href="#tab-existing-template" data-toggle="tab">Fork an Existing Image Template</a>
                </li>
                --}}
                <!--
                <li class="nav-item ml-auto">
                    <a class="nav-link" href="#btabs-alt-static-settings"><i class="si si-settings"></i></a>
                </li>
            -->
            </ul>
            <div class="block-content tab-content">
                <div class="tab-pane active" id="tab-operating-systems" role="tabpanel">
                    <div class="row my-3 mx-2">
                        <div class="col-12">

                            <div class="block block-bordered block-rounded">

                                {{-- CentOS --}}
                                <div class="block-content block-content-full border-bottom">
                                    <div class="d-flex">
                                        <div class="flex-00-auto">
                                            <div class="d-flex">
                                                <div class="flex-00-auto">
                                                    <span class="d-block fl-centos my-0 mr-3" style="font-size: 3em;"></span>
                                                </div>
                                                <div class="flex-fill font-size-h2 font-w600 my-3 mr-5" style="vertical-align: middle;">
                                                    CentOS
                                                </div>
                                            </div>
                                        </div>
                                        <div class="flex-fill">
                                            <div class="pull-right text-right">
                                                @foreach($cloud_image_flavors->data as $flavor)
                                                    @if($flavor->name == 'CentOS')
                                                        <div style="display: inline-block;">
                                                            <div class="custom-control custom-block custom-control-primary my-1 ml-2">
                                                                <input type="radio" class="custom-control-input" id="cloud_image_flavor_{{ $flavor->short_id }}" name="cloud_image_flavor_id" {!! $flavor->flags->flag_is_default == 1 ? 'checked="checked"' : '' !!} value="{{ $flavor->id }}">
                                                                <label class="custom-control-label" for="cloud_image_flavor_{{ $flavor->short_id }}">
                                                                    <span class="d-block font-w400 text-center">
                                                                        <div class="font-size-lg font-w400 my-1 mx-2">
                                                                            {{ strstr($flavor->version, '(', true) ? strstr($flavor->version, '(', true) : strstr($flavor->version, 'x', true) }}
                                                                            {!! strstr($flavor->version, 'x') ? '<span class="badge badge-secondary ml-1">'.strstr($flavor->version, 'x').'</span>' : '' !!}
                                                                        </div>
                                                                    </span>
                                                                </label>
                                                                <span class="custom-block-indicator">
                                                                    <i class="fa fa-check"></i>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    @endif
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{-- END of CentOS --}}

                                {{-- CoreOS --}}
                                <div class="block-content block-content-full border-bottom">
                                    <div class="d-flex">
                                        <div class="flex-00-auto">
                                            <div class="d-flex">
                                                <div class="flex-00-auto">
                                                    <span class="d-block fl-coreos my-0 mr-3" style="font-size: 3em;"></span>
                                                </div>
                                                <div class="flex-fill font-size-h2 font-w600 my-3 mr-5">
                                                    CoreOS
                                                </div>
                                            </div>
                                        </div>
                                        <div class="flex-fill">
                                            <div class="pull-right text-right">
                                                @foreach($cloud_image_flavors->data as $flavor)
                                                    @if($flavor->name == 'CoreOS')
                                                        <div style="display: inline-block;">
                                                            <div class="custom-control custom-block custom-control-primary my-1 ml-2">
                                                                <input type="radio" class="custom-control-input" id="cloud_image_flavor_{{ $flavor->short_id }}" name="cloud_image_flavor_id" {!! $flavor->flags->flag_is_default == 1 ? 'checked="checked"' : '' !!} value="{{ $flavor->id }}">
                                                                <label class="custom-control-label" for="cloud_image_flavor_{{ $flavor->short_id }}">
                                                                    <span class="d-block font-w400 text-center">
                                                                        <div class="font-size-lg font-w400 my-1 mx-2">
                                                                            {{ strstr($flavor->version, '(', true) ? strstr($flavor->version, '(', true) : $flavor->version }}
                                                                            {!! strstr($flavor->version, '(') ? '<span class="badge badge-secondary ml-1">'.strstr($flavor->version, '(').'</span>' : '' !!}
                                                                        </div>
                                                                    </span>
                                                                </label>
                                                                <span class="custom-block-indicator">
                                                                    <i class="fa fa-check"></i>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    @endif
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{-- END of CoreOS --}}

                                {{-- Debian --}}
                                <div class="block-content block-content-full border-bottom">
                                    <div class="d-flex">
                                        <div class="flex-00-auto">
                                            <div class="d-flex">
                                                <div class="flex-00-auto">
                                                    <span class="d-block fl-debian my-0 mr-3" style="font-size: 3em;"></span>
                                                </div>
                                                <div class="flex-fill font-size-h2 font-w600 my-3 mr-5" style="vertical-align: middle;">
                                                    Debian
                                                </div>
                                            </div>
                                        </div>
                                        <div class="flex-fill">
                                            <div class="pull-right text-right">
                                                @foreach($cloud_image_flavors->data as $flavor)
                                                    @if($flavor->name == 'Debian')
                                                        <div style="display: inline-block;">
                                                            <div class="custom-control custom-block custom-control-primary my-1 ml-2">
                                                                <input type="radio" class="custom-control-input" id="cloud_image_flavor_{{ $flavor->short_id }}" name="cloud_image_flavor_id" {!! $flavor->flags->flag_is_default == 1 ? 'checked="checked"' : '' !!} value="{{ $flavor->id }}">
                                                                <label class="custom-control-label" for="cloud_image_flavor_{{ $flavor->short_id }}">
                                                                    <span class="d-block font-w400 text-center">
                                                                        <div class="font-size-lg font-w400 my-1 mx-2">
                                                                            {{ strstr($flavor->version, '(', true) ? strstr($flavor->version, '(', true) : strstr($flavor->version, 'x', true) }}
                                                                            {!! strstr($flavor->version, 'x') ? '<span class="badge badge-secondary ml-1">'.strstr($flavor->version, 'x').'</span>' : '' !!}
                                                                        </div>
                                                                    </span>
                                                                </label>
                                                                <span class="custom-block-indicator">
                                                                    <i class="fa fa-check"></i>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    @endif
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{-- END of Debian --}}

                                {{-- Fedora --}}
                                <div class="block-content block-content-full border-bottom">
                                    <div class="d-flex">
                                        <div class="flex-00-auto">
                                            <div class="d-flex">
                                                <div class="flex-00-auto">
                                                    <span class="d-block fl-fedora my-0 mr-3" style="font-size: 3em;"></span>
                                                </div>
                                                <div class="flex-fill font-size-h2 font-w600 my-3 mr-5" style="vertical-align: middle;">
                                                    Fedora
                                                </div>
                                            </div>
                                        </div>
                                        <div class="flex-fill">
                                            <div class="pull-right text-right">
                                                @foreach($cloud_image_flavors->data as $flavor)
                                                    @if($flavor->name == 'Fedora')
                                                        <div style="display: inline-block;">
                                                            <div class="custom-control custom-block custom-control-primary my-1 ml-2">
                                                                <input type="radio" class="custom-control-input" id="cloud_image_flavor_{{ $flavor->short_id }}" name="cloud_image_flavor_id" {!! $flavor->flags->flag_is_default == 1 ? 'checked="checked"' : '' !!} value="{{ $flavor->id }}">
                                                                <label class="custom-control-label" for="cloud_image_flavor_{{ $flavor->short_id }}">
                                                                    <span class="d-block font-w400 text-center">
                                                                        <div class="font-size-lg font-w400 my-1 mx-2">
                                                                            {{ strstr($flavor->version, '(', true) ? strstr($flavor->version, '(', true) : strstr($flavor->version, 'x', true) }}
                                                                            {!! strstr($flavor->version, 'x') ? '<span class="badge badge-secondary ml-1">'.strstr($flavor->version, 'x').'</span>' : '' !!}
                                                                        </div>
                                                                    </span>
                                                                </label>
                                                                <span class="custom-block-indicator">
                                                                    <i class="fa fa-check"></i>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    @endif
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{-- END of Fedora --}}

                                {{-- Ubuntu --}}
                                <div class="block-content block-content-full">
                                    <div class="d-flex">
                                        <div class="flex-00-auto">
                                            <div class="d-flex">
                                                <div class="flex-00-auto">
                                                    <span class="d-block fl-ubuntu my-0 mr-3" style="font-size: 3em;"></span>
                                                </div>
                                                <div class="flex-fill font-size-h2 font-w600 my-3 mr-5" style="vertical-align: middle;">
                                                    Ubuntu
                                                </div>
                                            </div>
                                        </div>
                                        <div class="flex-fill">
                                            <div class="pull-right text-right">
                                                @foreach($cloud_image_flavors->data as $flavor)
                                                    @if($flavor->name == 'Ubuntu')
                                                        <div style="display: inline-block;">
                                                            <div class="custom-control custom-block custom-control-primary my-1 ml-2">
                                                                <input type="radio" class="custom-control-input" id="cloud_image_flavor_{{ $flavor->short_id }}" name="cloud_image_flavor_id" {!! $flavor->flags->flag_is_default == 1 ? 'checked="checked"' : '' !!} value="{{ $flavor->id }}">
                                                                <label class="custom-control-label" for="cloud_image_flavor_{{ $flavor->short_id }}">
                                                                    <span class="d-block font-w400 text-center">
                                                                        <div class="font-size-lg font-w400 my-1 mx-2">
                                                                            {{ strstr($flavor->version, '(', true) ? strstr($flavor->version, '(', true) : strstr($flavor->version, 'x', true) }}
                                                                            {!! strstr($flavor->version, 'x') ? '<span class="badge badge-secondary ml-1">'.strstr($flavor->version, 'x').'</span>' : '' !!}
                                                                        </div>
                                                                    </span>
                                                                </label>
                                                                <span class="custom-block-indicator">
                                                                    <i class="fa fa-check"></i>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    @endif
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{-- END of Ubuntu --}}

                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="tab-containers" role="tabpanel">
                    <div class="row my-3 mx-2">

                        <div class="col-12">

                            <div class="block block-bordered block-rounded">

                                {{-- Docker --}}
                                <div class="block-content block-content-full ribbon ribbon-left ribbon-danger">
                                    <div class="d-flex">
                                        <div class="flex-00-auto">
                                            <div class="d-flex">
                                                <div class="flex-00-auto">
                                                    <span class="d-block fa-docker fab fa-fw my-2 mr-3" style="font-size: 3em;"></span>
                                                </div>
                                                <div class="flex-fill font-size-h2 font-w600 my-3 mr-5" style="vertical-align: middle;">
                                                    Docker
                                                </div>
                                            </div>
                                        </div>
                                        <div class="flex-fill">
                                            <div class="pull-right text-right">
                                                <div style="display: inline-block;">
                                                    <div class="custom-control custom-block custom-control-primary my-1 ml-2">
                                                        <input type="radio" class="custom-control-input" id="cloud_image_flavor_#" name="#" value="#" disabled="disabled">
                                                        <label class="custom-control-label" for="cloud_image_flavor_#">
                                                            <span class="d-block font-w400 text-center">
                                                                <div class="font-size-lg font-w400 my-1 mx-2">
                                                                    Coming Soon
                                                                </div>
                                                            </span>
                                                        </label>
                                                        <span class="custom-block-indicator">
                                                            <i class="fa fa-check"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{-- END of Docker --}}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="tab-application-frameworks" role="tabpanel">
                    <div class="row my-3 mx-2">

                        <div class="col-12">

                            <div class="block block-bordered block-rounded">

                                {{-- NPM MEAN --}}
                                <div class="block-content block-content-full border-bottom ribbon ribbon-left ribbon-danger">
                                    <div class="d-flex">
                                        <div class="flex-00-auto">
                                            <div class="d-flex">
                                                <div class="flex-00-auto">
                                                    <span class="d-block fa-npm fab fa-fw my-2 mr-3" style="font-size: 3em;"></span>
                                                </div>
                                                <div class="flex-fill font-size-h2 font-w600 my-3 mr-5" style="vertical-align: middle;">
                                                    MEAN
                                                    <span class="badge badge-primary font-size-base bg-gd-sea-op ml-1">JavaScript</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="flex-fill">
                                            <div class="pull-right text-right">
                                                <div style="display: inline-block;">
                                                    <div class="custom-control custom-block custom-control-primary my-1 ml-2">
                                                        <input type="radio" class="custom-control-input" id="cloud_image_flavor_#" name="#" value="#" disabled="disabled">
                                                        <label class="custom-control-label" for="cloud_image_flavor_#">
                                                            <span class="d-block font-w400 text-center">
                                                                <div class="font-size-lg font-w400 my-1 mx-2">
                                                                    Coming Soon
                                                                </div>
                                                            </span>
                                                        </label>
                                                        <span class="custom-block-indicator">
                                                            <i class="fa fa-check"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{-- END of NPM MEAN --}}

                                {{-- Node JS --}}
                                <div class="block-content block-content-full border-bottom ribbon ribbon-left ribbon-danger">
                                    <div class="d-flex">
                                        <div class="flex-00-auto">
                                            <div class="d-flex">
                                                <div class="flex-00-auto">
                                                    <span class="d-block fa-node fab fa-fw my-2 mr-3" style="font-size: 3em;"></span>
                                                </div>
                                                <div class="flex-fill font-size-h2 font-w600 my-3 mr-5" style="vertical-align: middle;">
                                                    Node.js
                                                    <span class="badge badge-primary font-size-base bg-gd-sea-op ml-1">JavaScript</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="flex-fill">
                                            <div class="pull-right text-right">
                                                <div style="display: inline-block;">
                                                    <div class="custom-control custom-block custom-control-primary my-1 ml-2">
                                                        <input type="radio" class="custom-control-input" id="cloud_image_flavor_#" name="#" value="#" disabled="disabled">
                                                        <label class="custom-control-label" for="cloud_image_flavor_#">
                                                            <span class="d-block font-w400 text-center">
                                                                <div class="font-size-lg font-w400 my-1 mx-2">
                                                                    Coming Soon
                                                                </div>
                                                            </span>
                                                        </label>
                                                        <span class="custom-block-indicator">
                                                            <i class="fa fa-check"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{-- END of Node JS --}}

                                {{-- Angular --}}
                                <div class="block-content block-content-full border-bottom ribbon ribbon-left ribbon-danger">
                                    <div class="d-flex">
                                        <div class="flex-00-auto">
                                            <div class="d-flex">
                                                <div class="flex-00-auto">
                                                    <span class="d-block fa-angular fab fa-fw my-2 mr-3" style="font-size: 3em;"></span>
                                                </div>
                                                <div class="flex-fill font-size-h2 font-w600 my-3 mr-5" style="vertical-align: middle;">
                                                    Angular
                                                    <span class="badge badge-primary font-size-base bg-gd-sea-op ml-1">JavaScript</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="flex-fill">
                                            <div class="pull-right text-right">
                                                <div style="display: inline-block;">
                                                    <div class="custom-control custom-block custom-control-primary my-1 ml-2">
                                                        <input type="radio" class="custom-control-input" id="cloud_image_flavor_#" name="#" value="#" disabled="disabled">
                                                        <label class="custom-control-label" for="cloud_image_flavor_#">
                                                            <span class="d-block font-w400 text-center">
                                                                <div class="font-size-lg font-w400 my-1 mx-2">
                                                                    Coming Soon
                                                                </div>
                                                            </span>
                                                        </label>
                                                        <span class="custom-block-indicator">
                                                            <i class="fa fa-check"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{-- END of Angular --}}

                                {{-- Vue JS --}}
                                <div class="block-content block-content-full border-bottom ribbon ribbon-left ribbon-danger">
                                    <div class="d-flex">
                                        <div class="flex-00-auto">
                                            <div class="d-flex">
                                                <div class="flex-00-auto">
                                                    <span class="d-block fa-vuejs fab fa-fw my-2 mr-3" style="font-size: 3em;"></span>
                                                </div>
                                                <div class="flex-fill font-size-h2 font-w600 my-3 mr-5" style="vertical-align: middle;">
                                                    Vue
                                                    <span class="badge badge-primary font-size-base bg-gd-sea-op ml-1">JavaScript</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="flex-fill">
                                            <div class="pull-right text-right">
                                                <div style="display: inline-block;">
                                                    <div class="custom-control custom-block custom-control-primary my-1 ml-2">
                                                        <input type="radio" class="custom-control-input" id="cloud_image_flavor_#" name="#" value="#" disabled="disabled">
                                                        <label class="custom-control-label" for="cloud_image_flavor_#">
                                                            <span class="d-block font-w400 text-center">
                                                                <div class="font-size-lg font-w400 my-1 mx-2">
                                                                    Coming Soon
                                                                </div>
                                                            </span>
                                                        </label>
                                                        <span class="custom-block-indicator">
                                                            <i class="fa fa-check"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{-- END of Vue JS --}}

                                {{-- React --}}
                                <div class="block-content block-content-full border-bottom ribbon ribbon-left ribbon-danger">
                                    <div class="d-flex">
                                        <div class="flex-00-auto">
                                            <div class="d-flex">
                                                <div class="flex-00-auto">
                                                    <span class="d-block fa-react fab fa-fw my-2 mr-3" style="font-size: 3em;"></span>
                                                </div>
                                                <div class="flex-fill font-size-h2 font-w600 my-3 mr-5" style="vertical-align: middle;">
                                                    React
                                                    <span class="badge badge-primary font-size-base bg-gd-sea-op ml-1">JavaScript</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="flex-fill">
                                            <div class="pull-right text-right">
                                                <div style="display: inline-block;">
                                                    <div class="custom-control custom-block custom-control-primary my-1 ml-2">
                                                        <input type="radio" class="custom-control-input" id="cloud_image_flavor_#" name="#" value="#" disabled="disabled">
                                                        <label class="custom-control-label" for="cloud_image_flavor_#">
                                                            <span class="d-block font-w400 text-center">
                                                                <div class="font-size-lg font-w400 my-1 mx-2">
                                                                    Coming Soon
                                                                </div>
                                                            </span>
                                                        </label>
                                                        <span class="custom-block-indicator">
                                                            <i class="fa fa-check"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{-- END of React --}}

                                {{-- Spring --}}
                                <div class="block-content block-content-full border-bottom ribbon ribbon-left ribbon-danger">
                                    <div class="d-flex">
                                        <div class="flex-00-auto">
                                            <div class="d-flex">
                                                <div class="flex-00-auto">
                                                    <span class="d-block fa-java fab fa-fw my-2 mr-3" style="font-size: 3em;"></span>
                                                </div>
                                                <div class="flex-fill font-size-h2 font-w600 my-3 mr-5" style="vertical-align: middle;">
                                                    Spring
                                                    <span class="badge badge-primary font-size-base bg-gd-sun-op ml-1">Java</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="flex-fill">
                                            <div class="pull-right text-right">
                                                <div style="display: inline-block;">
                                                    <div class="custom-control custom-block custom-control-primary my-1 ml-2">
                                                        <input type="radio" class="custom-control-input" id="cloud_image_flavor_#" name="#" value="#" disabled="disabled">
                                                        <label class="custom-control-label" for="cloud_image_flavor_#">
                                                            <span class="d-block font-w400 text-center">
                                                                <div class="font-size-lg font-w400 my-1 mx-2">
                                                                    Coming Soon
                                                                </div>
                                                            </span>
                                                        </label>
                                                        <span class="custom-block-indicator">
                                                            <i class="fa fa-check"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{-- END of Spring --}}

                                {{-- Ruby on Rails --}}
                                <div class="block-content block-content-full border-bottom ribbon ribbon-left ribbon-danger">
                                    <div class="d-flex">
                                        <div class="flex-00-auto">
                                            <div class="d-flex">
                                                <div class="flex-00-auto">
                                                    <span class="d-block icon-ruby-on-rails-alt fab fa-fw my-2 mr-3" style="font-size: 3em;"></span>
                                                </div>
                                                <div class="flex-fill font-size-h2 font-w600 my-3 mr-5" style="vertical-align: middle;">
                                                    Ruby on Rails
                                                    <span class="badge badge-primary font-size-base bg-gd-fruit-op ml-1">Ruby</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="flex-fill">
                                            <div class="pull-right text-right">
                                                <div style="display: inline-block;">
                                                    <div class="custom-control custom-block custom-control-primary my-1 ml-2">
                                                        <input type="radio" class="custom-control-input" id="cloud_image_flavor_#" name="#" value="#" disabled="disabled">
                                                        <label class="custom-control-label" for="cloud_image_flavor_#">
                                                            <span class="d-block font-w400 text-center">
                                                                <div class="font-size-lg font-w400 my-1 mx-2">
                                                                    Coming Soon
                                                                </div>
                                                            </span>
                                                        </label>
                                                        <span class="custom-block-indicator">
                                                            <i class="fa fa-check"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{-- END of Ruby on Rails --}}

                                {{-- Laravel --}}
                                <div class="block-content block-content-full border-bottom ribbon ribbon-left ribbon-danger">
                                    <div class="d-flex">
                                        <div class="flex-00-auto">
                                            <div class="d-flex">
                                                <div class="flex-00-auto">
                                                    <span class="d-block fa-laravel fab fa-fw my-2 mr-3" style="font-size: 3em;"></span>
                                                </div>
                                                <div class="flex-fill font-size-h2 font-w600 my-3 mr-5" style="vertical-align: middle;">
                                                    Laravel
                                                    <span class="badge badge-primary font-size-base bg-gd-dusk-op ml-1">PHP</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="flex-fill">
                                            <div class="pull-right text-right">
                                                <div style="display: inline-block;">
                                                    <div class="custom-control custom-block custom-control-primary my-1 ml-2">
                                                        <input type="radio" class="custom-control-input" id="cloud_image_flavor_#" name="#" value="#" disabled="disabled">
                                                        <label class="custom-control-label" for="cloud_image_flavor_#">
                                                            <span class="d-block font-w400 text-center">
                                                                <div class="font-size-lg font-w400 my-1 mx-2">
                                                                    Coming Soon
                                                                </div>
                                                            </span>
                                                        </label>
                                                        <span class="custom-block-indicator">
                                                            <i class="fa fa-check"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{-- END of Laravel --}}

                                {{-- Wordpress --}}
                                <div class="block-content block-content-full border-bottom ribbon ribbon-left ribbon-danger">
                                    <div class="d-flex">
                                        <div class="flex-00-auto">
                                            <div class="d-flex">
                                                <div class="flex-00-auto">
                                                    <span class="d-block fa-wordpress fab fa-fw my-2 mr-3" style="font-size: 3em;"></span>
                                                </div>
                                                <div class="flex-fill font-size-h2 font-w600 my-3 mr-5" style="vertical-align: middle;">
                                                    Wordpress
                                                    <span class="badge badge-primary font-size-base bg-gd-dusk-op ml-1">PHP</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="flex-fill">
                                            <div class="pull-right text-right">
                                                <div style="display: inline-block;">
                                                    <div class="custom-control custom-block custom-control-primary my-1 ml-2">
                                                        <input type="radio" class="custom-control-input" id="cloud_image_flavor_#" name="#" value="#" disabled="disabled">
                                                        <label class="custom-control-label" for="cloud_image_flavor_#">
                                                            <span class="d-block font-w400 text-center">
                                                                <div class="font-size-lg font-w400 my-1 mx-2">
                                                                    Coming Soon
                                                                </div>
                                                            </span>
                                                        </label>
                                                        <span class="custom-block-indicator">
                                                            <i class="fa fa-check"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{-- END of Wordpress --}}

                                {{-- Drupal --}}
                                <div class="block-content block-content-full border-bottom ribbon ribbon-left ribbon-danger">
                                    <div class="d-flex">
                                        <div class="flex-00-auto">
                                            <div class="d-flex">
                                                <div class="flex-00-auto">
                                                    <span class="d-block fa-drupal fab fa-fw my-2 mr-3" style="font-size: 3em;"></span>
                                                </div>
                                                <div class="flex-fill font-size-h2 font-w600 my-3 mr-5" style="vertical-align: middle;">
                                                    Drupal
                                                    <span class="badge badge-primary font-size-base bg-gd-dusk-op ml-1">PHP</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="flex-fill">
                                            <div class="pull-right text-right">
                                                <div style="display: inline-block;">
                                                    <div class="custom-control custom-block custom-control-primary my-1 ml-2">
                                                        <input type="radio" class="custom-control-input" id="cloud_image_flavor_#" name="#" value="#" disabled="disabled">
                                                        <label class="custom-control-label" for="cloud_image_flavor_#">
                                                            <span class="d-block font-w400 text-center">
                                                                <div class="font-size-lg font-w400 my-1 mx-2">
                                                                    Coming Soon
                                                                </div>
                                                            </span>
                                                        </label>
                                                        <span class="custom-block-indicator">
                                                            <i class="fa fa-check"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{-- END of Drupal --}}

                                {{-- LAMP PHP --}}
                                <div class="block-content block-content-full border-bottom ribbon ribbon-left ribbon-danger">
                                    <div class="d-flex">
                                        <div class="flex-00-auto">
                                            <div class="d-flex">
                                                <div class="flex-00-auto">
                                                    <span class="d-block fa-php fab fa-fw my-2 mr-3" style="font-size: 3em;"></span>
                                                </div>
                                                <div class="flex-fill font-size-h2 font-w600 my-3 mr-5" style="vertical-align: middle;">
                                                    LAMP (PHP)
                                                    <span class="badge badge-primary font-size-base bg-gd-dusk-op ml-1">PHP</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="flex-fill">
                                            <div class="pull-right text-right">
                                                <div style="display: inline-block;">
                                                    <div class="custom-control custom-block custom-control-primary my-1 ml-2">
                                                        <input type="radio" class="custom-control-input" id="cloud_image_flavor_#" name="#" value="#" disabled="disabled">
                                                        <label class="custom-control-label" for="cloud_image_flavor_#">
                                                            <span class="d-block font-w400 text-center">
                                                                <div class="font-size-lg font-w400 my-1 mx-2">
                                                                    Coming Soon
                                                                </div>
                                                            </span>
                                                        </label>
                                                        <span class="custom-block-indicator">
                                                            <i class="fa fa-check"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{-- END of LAMP PHP --}}

                                {{-- LEMP PHP --}}
                                <div class="block-content block-content-full border-bottom ribbon ribbon-left ribbon-danger">
                                    <div class="d-flex">
                                        <div class="flex-00-auto">
                                            <div class="d-flex">
                                                <div class="flex-00-auto">
                                                    <span class="d-block fa-php fab fa-fw my-2 mr-3" style="font-size: 3em;"></span>
                                                </div>
                                                <div class="flex-fill font-size-h2 font-w600 my-3 mr-5" style="vertical-align: middle;">
                                                    LEMP (PHP)
                                                    <span class="badge badge-primary font-size-base bg-gd-dusk-op ml-1">PHP</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="flex-fill">
                                            <div class="pull-right text-right">
                                                <div style="display: inline-block;">
                                                    <div class="custom-control custom-block custom-control-primary my-1 ml-2">
                                                        <input type="radio" class="custom-control-input" id="cloud_image_flavor_#" name="#" value="#" disabled="disabled">
                                                        <label class="custom-control-label" for="cloud_image_flavor_#">
                                                            <span class="d-block font-w400 text-center">
                                                                <div class="font-size-lg font-w400 my-1 mx-2">
                                                                    Coming Soon
                                                                </div>
                                                            </span>
                                                        </label>
                                                        <span class="custom-block-indicator">
                                                            <i class="fa fa-check"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{-- END of LEMP PHP --}}

                                {{-- LEMP Python --}}
                                <div class="block-content block-content-full border-bottom ribbon ribbon-left ribbon-danger">
                                    <div class="d-flex">
                                        <div class="flex-00-auto">
                                            <div class="d-flex">
                                                <div class="flex-00-auto">
                                                    <span class="d-block fa-python fab fa-fw my-2 mr-3" style="font-size: 3em;"></span>
                                                </div>
                                                <div class="flex-fill font-size-h2 font-w600 my-3 mr-5" style="vertical-align: middle;">
                                                    LEMP (Python)
                                                    <span class="badge badge-primary font-size-base bg-gd-leaf-op ml-1">Python</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="flex-fill">
                                            <div class="pull-right text-right">
                                                <div style="display: inline-block;">
                                                    <div class="custom-control custom-block custom-control-primary my-1 ml-2">
                                                        <input type="radio" class="custom-control-input" id="cloud_image_flavor_#" name="#" value="#" disabled="disabled">
                                                        <label class="custom-control-label" for="cloud_image_flavor_#">
                                                            <span class="d-block font-w400 text-center">
                                                                <div class="font-size-lg font-w400 my-1 mx-2">
                                                                    Coming Soon
                                                                </div>
                                                            </span>
                                                        </label>
                                                        <span class="custom-block-indicator">
                                                            <i class="fa fa-check"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{-- END of LEMP Python --}}

                                {{-- Django Python --}}
                                <div class="block-content block-content-full border-bottom ribbon ribbon-left ribbon-danger">
                                    <div class="d-flex">
                                        <div class="flex-00-auto">
                                            <div class="d-flex">
                                                <div class="flex-00-auto">
                                                    <span class="d-block fa-python fab fa-fw my-2 mr-3" style="font-size: 3em;"></span>
                                                </div>
                                                <div class="flex-fill font-size-h2 font-w600 my-3 mr-5" style="vertical-align: middle;">
                                                    Django
                                                    <span class="badge badge-primary font-size-base bg-gd-leaf-op ml-1">Python</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="flex-fill">
                                            <div class="pull-right text-right">
                                                <div style="display: inline-block;">
                                                    <div class="custom-control custom-block custom-control-primary my-1 ml-2">
                                                        <input type="radio" class="custom-control-input" id="cloud_image_flavor_#" name="#" value="#" disabled="disabled">
                                                        <label class="custom-control-label" for="cloud_image_flavor_#">
                                                            <span class="d-block font-w400 text-center">
                                                                <div class="font-size-lg font-w400 my-1 mx-2">
                                                                    Coming Soon
                                                                </div>
                                                            </span>
                                                        </label>
                                                        <span class="custom-block-indicator">
                                                            <i class="fa fa-check"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{-- END of Django Python --}}

                                {{-- Flask Python --}}
                                <div class="block-content block-content-full border-bottom ribbon ribbon-left ribbon-danger">
                                    <div class="d-flex">
                                        <div class="flex-00-auto">
                                            <div class="d-flex">
                                                <div class="flex-00-auto">
                                                    <span class="d-block fa-python fab fa-fw my-2 mr-3" style="font-size: 3em;"></span>
                                                </div>
                                                <div class="flex-fill font-size-h2 font-w600 my-3 mr-5" style="vertical-align: middle;">
                                                    Flask
                                                    <span class="badge badge-primary font-size-base bg-gd-leaf-op ml-1">Python</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="flex-fill">
                                            <div class="pull-right text-right">
                                                <div style="display: inline-block;">
                                                    <div class="custom-control custom-block custom-control-primary my-1 ml-2">
                                                        <input type="radio" class="custom-control-input" id="cloud_image_flavor_#" name="#" value="#" disabled="disabled">
                                                        <label class="custom-control-label" for="cloud_image_flavor_#">
                                                            <span class="d-block font-w400 text-center">
                                                                <div class="font-size-lg font-w400 my-1 mx-2">
                                                                    Coming Soon
                                                                </div>
                                                            </span>
                                                        </label>
                                                        <span class="custom-block-indicator">
                                                            <i class="fa fa-check"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{-- END of Flask Python --}}

                                {{-- Golang --}}
                                <div class="block-content block-content-full ribbon ribbon-left ribbon-danger">
                                    <div class="d-flex">
                                        <div class="flex-00-auto">
                                            <div class="d-flex">
                                                <div class="flex-00-auto">
                                                    <span class="d-block icon-go fab fa-fw my-2 mr-3" style="font-size: 3em;"></span>
                                                </div>
                                                <div class="flex-fill font-size-h2 font-w600 my-3 mr-5" style="vertical-align: middle;">
                                                    Golang
                                                    <span class="badge badge-primary font-size-base bg-gd-aqua-op ml-1">Go</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="flex-fill">
                                            <div class="pull-right text-right">
                                                <div style="display: inline-block;">
                                                    <div class="custom-control custom-block custom-control-primary my-1 ml-2">
                                                        <input type="radio" class="custom-control-input" id="cloud_image_flavor_#" name="#" value="#" disabled="disabled">
                                                        <label class="custom-control-label" for="cloud_image_flavor_#">
                                                            <span class="d-block font-w400 text-center">
                                                                <div class="font-size-lg font-w400 my-1 mx-2">
                                                                    Coming Soon
                                                                </div>
                                                            </span>
                                                        </label>
                                                        <span class="custom-block-indicator">
                                                            <i class="fa fa-check"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{-- END of Golang --}}

                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="tab-networking" role="tabpanel">
                    <div class="row my-3 mx-2">

                        <div class="col-12">

                            <div class="block block-bordered block-rounded">

                                {{-- Docker --}}
                                <div class="block-content block-content-full ribbon ribbon-left ribbon-danger">
                                    <div class="d-flex">
                                        <div class="flex-00-auto">
                                            <div class="d-flex">
                                                <div class="flex-00-auto">
                                                    <img src="{{ asset('images/icons/vendors/gns3-logo.png') }}" alt="GNS3" class="my-2 mr-3" style="width: 50px;" />
                                                </div>
                                                <div class="flex-fill font-size-h2 font-w600 my-3 mr-5" style="vertical-align: middle;">
                                                    GNS3
                                                </div>
                                            </div>
                                        </div>
                                        <div class="flex-fill">
                                            <div class="pull-right text-right">
                                                <div style="display: inline-block;">
                                                    <div class="custom-control custom-block custom-control-primary my-1 ml-2">
                                                        <input type="radio" class="custom-control-input" id="cloud_image_flavor_#" name="#" value="#" disabled="disabled">
                                                        <label class="custom-control-label" for="cloud_image_flavor_#">
                                                            <span class="d-block font-w400 text-center">
                                                                <div class="font-size-lg font-w400 my-1 mx-2">
                                                                    Coming Soon
                                                                </div>
                                                            </span>
                                                        </label>
                                                        <span class="custom-block-indicator">
                                                            <i class="fa fa-check"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{-- END of Docker --}}
                            </div>
                        </div>
                    </div>
                </div>
          </div>
      </div>

      <div class="row">

          <div class="col-12">

              <div class="block block-bordered">

                  <div class="block-content d-flex">

                      <div class="flex-fill mr-3">

                          <p>
                              You will be able to power up your image template virtual machine after you proceed to the next step. You can customize the geographic location in <a href="{{ route('user.image.settings.edit') }}">Image Template Settings</a>.
                          </p>

                      </div>
                      <div class="flex-00-auto mb-3">
                          <a class="btn btn-outline-secondary btn-lg mr-1" href="{{ route('user.image.templates.index') }}">Cancel</a>
                          <button type="submit" class="btn btn-success btn-lg">Create Image Template</button>
                      </div>

                  </div>

              </div>

          </div>

      </div>

  </form>

@endsection

@section('modals')
@endsection
