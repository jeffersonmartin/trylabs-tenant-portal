@extends('_layouts.tenant_lite')

@section('page_title', 'Image Template Usage')
@section('page_actions')
    <a class="btn btn-outline-primary mr-2" href="{{ route('user.image.templates.show', ['cloud_image_template_id' => $image_template->data->id]) }}">
        <i class="si si-arrow-left mr-2"></i>Back to Image Template
    </a>
    <a class="btn btn-outline-info mr-2" href="#" data-toggle="tooltip" data-placement="top" title data-original-title="Coming Soon">
        <i class="si si-doc mr-2"></i>Export as CSV
    </a>
@endsection

@section('content')

    <div class="alert alert-warning">
        <div class="d-flex">
            <div class="flex-00-auto mt-2">
                <i class="si si-chemistry fa-2x mr-3 mt-2"></i>
            </div>
            <div class="flex-fill">
                <strong>Redesign In Progress</strong>. We are collecting a lot of great metrics and are rebuilding the usage dashboard to provide you with an amazing analytics experience and pivotable data set. In the mean time, below is some of the raw data that you may find useful.
            </div>
        </div>
    </div>

    <div class="block block-bordered block-rounded">
        <div class="block-header bg-body-light border-bottom">
            <h3 class="block-title">
                {{ $image_template->data->name }}
                {!! $image_template->data->version != null ? '<span class="badge text-primary-darker bg-primary-lighter ml-2">Version '.$image_template->data->version.'</span>': '' !!}
                <span class="badge badge-secondary ml-2">{{ $image_template->data->short_id }}</span>
            </h3>
            <div class="block-options">
                Created on {{ \Carbon\Carbon::parse($image_template->data->timestamps->created_at)->toFormattedDateString() }}
            </div>
        </div>
        <div class="block-content">
            <div class="row mb-3">
                <div class="col-lg-6">
                    <div class="font-size-h4 font-w600">Description</div>
                    <p>
                        @if($image_template->data->description != null)
                            {{ $image_template->data->description }}
                        @else
                            You have not added a description for this image template.<br/>
                            <a href="{{ route('user.image.templates.edit', ['cloud_image_template_id' => $image_template->data->id]) }}">Would you like to add a description?</a>
                        @endif
                    </p>
                </div>
                <div class="col-lg-6">
                    <div class="font-size-h4 font-w600">Internal Notes</div>
                    <p>
                        @if($image_template->data->notes != null)
                            {{ $image_template->data->notes }}
                        @else
                            You have not added any internal notes for this image template.<br />
                            <a href="{{ route('user.image.templates.edit', ['cloud_image_template_id' => $image_template->data->id]) }}">Would you like to add some notes?</a>
                        @endif
                    </p>
                </div>
            </div>
        </div>
    </div>

    <div class="block block-bordered block-rounded">
        <div class="block-header">
            <h3 class="block-title">Lab Access Tokens</h3>
        </div>
        <div class="block-content">

            @if(!empty($tokens->data))
                <table class="table table-condensed">
                    <thead>
                        <tr>
                            <th>Token ID</th>
                            <th>Name</th>
                            <th># Redemptions</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($tokens->data as $token)
                            <tr>
                                <td>{{ $token->short_id }}</td>
                                <td>{{ $token->name }}</td>
                                <td>
                                    @if($token->flags->flag_is_redeem_count_protected == 1 && $token->redeem_count_max != null)
                                        {{ $token->redeem_count }} / {{ $token->redeem_count_max }}
                                    @else
                                        {{ $token->redeem_count }}
                                    @endif
                                </td>
                                <td>
                                    {{ $token->status }}
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @else
                <div class="alert alert-secondary mb-4">
                    It does not look like you have created a lab access token for this image template yet.
                </div>
            @endif
        </div>
    </div>

    <div class="block block-bordered block-rounded">
        <div class="block-header">
            <h3 class="block-title">My Reservation History</h3>
        </div>
        <div class="block-content">

            @if(!empty($reservations->data))
                <table class="table table-condensed">
                    <thead>
                        <tr>
                            <th>Reservation ID</th>
                            <th>Date</th>
                            <th># Credits</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($reservations->data as $reservation)
                            <tr>
                                <td>{{ $reservation->short_id }}</td>
                                <td>{{ \Carbon\Carbon::parse($reservation->timestamps->created_at)->format('Y-m-d H:i:s T') }}</td>
                                <td>
                                    {{ number_format($reservation->qty_credits, 1) }} Credits
                                </td>
                                <td>
                                    {{ $reservation->status }}
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @else
                <div class="alert alert-secondary mb-4">
                    It does not look like you have created a reservation with this image template yet.
                </div>
            @endif
        </div>
    </div>

@endsection
