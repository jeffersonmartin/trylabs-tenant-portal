@extends('_layouts.tenant_lite')

@section('page_title', 'Lab Access Token Details')
@section('page_actions')
    <a class="btn btn-outline-primary mr-2" href="{{ route('user.tokens.index') }}">Back to List of Tokens</a>
    <a class="btn btn-outline-info mr-2" href="{{ route('user.tokens.usage.index', ['tenant_reservation_token_id' => $token->data->id]) }}">View Usage</a>
    <a class="btn btn-outline-danger mr-2" href="#modal-delete-{{ $token->data->id }}" data-toggle="modal">Delete Token</a>
@endsection

@section('content')

    <form method="POST" action="{{ route('user.tokens.update', ['tenant_reservation_token_id' => $token->data->id]) }}">
    @csrf
    @method('PATCH')

    @if($errors->any())
        <div class="alert alert-warning">
            <strong>You'll need to make a few changes before your magic spells will work here.</strong>
            <ul class="mb-0">
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="block block-bordered block-rounded">
        <div class="block-header bg-body-light border-bottom">
            <h3 class="block-title">
                {{ $token->data->relationships->cloud_image_template->data->name }}
                {!! $token->data->relationships->cloud_image_template->data->version != null ? '<span class="badge text-primary-darker bg-primary-lighter ml-2">Version '.$token->data->relationships->cloud_image_template->data->version.'</span>': '' !!}
                <span class="badge badge-secondary ml-2">{{ $token->data->relationships->cloud_image_template->data->short_id }}</span>
            </h3>
            <div class="block-options">
                Created on {{ \Carbon\Carbon::parse($token->data->relationships->cloud_image_template->data->timestamps->created_at)->toFormattedDateString() }}
            </div>
        </div>
        <div class="block-content">
            <div class="row mb-3">
                <div class="col-lg-6">
                    <div class="font-w600">Description <span class="small text-danger">Not displayed to end users</span></div>
                    <p>
                        @if($token->data->relationships->cloud_image_template->data->description != null)
                            {{ $token->data->relationships->cloud_image_template->data->description }}
                        @else
                            You have not added a description for this image template.<br/>
                            <a href="{{ route('user.image.templates.edit', ['cloud_image_template_id' => $token->data->relationships->cloud_image_template->data->id]) }}">Would you like to add a description?</a>
                        @endif
                    </p>
                </div>
                <div class="col-lg-6">
                    <div class="font-w600">Internal Notes <span class="small text-danger">Not displayed to end users</span></div>
                    <p>
                        @if($token->data->relationships->cloud_image_template->data->notes != null)
                            {{ $token->data->relationships->cloud_image_template->data->notes }}
                        @else
                            You have not added any internal notes for this image template.<br />
                            <a href="{{ route('user.image.templates.edit', ['cloud_image_template_id' => $token->data->relationships->cloud_image_template->data->id]) }}">Would you like to add some notes?</a>
                        @endif
                    </p>
                </div>
            </div>
        </div>
    </div>

    <div class="row">

        <div class="col-lg-3">
            <div class="block block-bordered block-rounded">
                <div class="block-content ribbon ribbon-light">
                    <div class="ribbon-box">
                        <small>{{ $token->data->relationships->cloud_image_template->data->relationships->cloud_image_flavor->data->version }}</small>
                    </div>
                    <div class="row text-center">
                        <div class="col-12">
                            @if($token->data->relationships->cloud_image_template->data->relationships->cloud_image_flavor->data->name == 'Ubuntu')
                                <span class="d-block fl-ubuntu my-1" style="font-size: 3em;"></span>
                            @elseif($token->data->relationships->cloud_image_template->data->relationships->cloud_image_flavor->data->name == 'CentOS')
                                <span class="d-block fl-centos my-1" style="font-size: 3em;"></span>
                            @elseif($token->data->relationships->cloud_image_template->data->relationships->cloud_image_flavor->data->name == 'CoreOS')
                                <span class="d-block fl-coreos my-1" style="font-size: 3em;"></span>
                            @elseif($token->data->relationships->cloud_image_template->data->relationships->cloud_image_flavor->data->name == 'Fedora')
                                <span class="d-block fl-fedora my-1" style="font-size: 3em;"></span>
                            @elseif($token->data->relationships->cloud_image_template->data->relationships->cloud_image_flavor->data->name == 'Debian')
                                <span class="d-block fl-debian my-1" style="font-size: 3em;"></span>
                            @else
                                <span class="d-block fl-tux my-1" style="font-size: 3em;"></span>
                            @endif
                            <div class="font-size-h4 font-w600 mb-4">{{ $token->data->relationships->cloud_image_template->data->relationships->cloud_image_flavor->data->name }}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-6">
            <div class="block block-bordered block-rounded">
                <div class="block-content ribbon ribbon-light">
                    <div class="ribbon-box">
                        <small>{{ $token->data->relationships->cloud_image_template->data->relationships->cloud_image_size->data->name }}</small>
                    </div>
                    <div class="row text-center mb-4">
                        <div class="col-4">
                            <span class="d-block icon-CPU my-3" style="font-size: 3em;"></span>
                            <div class="font-size-h4 font-w600">{{ $token->data->relationships->cloud_image_template->data->relationships->cloud_image_size->data->vcpu }} vCPU</div>
                        </div>
                        <div class="col-4">
                            <span class="d-block icon-Ram my-3" style="font-size: 3em;"></span>
                            <div class="font-size-h4 font-w600">{{ $token->data->relationships->cloud_image_template->data->relationships->cloud_image_size->data->memory }}GB RAM</div>
                        </div>
                        <div class="col-4">
                            <span class="d-block icon-Data-Storage my-3" style="font-size: 3em;"></span>
                            <div class="font-size-h4 font-w600">{{ $token->data->relationships->cloud_image_template->data->relationships->cloud_image_size->data->disk }}GB SSD</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-3">
            <div class="block block-bordered block-rounded">
                <div class="block-content">
                    <div class="row text-center mb-4">
                        <div class="col-12">
                            @if($token->data->relationships->cloud_image_template->data->relationships->cloud_region->data->name == 'Amsterdam')
                                <span class="d-block icon-Bicycle my-3" style="font-size: 3em;"></span>
                            @elseif($token->data->relationships->cloud_image_template->data->relationships->cloud_region->data->name == 'Frankfurt')
                                <span class="d-block icon-Beer-Glass my-3" style="font-size: 3em;"></span>
                            @elseif($token->data->relationships->cloud_image_template->data->relationships->cloud_region->data->name == 'India')
                                <span class="d-block icon-Taj-Mahal my-3" style="font-size: 3em;"></span>
                            @elseif($token->data->relationships->cloud_image_template->data->relationships->cloud_region->data->name == 'London')
                                <span class="d-block icon-United-Kingdom my-3" style="font-size: 3em;"></span>
                            @elseif($token->data->relationships->cloud_image_template->data->relationships->cloud_region->data->name == 'New York' || $token->data->relationships->cloud_image_template->data->relationships->cloud_region->data->name == 'New York City')
                                <span class="d-block icon-Chrysler-Building my-3" style="font-size: 3em;"></span>
                            @elseif($token->data->relationships->cloud_image_template->data->relationships->cloud_region->data->name == 'San Francisco')
                                <span class="d-block icon-Bridge my-3" style="font-size: 3em;"></span>
                            @elseif($token->data->relationships->cloud_image_template->data->relationships->cloud_region->data->name == 'Singapore')
                                <span class="d-block icon-Singapore my-3" style="font-size: 3em;"></span>
                            @elseif($token->data->relationships->cloud_image_template->data->relationships->cloud_region->data->name == 'Toronto')
                                <span class="d-block icon-Canada my-3" style="font-size: 3em;"></span>
                            @else
                                <span class="d-block icon-Location-2 my-3" style="font-size: 3em;"></span>
                            @endif
                            <div class="font-size-h4 font-w600">{{ $token->data->relationships->cloud_image_template->data->relationships->cloud_region->data->name }}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="block block-bordered">
        <div class="block-content">

            <div class="row">

                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="name">Lab Scenario Name <span class="small text-info">Displayed to end users</span></label>
                        <input type="text" name="name" class="form-control" maxlength="255" value="{{ $token->data->name }}" />
                    </div>
                    <div class="form-group">
                        <label for="name">Internal Notes <span class="small text-danger">Not displayed to end users</span></label>
                        <textarea name="internal_notes" class="form-control" style="height: 65px;">{{ $token->data->internal_notes }}</textarea>
                    </div>
                </div>

                <div class="col-sm-6">

                    <div class="form-group">
                        <label for="name">Description <span class="small text-info">Displayed to end users</span></label>
                        <textarea name="external_notes" class="form-control" style="height: 150px;">{{ $token->data->external_notes }}</textarea>
                    </div>
                </div>
            </div>
        </div>
    </div>{{-- END block --}}

    <div class="block block-rounded block-bordered">
        <div class="block-content">

            <h2 class="content-heading">
                Billing Responsibility
            </h2>

            <div class="row mb-3">

                <div class="col-sm-12 col-md-6">{{-- Redeem Plan Hours --}}
                    <div class="custom-control custom-block custom-control-primary mb-1">
                        <input type="radio" class="custom-control-input" id="creator" name="billing_responsibility" value="creator" {!! $token->data->billing_responsibility == 'creator' ? 'checked="checked"' : '' !!}>
                        <label class="custom-control-label" for="creator">
                            <span class="d-block font-w400 text-center my-3">
                                <i class="icon-Stopwatch-2" style="font-size: 3em;"></i><br />
                                <div class="font-size-h3 font-w600 mt-1 mb-3">Redeem My Plan Hours</div>
                                <p class="mb-3">
                                    The hours that your end users consume will be deducted from your plan's monthly lab credit balance. Once your monthly lab credits are depleted, the users will receive a friendly error message. If you intend to share this link with a large number of users, it is recommended to upgrade to a plan with a higher number of lab credits per month.<br />
                                    <br />
                                    You have <span class="font-w700">## remaining</span> lab credits. We'll deposit additional lab credits in your account when your subscription renews in ## {{-- TODO --}}days or you can upgrade your plan retroactively to get more lab credits today.
                                </p>
                            </span>
                        </label>

                        <span class="custom-block-indicator">
                            <i class="fa fa-check"></i>
                        </span>
                    </div>
                </div>{{-- END Redeem Plan Hours --}}

                <div class="col-sm-12 col-md-6">{{-- End Users Pay Per Lab --}}
                    <div class="custom-control custom-block custom-control-primary mb-1">
                        <input type="radio" class="custom-control-input" id="consumer" name="billing_responsibility" {!! $token->data->billing_responsibility == 'consumer' ? 'checked="checked"' : '' !!} disabled="disabled">
                        <label class="custom-control-label ribbon ribbon-bookmark ribbon-info" for="consumer">
                            <div class="ribbon-box">Coming Soon</div>
                            <span class="d-block font-w400 text-center my-3">
                                <i class="icon-Cash-Register" style="font-size: 3em;"></i><br />
                                <div class="font-size-h3 font-w600 mt-1 mb-3">End Users Pay Per Lab</div>

                                <p class="mb-3">
                                    When end users access your reservation token link, they will be prompted to pay for their own lab time with a credit card. This allows you to avoid paying for a higher plan than you need for your own usage. This is particularly beneficial if you are sharing a lab access token with your customers, partners, followers, etc. and you do not have the budget to pay for their lab usage. You can access usage analytics for all end users regardless of billing responsibility.
                                </p>
                                <div class="font-size-h4 font-w600">
                                    $2.00 per Credit<br />
                                </div>

                            </span>
                        </label>

                        <span class="custom-block-indicator">
                            <i class="fa fa-check"></i>
                        </span>
                    </div>
                </div>{{-- END Users Pay Per Lab --}}

            </div>{{-- END row --}}

            <h2 class="content-heading">
                Security Settings
            </h2>

            <div class="row mb-4">

                <div class="col-sm-6 col-md-3">{{-- Time Limit --}}
                    <div class="custom-control custom-block custom-control-primary mb-1">
                        <input type="hidden" name="flag_is_time_limit_protected" value="1" />
                        <input type="checkbox" class="custom-control-input" id="flag_is_time_limit_protected" name="flag_is_time_limit_protected" value="1" checked="checked" disabled="disabled">
                        <label class="custom-control-label" for="flag_is_time_limit_protected">
                            <span class="d-block font-w400 text-center my-2">
                                <i class="icon-Sand-watch" style="font-size: 3em;"></i><br />
                                <div class="font-size-h4 font-w600 mb-3">Time Limit</div>
                                <div class="form-group col-12">
                                    <div class="form-group">
                                        <select name="time_limit_minutes" class="form-control">
                                            @if($token->data->time_limit_minutes >= 120)
                                                <option value="{{ $token->data->time_limit_minutes }}">{{ $token->data->time_limit_minutes / 60 }} Hours ({!! $token->data->relationships->cloud_image_template->data->relationships->cloud_image_size->data->qty_credits_per_hour * ($token->data->time_limit_minutes/60) !!} Credits)</option>
                                            @else
                                                <option value="{{ $token->data->time_limit_minutes }}">{{ $token->data->time_limit_minutes }} Mins ({!! $token->data->relationships->cloud_image_template->data->relationships->cloud_image_size->data->qty_credits_per_hour * ($token->data->time_limit_minutes/60) !!} Credits)</option>
                                            @endif
                                            <option value="#" disabled="disabled">--</option>
                                            <option value="30">30 Mins ({!! $token->data->relationships->cloud_image_template->data->relationships->cloud_image_size->data->qty_credits_per_hour * 0.5 !!} Credits)</option>
                                            <option value="60">60 Mins ({!! $token->data->relationships->cloud_image_template->data->relationships->cloud_image_size->data->qty_credits_per_hour * 1.0 !!} Credits)</option>
                                            <option value="90">90 Mins ({!! $token->data->relationships->cloud_image_template->data->relationships->cloud_image_size->data->qty_credits_per_hour * 1.5 !!} Credits)</option>
                                            @for($hour = 2; $hour <= 30; $hour++)
                                                <option value="{{ $hour * 60 }}">{{ $hour }} Hours ({!! $token->data->relationships->cloud_image_template->data->relationships->cloud_image_size->data->qty_credits_per_hour * $hour !!} Credits)</option>
                                            @endfor
                                        </select>
                                    </div>
                                </div>
                            </span>
                        </label>
                        <span class="custom-block-indicator">
                            <i class="fa fa-check"></i>
                        </span>
                    </div>
                </div>{{-- END Time Limit --}}

                <div class="col-sm-6 col-md-3">{{-- Password Protected --}}
                    <div class="custom-control custom-block custom-control-primary mb-1">
                        <input type="hidden" name="flag_is_password_protected" value="0" />
                        <input type="checkbox" class="custom-control-input" id="flag_is_password_protected" name="flag_is_password_protected" value="1" {!! $token->data->flags->flag_is_password_protected == '1' ? 'checked="checked"' : '' !!}>
                        <label class="custom-control-label" for="flag_is_password_protected">
                            <span class="d-block font-w400 text-center my-2">
                                <i class="icon-Type-Pass" style="font-size: 3em;"></i><br />
                                <div class="font-size-h4 font-w600 mb-3">Password Protected</div>
                                <div class="form-group col-12">
                                    <input type="text" class="form-control" name="password" placeholder="No Password" maxlength="55" value="{{ $token->data->password }}" />
                                </div>
                            </span>
                        </label>
                        <span class="custom-block-indicator">
                            <i class="fa fa-check"></i>
                        </span>
                    </div>
                </div>{{-- END Password Protected --}}

                <div class="col-sm-6 col-md-3">{{-- Max # Redemptions --}}
                    <div class="custom-control custom-block custom-control-primary mb-1">
                        <input type="hidden" name="flag_is_redeem_count_protected" value="0" />
                        <input type="checkbox" class="custom-control-input" id="flag_is_redeem_count_protected" name="flag_is_redeem_count_protected" value="1" {!! $token->data->flags->flag_is_redeem_count_protected == '1' ? 'checked="checked"' : '' !!}>
                        <label class="custom-control-label" for="flag_is_redeem_count_protected">
                            <span class="d-block font-w400 text-center my-2">
                                <i class="icon-Movie-Ticket" style="font-size: 3em;"></i><br />
                                <div class="font-size-h4 font-w600 mb-3">Max # Redemptions</div>
                                <div class="form-group col-8 offset-2">
                                    <input type="text" class="form-control" name="redeem_count_max" placeholder="No Limit" maxlength="5" value="{{ $token->data->redeem_count_max }}" />
                                </div>
                            </span>
                        </label>
                        <span class="custom-block-indicator">
                            <i class="fa fa-check"></i>
                        </span>
                    </div>
                </div>{{-- END Max # Redemptions --}}

                <div class="col-sm-6 col-md-3">{{-- Rate Limit --}}
                    <div class="custom-control custom-block custom-control-primary mb-1">
                        <input type="hidden" name="flag_is_rate_limit_protected" value="0" />
                        <input type="checkbox" class="custom-control-input" id="flag_is_rate_limit_protected" name="flag_is_rate_limit_protected" value="1" {!! $token->data->flags->flag_is_rate_limit_protected == '1' ? 'checked="checked"' : '' !!}>
                        <label class="custom-control-label" for="flag_is_rate_limit_protected">
                            <span class="d-block font-w400 text-center my-2">
                                <i class="icon-Clock-Forward" style="font-size: 3em;"></i><br />
                                <div class="font-size-h4 font-w600 mb-3">Rate Limited</div>
                                <div class="form-group col-12">
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="rate_limit_count_max" placeholder="No Limit" maxlength="5" value="{{ $token->data->rate_limit_count_max }}" /><br />
                                        <div class="input-group-append">
                                            <select name="rate_limit_interval" class="form-control">
                                                <option value="hour" {!! $token->data->rate_limit_interval == 'hour' ? 'selected="selected"' : '' !!}>per hour</option>
                                                <option value="day" {!! $token->data->rate_limit_interval == 'day' ? 'selected="selected"' : '' !!}>per day</option>
                                                <option value="week" {!! $token->data->rate_limit_interval == 'week' ? 'selected="selected"' : '' !!}>per week</option>
                                                <option value="month" {!! $token->data->rate_limit_interval == 'month' ? 'selected="selected"' : '' !!}>per month</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </span>
                        </label>
                        <span class="custom-block-indicator">
                            <i class="fa fa-check"></i>
                        </span>
                    </div>
                </div>{{-- END Rate Limit --}}

            </div>{{-- END row --}}
        </div>{{-- END block-content --}}

        <div class="block-content block-content-full text-right bg-white border-top">{{-- Block Footer --}}
            <button type="button" class="btn btn-outline-secondary mr-2" data-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-success">Update Token</button>
        </div>{{-- END Block Footer --}}

    </div>{{-- END block --}}

</form>

@endsection

@section('modals')
    @include('user.tokens._modals.delete', ['token' => $token->data])
@endsection
