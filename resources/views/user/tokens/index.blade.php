@extends('_layouts.tenant_lite')

@section('page_title', 'Shareable Lab Access Tokens')
@section('page_actions')
    <a class="btn btn-outline-primary mr-2" data-toggle="modal" href="#modal-create-token">Create a Lab Access Token</a>
    {{--<b-dropdown id="page-actions-dropdown" right variant="outline-primary" text="Actions" class="m-md-2">
        <b-dropdown-item>Verify Account</b-dropdown-item>
        <b-dropdown-item>Reset Password</b-dropdown-item>
        <b-dropdown-item>Lock Account</b-dropdown-item>
        <b-dropdown-item>Unlock Account</b-dropdown-item>
        <b-dropdown-divider></b-dropdown-divider>
        <b-dropdown-item>Delete User</b-dropdown-item>
    </b-dropdown>--}}
@endsection

@section('content')

    @if($errors->any())
        <div class="alert alert-warning">
            <strong>You'll need to make a few changes before your magic spells will work here.</strong>
            <ul class="mb-0">
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    @if(!empty($reservation_tokens->data))

        <div class="block block-rounded block-bordered">

                <table class="table table-condensed mb-0">
                    <tbody>
                        @foreach($reservation_tokens->data as $token)

                            <tr>
                                <td class="p-3" {!! $loop->first ? 'style="border-top: none; width: 35%;"': 'style="width: 35%;"' !!}>{{-- Image Template --}}
                                    <p class="mb-0" style="height: 55px;">
                                        <span class="font-w700">{{ $token->name }}</span><br />
                                        <span class="">{{ config('trylabs.portal.reservation.url') }}/t/{{ $token->short_id }}</span><br />
                                    </p>
                                    <span class="badge badge-success">## Credits</span>
                                    <span class="badge ml-2 badge-info">{{ $token->time_limit_minutes }} Mins</span>
                                    @if($token->flags->flag_is_redeem_count_protected)
                                        <span class="badge ml-2 badge-primary">{{ $token->redeem_count_max - $token->redeem_count }} Redemptions Left</span>
                                    @endif
                                    <br />
                                </td>
                                <td class="p-3" {!! $loop->first ? 'style="border-top: none; width: 40%;"': 'style="width: 40%;"' !!}>{{-- Reservation Details --}}
                                    <p class="mb-0" style="height: 55px;">
                                        {{ str_limit($token->external_notes, 100, '...') }}
                                    </p>
                                    <span class="badge text-primary-darker bg-primary-lighter">{{ $token->relationships->cloud_image_template->data->name }} {!! $token->relationships->cloud_image_template->data->version != null ? ' - Version '.$token->relationships->cloud_image_template->data->version: '' !!} - {{ $token->relationships->cloud_image_template->data->short_id }}</span>
                                    {{--<span class="badge ml-2 badge-secondary"></span>--}}
                                    <br />
                                </td>

                                <td class="p-3 text-right" {!! $loop->first ? 'style="border-top: none; width: 25%;"': 'style="width: 25%;"' !!}>{{-- Actions --}}
                                    <div class="btn-group btn-group-lg">
                                        <a class="btn btn-outline-success" target="_blank" href="{{ config('trylabs.portal.reservation.url') }}/t/{{ $token->short_id }}" data-toggle="tooltip" data-placement="top" title data-original-title="Visit Token Redemption Page">
                                            <i class="si si-share-alt"></i>
                                        </a>
                                        <a class="btn btn-outline-info" href="{{ route('user.tokens.usage.index', ['tenant_reservation_token_id' => $token->id]) }}" data-toggle="tooltip" data-placement="top" title data-original-title="View Usage">
                                            <i class="si si-bar-chart"></i>
                                        </a>
                                        <a class="btn btn-outline-info border-right-0" href="{{ route('user.tokens.show', ['tenant_reservation_token_id' => $token->id]) }}" data-toggle="tooltip" data-placement="top" title data-original-title="Edit Token">
                                            <i class="si si-note"></i>
                                        </a>
                                        <a class="btn btn-outline-danger" href="#modal-delete-{{ $token->id }}" data-toggle="modal">
                                            <i class="si si-trash"></i>
                                        </a>
                                    </div>
                                    @if($token->flags->flag_is_rate_limit_protected == 1 && $token->rate_limit_count != null && $token->rate_limit_count >= $token->rate_limit_count_max)
                                        <p class="small text-danger mt-2 mb-0">
                                            Rate limit resets {{ \Carbon\Carbon::parse($token->timestamps->rate_limit_resets_at)->format('M j g:i A T') }}
                                        </p>
                                    @endif
                                </td>
                            </tr>

                            @include('user.tokens._modals.delete', ['tenant_reservation_token_id' => $token->id])

                        @endforeach
                    </tbody>
                </table>

        </div>

    @else

        <div class="alert alert-info">

            <h3 class="alert-heading font-size-h4 my-2">No Lab Access Tokens Found</h3>

            <div class="d-flex">

                <div class="flex-fill">
                    <p class="mb-1">
                        It doesn't look like you have created a lab access token yet. A lab access token allows you to publish and share your image template with other people (ex. customers, partners, employees, co-workers, followers, friends) to launch their own lab reservations. You can create multiple lab access tokens for an image template, allowing you to create unique lab access tokens for each of your audiences with different passwords, time limits, redemption limits, rate limits, and billing responsibility.
                    </p>
                </div>

                <div class="flex-00-auto ml-5 mr-4">
                    <a class="btn btn-info btn-lg font-w400" data-toggle="modal" href="#modal-create-token">Create a Lab Access Token</a>
                </div>
            </div>
        </div>

    @endif

    <div class="row">

        <div class="col-sm-4">
            <div class="block block-bordered block-rounded">
                <div class="block-header border-bottom">
                    <h3 class="block-title">How To Use A Lab Access Token</h3>
                </div>
                <div class="block-content block-content-full">
                    <div data-toggle="tooltip" data-placement="top" title data-original-title="Coming Soon" style="width: 100%; color: #fff; background-color: #111; text-align: center;">
                        <i class="fab fa-youtube fa-3x my-6"></i>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-8">
            <div class="block block-bordered block-rounded">
                <div class="block-header border-bottom">
                    <h3 class="block-title">Tutorials for Common Use Cases</h3>
                </div>
                <div class="block-content block-content-full">
                    <ul class="fa-ul mb-0">
                        <li><a target="_blank" href="{{ route('support.kb.trylabs.tokens.best-practices') }}"><span class="fa-li"><i class="si si-book-open"></i></span>Best Practices with Lab Access Tokens and Security Settings</a></li>
                        <li><a target="_blank" href="{{ route('support.kb.trylabs.tokens.ux') }}"><span class="fa-li"><i class="si si-book-open"></i></span>Creating a Great User Experience with Lab Access Tokens</a></li>
                        <li><a target="_blank" href="{{ route('support.kb.trylabs.tokens.share-email') }}"><span class="fa-li"><i class="si si-book-open"></i></span>How To Share Link via Email</a></li>
                        <li><a target="_blank" href="{{ route('support.kb.trylabs.tokens.share-sharepoint') }}"><span class="fa-li"><i class="si si-book-open"></i></span>How To Share Link on SharePoint</a></li>
                        <li><a target="_blank" href="{{ route('support.kb.trylabs.tokens.share-html') }}"><span class="fa-li"><i class="si si-book-open"></i></span>How To Embed Link in HTML Webpage</a></li>
                        <li><a target="_blank" href="{{ route('support.kb.trylabs.tokens.share-youtube') }}"><span class="fa-li"><i class="si si-book-open"></i></span>How To Embed Link in YouTube Video</a></li>
                        <li><a target="_blank" href="{{ route('support.kb.trylabs.tokens.share-lms-skilljar') }}"><span class="fa-li"><i class="si si-book-open"></i></span>How To Add Lab to Skilljar LMS Course</a></li>
                        <li><a target="_blank" href="{{ route('support.kb.trylabs.tokens.share-lms-other') }}"><span class="fa-li"><i class="si si-book-open"></i></span>How To Add Lab to Other LMS Platforms</a></li>
                    </ul>
                </div>
            </div>
        </div>

    </div>

@endsection

@section('modals')
    @include('user.tokens._modals.create_token')
@endsection
