<!-- Reservation Category Modal -->
<div class="modal fade" id="modal-delete-{{ $token->id }}" tabindex="-1" role="dialog" aria-labelledby="modal-terms" aria-hidden="true">
    <form method="get" action="{{ route('user.tokens.delete', ['tenant_reservation_token_id', $token->id]) }}">

        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="block block-transparent mb-0">
                    <div class="block-header bg-light border-bottom">
                        <h3 class="block-title">{{ $token->name }}<span class="badge ml-2 badge-secondary">{{ $token->short_id }}</span></h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="fa fa-fw fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content">

                        <div class="row mb-3">

                            <div class="col-sm-12 col-md-6">{{-- Redeem Plan Hours --}}
                                <div class="custom-control custom-block custom-control-warning mb-1">
                                    <input type="radio" class="custom-control-input" id="modal_action_deactivate" name="modal_action" value="deactivate">
                                    <label class="custom-control-label" for="modal_action_deactivate">
                                        <span class="d-block font-w400 text-center my-3">
                                            {{--<i class="icon-Stopwatch-2" style="font-size: 3em;"></i><br />--}}
                                            <div class="font-size-h3 font-w600 mt-1 mb-3">Deactivate Token</div>
                                            <p class="mb-3">
                                                If you deactivate this token, the token link will still remain accessible, however users will receive a friendly error message that the token can no longer be redeemed and they should contact you for assistance.
                                            </p>
                                        </span>
                                    </label>

                                    <span class="custom-block-indicator">
                                        <i class="fa fa-check"></i>
                                    </span>
                                </div>
                            </div>{{-- END Redeem Plan Hours --}}

                            <div class="col-sm-12 col-md-6">{{-- End Users Pay Per Lab --}}
                                <div class="custom-control custom-block custom-control-danger mb-1">
                                    <input type="radio" class="custom-control-input" id="modal_action_delete" name="modal_action" value="delete">
                                    <label class="custom-control-label" for="modal_action_delete">
                                        <span class="d-block font-w400 text-center my-3">
                                            {{--<i class="icon-Cash-Register" style="font-size: 3em;"></i><br />--}}
                                            <div class="font-size-h3 font-w600 mt-1 mb-3">Delete Token</div>
                                            <p class="mb-3">
                                                If you delete this token, end users will receive a 404 error message when accessing the token link which may provide a bad user experience. If you have not shared this token with many users, this may not be a concern.
                                            </p>
                                        </span>
                                    </label>

                                    <span class="custom-block-indicator">
                                        <i class="fa fa-check"></i>
                                    </span>
                                </div>
                            </div>{{-- END Users Pay Per Lab --}}

                        </div>{{-- END row --}}


                    </div>
                    <div class="block-content block-content-full text-right bg-light border-top">
                        <button type="button" class="btn btn-outline-secondary mr-2" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-danger">Save Changes</button>
                    </div>

                </div>
            </div>
        </div>
    </form>
</div>
<!-- END Terms Modal -->
