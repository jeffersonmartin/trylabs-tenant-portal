<!-- Reservation Category Modal -->
<div class="modal fade" id="modal-create-token" tabindex="-1" role="dialog" aria-labelledby="modal-create-token" aria-hidden="true">
    <form method="get" action="{{ route('user.tokens.create') }}">

        <div class="modal-dialog modal-md modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="block block-themed block-transparent mb-0">
                    <div class="block-header bg-info">
                        <h3 class="block-title">Create a Lab Access Token</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="fa fa-fw fa-times"></i>
                            </button>
                        </div>
                    </div>
                    @if(!empty($image_templates->data))
                        <div class="block-content">

                            <p>
                                Please select the previously created image template that you would like to use or <a class="text-info" href="{{ route('user.image.templates.create') }}">create a new image template</a>.
                            </p>

                            <div class="form-group col-12 mb-4">
                                <select name="cloud_image_template_id" class="form-control">
                                    @foreach($image_templates->data as $template)
                                        <option value="{{ $template->id }}">{{ $template->name }} {!! $template->version != null ? '- v'.$template->version.' ' : '' !!}({{ $template->short_id }})</option>
                                    @endforeach
                                </select>
                            </div>

                        </div>
                        <div class="block-content block-content-full text-right bg-light">
                            <button type="button" class="btn btn-outline-secondary mr-2" data-dismiss="modal">Cancel</button>
                            <button type="submit" class="btn btn-success">Next Step</button>
                        </div>
                    @else
                        <div class="block-content">

                            <p>
                                It looks like you don't have any image templates yet. Let's <a class="text-info" href="{{ route('user.image.templates.create') }}">create an image template</a> and then you can create a new lab access token.
                            </p>

                        </div>
                        <div class="block-content block-content-full text-right bg-light">
                            <button type="button" class="btn btn-outline-secondary mr-2" data-dismiss="modal">Cancel</button>
                            <a href="{{ route('user.image.templates.create') }}" class="btn btn-info">Create an Image Template</a>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </form>
</div>
<!-- END Terms Modal -->
