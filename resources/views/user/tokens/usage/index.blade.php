@extends('_layouts.tenant_lite')

@section('page_title', 'Lab Access Token Usage')
@section('page_actions')
    <a class="btn btn-outline-primary mr-2" href="{{ route('user.tokens.show', ['tenant_reservation_token_id' => $token->data->id]) }}">
        <i class="si si-arrow-left mr-2"></i>Back to Token Details
    </a>
    <a class="btn btn-outline-info mr-2" href="#" data-toggle="tooltip" data-placement="top" title data-original-title="Coming Soon">
        <i class="si si-doc mr-2"></i>Export as CSV
    </a>
@endsection

@section('content')

    <div class="alert alert-warning">
        <div class="d-flex">
            <div class="flex-00-auto mt-2">
                <i class="si si-chemistry fa-2x mr-3 mt-2"></i>
            </div>
            <div class="flex-fill">
                <strong>Redesign In Progress</strong>. We are collecting a lot of great metrics and are rebuilding the usage dashboard to provide you with an amazing analytics experience and pivotable data set. In the mean time, below is some of the raw data that you may find useful.
            </div>
        </div>
    </div>

    <div class="block block-bordered block-rounded">
        <div class="block-header bg-body-light border-bottom">
            <h3 class="block-title">
                {{ $token->data->name }}
                <span class="badge badge-secondary ml-2">{{ $token->data->short_id }}</span>
            </h3>
            <div class="block-options">
                Created on {{ \Carbon\Carbon::parse($token->data->timestamps->created_at)->toFormattedDateString() }}
            </div>
        </div>
        <div class="block-content">
            <div class="row mb-3">
                <div class="col-lg-6">
                    <div class="font-w600">Description</div>
                    <p>
                        @if($token->data->external_notes != null)
                            {{ $token->data->external_notes }}
                        @else
                            You have not added a description for this token.<br/>
                            <a href="{{ route('user.tokens.show', ['tenant_reservation_token_id' => $token->data->id]) }}">Would you like to add a description?</a>
                        @endif
                    </p>
                </div>
                <div class="col-lg-6">
                    <div class="font-w600">Internal Notes</div>
                    <p>
                        @if($token->data->internal_notes != null)
                            {{ $token->data->internal_notes }}
                        @else
                            You have not added any internal notes for this token.<br />
                            <a href="{{ route('user.tokens.show', ['tenant_reservation_token_id' => $token->data->id]) }}">Would you like to add some notes?</a>
                        @endif
                    </p>
                </div>
            </div>
        </div>
    </div>

    <div class="block block-bordered block-rounded">
        <div class="block-header">
            <h3 class="block-title">Redemption History</h3>
        </div>
        <div class="block-content">

            @if(!empty($reservations->data))
                <table class="table table-condensed">
                    <thead>
                        <tr>
                            <th>Reservation ID</th>
                            <th>Date</th>
                            <th>User Information</th>
                            <th># Credits</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($reservations->data as $reservation)
                            <tr>
                                <td>{{ $reservation->short_id }}</td>
                                <td>{{ \Carbon\Carbon::parse($reservation->timestamps->created_at)->format('Y-m-d H:i:s T') }}</td>
                                <td>
                                    {{ $reservation->relationships->tenant_user->data->auth_user_account->data->full_name }}<br />
                                    {!! $reservation->relationships->tenant_user->data->auth_user_account->data->organization != null ? $reservation->relationships->tenant_user->data->auth_user_account->data->organization.'<br />' : '' !!}
                                    {{ $reservation->relationships->tenant_user->data->auth_user_account->data->email }}<br />
                                </td>
                                <td>
                                    {{ number_format($reservation->qty_credits, 1) }} Credits
                                </td>
                                <td>
                                    {{ $reservation->status }}
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @else
                <div class="alert alert-secondary mb-4">
                    It does not look like this token has been redeemed yet.
                </div>
            @endif
        </div>
    </div>

@endsection
