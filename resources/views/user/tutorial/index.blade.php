@extends('_layouts.tenant_lite')

@section('page_title', 'Getting Started Tutorial')
@section('page_actions')

@endsection

@section('content')

    @include('user.tutorial._partials.getting_started')

@endsection

@section('sidebar')

@endsection
