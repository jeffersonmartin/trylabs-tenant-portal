<!-- Timeline -->
<!--
    Available classes for timeline list:

    'timeline'                                      A normal timeline with icons to the left in all screens
    'timeline timeline-centered timeline-alt'       A centered timeline with odd events to the left and even events to the right (screen width > 1200px)
    'timeline timeline-centered'                    A centered timeline with all events to the left. You can add the class 'timeline-event-alt'
                                                    to 'timeline-event' elements to position them to the right (screen width > 1200px) (useful, if you
                                                    would like to have multiple events to the left or to the right section)
-->
<h2 class="content-heading mb-0">
    Getting Started
</h2>

<div class="row">
    <div class="col-12">
        <ul class="timeline">
            {{-- Create an Image Template --}}
            <li class="timeline-event">
                <div class="timeline-event-icon bg-gd-xsmooth">
                    1
                </div>
                <div class="timeline-event-block block block-rounded block-bordered">
                    <div class="block-content block-content-full">
                        <div class="d-flex">
                            <div class="flex-fill">
                                <h4 class="h4 mt-2 mb-3">Create an Image Template</h2>
                                <p class="mb-0">
                                    An Image Template contains meta data about the operating system, image size (vCPU, memory, and disk), and the snapshot of the virtual machine (VM). Each Image Template is initially created with a base Linux operating system.<br />
                                    <br />
                                    When you launch a Lab Reservation, you can configure the virtual machine however you'd like with additional packages, applications or sample data for your lab environment. When you terminate a lab reservation, you can choose whether to replace the current snapshot, create a new image template, or discard your changes.<br />
                                    <br />
                                    The best practice is to replace the current snapshot while you're working on building and refining your image template to a "gold image". When you create subsequent reservations, you will usually discard your changes or you can "save a new Image Template" to create a new version.<br />
                                    <br />
                                    Your plan has a fixed number of Image Template Snapshots. If you reach the limit, you can either delete old Image Templates that you don't need any longer or upgrade your plan to one with a higher image template limit.
                                </p>
                                <a class="btn btn-primary bg-gd-xsmooth btn-lg mt-4 mb-3 font-w400" href="{{ route('user.image.templates.create') }}"><i class="si si-disc mr-3"></i>Create an Image</a>
                            </div>
                            <div class="flex-00-auto">
                                <img src="{{ asset('images/icons/streamline/duotone/web-programming-design-construction-2.svg') }}" alt="Diagram" style="width: 150px;" class="mr-3" />
                            </div>
                        </div>
                    </div>
                </div>
            </li>
            {{-- END Create an Image Template --}}

            {{-- Launch a Lab Reservation --}}
            <li class="timeline-event">
                <div class="timeline-event-icon bg-gd-sea">
                    2
                </div>
                <div class="timeline-event-block block block-rounded block-bordered">
                    <div class="block-content block-content-full">
                        <div class="d-flex">
                            <div class="flex-fill">
                                <h4 class="h4 mt-2 mb-3">Launch a Lab Reservation</h2>
                                <p class="mb-0">
                                    Lab reservations are the bread-and-butter of TryLabs that allow you to access a VM Image Template that you've created for the few hours that you need to use the lab environment. Unlilke an always-on lab environment, we use VM snapshots to avoid having to pay for compute when you're not using it. When you're done with a lab reservation, you can choose to discard your changes or take a snapshot and we'll save your work for the next time you want to access the lab.<br />
                                    <br />
                                    Your plan includes Usage Credits that are consumed baesd on the size of the image and the number of hours that your reservation lasts. As a rule of thumb, 1 credit is equal to 1 hour of lab time with the standard image size. If you run out of credits, you can wait until your plan renews at the end of the billing cycle, or upgrade your plan to get additional credits immediately.
                                </p>
                                <a class="btn btn-info bg-gd-sea btn-lg mt-4 mb-3 font-w400" href="#modal-create-reservation" data-toggle="modal"><i class="si si-screen-desktop mr-3"></i>Launch a Lab</a>
                            </div>
                            <div class="flex-00-auto">
                                <img src="{{ asset('images/icons/streamline/duotone/cloud-laptop-image-picture-wireless.svg') }}" alt="Diagram" style="width: 150px;" class="mr-3" />
                            </div>
                        </div>
                    </div>
                </div>
            </li>
            {{-- END Launch a Lab Reservation --}}

            {{-- Publish for End Users --}}
            <li class="timeline-event">
                <div class="timeline-event-icon bg-gd-xinspire">
                    3
                </div>
                <div class="timeline-event-block block block-rounded block-bordered">
                    <div class="block-content block-content-full">
                        <div class="d-flex">
                            <div class="flex-fill">
                                <h4 class="h4 mt-2 mb-3">Publish Lab Access Token for End Users</h2>
                                <p class="mb-0">
                                    You can share an Image Template by creating a Lab Access Token. Each token has a unique URL that you can share with your end users in a YouTube video description, on your intranet, embed in a "Try It Now" HTML link or button on your website, as an external URL lesson on your LMS, or even send via email. (Ex. https://trylabs.cloud/t/a1b2c3d4) <br />
                                    <br />
                                    You can create multiple Lab Access Tokens using a single Image Template for each of your audiences, allowing you to configure different time limits, passwords, redemption limits, rate limits and whether you will pay for the lab usage or the end user should pay for their own lab usage with a credit card.<br />
                                </p>
                                <a class="btn btn-success bg-gd-xinspire btn-lg mt-4 mb-3 font-w400" href="#modal-create-token" data-toggle="modal"><i class="si si-link mr-3"></i>Share a Token</a>
                            </div>
                            <div class="flex-00-auto">
                                <img src="{{ asset('images/icons/streamline/duotone/web-programming-mouse-pointer.svg') }}" alt="Diagram" style="width: 150px;" class="mr-3" />
                            </div>
                        </div>
                    </div>
                </div>
            </li>
            {{-- END Publish for End Users --}}

            {{-- Users Access Your Lab --}}
            <li class="timeline-event">
                <div class="timeline-event-icon bg-gd-sun">
                    4
                </div>
                <div class="timeline-event-block block block-rounded block-bordered">
                    <div class="block-content block-content-full">
                        <div class="d-flex">
                            <div class="flex-fill">
                                <h4 class="h4 mt-2 mb-3">Users Redeem Lab Access Token for a Lab Reservation</h2>
                                <p class="mb-0">
                                    After a Lab Access Token is validated, we will create a new lab reservation for the end user, power up a new virtual machine instance, and provision a DNS A record for the virtual machine IP address (Ex. re5f6g7h8.trylabs.cloud). Users will be redirected to their virtual machine in less than 90 seconds after the VM has finished powering up.<br />
                                    <br />
                                    If your image template is configured with a web application, your users will see the web application natively in their browser.
                                    {{--
                                    For image templates with web applications, you can choose whether to natively display the web application or embed your application in an iFrame to be able to show your logo and a sidebar with step-by-step lab instructions.<br />
                                    --}}
                                    If you want to provide a Remote Desktop (VNC) or SSH (CLI) experience for your users, you should use our tutorial for configuring <a href="https://guacamole.apache.org" target="_blank">Apache Guacamole</a>, an open source package for HTML5 access to a virtual machine using RDP (Windows VMs), VNC (Linux VMs), or SSH (CLI).
                                    {{--
                                    If your image template is configured for RDP or VNC access, end users will use the HTML5 interface with an embedded sidebar for accessing the desktop environment without needing to install any software. We use a modified version of Apache Guacamole, an open source package for HTML5 access to a virtual machine using RDP (Windows VMs), VNC (Linux VMs), or SSH (CLI).
                                    --}}
                                </p>
                            </div>
                            <div class="flex-00-auto">
                                <img src="{{ asset('images/icons/streamline/duotone/developer-team-2.svg') }}" alt="Diagram" style="width: 150px;" class="mr-3" />
                            </div>
                        </div>
                    </div>
                </div>
            </li>
            {{-- END Users Access Your Lab --}}

            {{-- Monitor Your Usage --}}
            <li class="timeline-event">
                <div class="timeline-event-icon bg-gd-fruit">
                    5
                </div>
                <div class="timeline-event-block block block-rounded block-bordered">
                    <div class="block-content block-content-full">
                        <div class="d-flex">
                            <div class="flex-fill">
                                <h4 class="h4 mt-2 mb-3">Monitor Usage for Your End Users</h2>
                                <p class="mb-0">
                                    You likely chose a plan that fits your personal usage, however you will need a larger plan if you have shared Lab Access Tokens with end users. When you share a Lab Access Token, end users who redeem that token will consume Usage Credits each time that they launch a lab.<br />
                                    <br />
                                    It's important to keep an eye on your Usage Credit balance and upgrade your plan if needed.
                                </p>
                                <a class="btn btn-danger bg-gd-fruit btn-lg mt-4 mb-3 font-w400" href="{{ route('user.billing.usage.index') }}"><i class="si si-bar-chart mr-3"></i>View Usage</a>
                            </div>
                            <div class="flex-00-auto">
                                <img src="{{ asset('images/icons/streamline/duotone/inspecting-bill-finance-document.svg') }}" alt="Diagram" style="width: 150px;" class="mr-3" />
                            </div>
                        </div>
                    </div>
                </div>
            </li>
            {{-- END Monitor Your Usage --}}

        </ul>
    </div>
</div>

<!-- END Timeline -->
