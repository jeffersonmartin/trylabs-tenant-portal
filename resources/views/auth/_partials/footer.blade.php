<div class="block-content bg-body-light border-top py-3">
    <div class="row">
        <div class="col-8">
            <span class="small">
                &copy; 2019 <a target="_blank" href="https://www.cloudstration.com">Cloudstration Inc.</a> All Rights Reserved.<br />
                <a target="_blank" href="{{ route('legal.terms.show') }}">Terms of Service</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a target="_blank" href="{{ route('legal.privacy.show') }}">Privacy Policy</a>
            </span>
        </div>
        <div class="col-4 text-right">
            <a class="btn btn-outline-info mt-1" target="_blank" href="mailto:support@trylabs.cloud"><i class="fa fa-question-circle mr-2"></i>Contact Support</a>
        </div>
    </div>
</div>
