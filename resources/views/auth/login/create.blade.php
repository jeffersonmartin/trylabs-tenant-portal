@extends('_layouts.auth')

@section('content')

    <div class="bg-image" style="background-image: url({{ asset('images/bg-auth-table.jpg') }});">
        <div class="row no-gutters justify-content-center bg-black-75">
            <div class="hero-static col-sm-10 col-md-7 col-lg-6 col-xl-5 d-flex align-items-center p-2 px-sm-0">
                <div class="block block-transparent block-rounded w-100 mb-0 overflow-hidden">

                    <div class="block-content bg-primary-lighter text-center py-3">
                        <a href="{{ route('auth.register.account.create') }}">Don't have an account? <span class="font-w700">Create an account</span> to get started for free.</a>
                    </div>

                    <div class="block-content block-content-full px-lg-5 px-xl-6 py-4 py-md-5 py-lg-6 bg-white">
                        <!-- Header -->
                        <div class="mb-2 text-center">
                            <a class="font-w700 font-size-h1" target="_blank" href="https://www.trylabs.io">
                                <span class="text-primary">Try</span><span class="text-gray-dark">Labs</span>
                            </a>
                            <p class="text-uppercase font-w700 font-size-sm text-muted">Welcome back! Please sign in.</p>
                        </div>
                        <!-- END Header -->

                        @if($errors->any())
                            <div class="alert alert-warning">
                                <strong>You'll need to make a few changes before your magic spells will work here.</strong>
                                <ul class="mb-0">
                                    @foreach($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        @if (session('alert'))
                            <div class="alert alert-danger">
                                {{ session('alert') }}
                            </div>
                        @endif

                        <form class="" action="{{ route('auth.login.store') }}" method="POST">
                            @csrf

                            <input type="hidden" name="timezone" id="timezone" />

                            <div class="form-group mt-5">
                                <label for="email">Email Address</label>
                                <input type="email" class="form-control form-control-lg" id="email" name="email" placeholder="">
                            </div>

                            <div class="form-group">
                                <label for="password">Password</label>
                                <input type="password" class="form-control form-control-lg" id="password" name="password" placeholder="">
                            </div>

                            <!--
                            <div class="form-group text-center">
                                <a class="font-w600 font-size-sm" href="#" data-toggle="modal" data-target="#modal-terms">Terms &amp; Conditions</a>
                                <div class="custom-control custom-checkbox custom-control-primary">
                                    <input type="checkbox" class="custom-control-input" id="signup-terms" name="signup-terms">
                                    <label class="custom-control-label" for="signup-terms">I agree</label>
                                </div>
                            </div>
                            -->

                            <div class="d-flex pt-2">
                                <div class="flex-fill">
                                    <a href="{{ route('auth.register.account.create') }}" class="text-info">Create an Account</a><br />
                                    <a href="#" class="text-info" v-b-tooltip.hover title="Temporarily Unavailable">Reset Password</a>
                                </div>
                                <div class="flex-00-auto">
                                    <button type="submit" class="btn btn-lg btn-outline-success">
                                        Sign In
                                    </button>
                                </div>
                            </div>

                        </form>
                        <!-- END Sign Up Form -->
                    </div>

                    <!-- Footer -->
                    <div class="block-content bg-body-light border-top py-3">
                        <div class="row">
                            <div class="col-8">
                                <span class="small">
                                    &copy; 2019 <a target="_blank" href="https://www.cloudstration.com">Cloudstration Inc.</a> All Rights Reserved.<br />
                                    <a target="_blank" href="{{ route('legal.terms.show') }}">Terms of Service</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a target="_blank" href="{{ route('legal.privacy.show') }}">Privacy Policy</a>
                                </span>
                            </div>
                            <div class="col-4 text-right">
                                <a class="btn btn-outline-info mt-1" target="_blank" href="mailto:support@trylabs.cloud"><i class="fa fa-question-circle mr-2"></i>Contact Support</a>
                            </div>
                        </div>
                    </div>
                    <!-- END Footer -->

                </div>
            </div>
        </div>
    </div>

@endsection

@section('js_after')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment-timezone/0.5.26/moment-timezone-with-data-2012-2022.js"></script>
    <script>
        var timezone = moment.tz.guess();
        $('#timezone').val(timezone);
    </script>
@endsection
