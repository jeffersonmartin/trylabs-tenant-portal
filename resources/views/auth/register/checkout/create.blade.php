@extends('_layouts.auth')

@section('content')

    <div class="bg-image" style="background-image: url({{ asset('images/bg-auth-payment.jpg') }});">
        <div class="row no-gutters justify-content-center bg-black-75">
            <div class="hero-static col-sm-10 col-md-7 col-lg-6 col-xl-5 d-flex align-items-center p-2 px-sm-0">
                <div class="block block-transparent block-rounded w-100 mb-0 overflow-hidden">

                    <div class="block-content block-content-full px-lg-5 px-xl-6 py-4 py-md-5 py-lg-6 bg-white">
                        <!-- Header -->
                        <div class="mb-5 text-center">
                            <a class="font-w700 font-size-h1" target="_blank" href="https://www.trylabs.io">
                                <span class="text-primary">Try</span><span class="text-gray-dark">Labs</span>
                            </a>
                            <p class="text-uppercase font-w700 font-size-sm text-muted">Let's activate your subscription.</p>
                        </div>
                        <!-- END Header -->

                        <form class="" action="{{ route('auth.register.checkout.store') }}" method="POST" id="payment-form">
                            @csrf

                            @if($errors->any())
                                <div class="alert alert-warning">
                                    <strong>The overlords of the billing system sense that something is wrong.</strong>
                                    <ul class="mb-0">
                                        @foreach($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            <div class="">

                                <span class="font-w700">{{ $request->user()->full_name }}</span><br />
                                {!! $request->user()->organization_name != null ? $request->user()->organization_name.'<br />' : '' !!}
                                {{ $request->user()->email }}<br />


                                <!-- Table -->
                                <div class="table-responsive push">
                                    <table class="table table-condensed">
                                        <thead class="bg-body-light">
                                            <tr>
                                                <th style="width: 15%;">Qty</th>
                                                <th style="width: 65%;">Usage</th>
                                                <th class="text-right" style="width: 120px;">Amount</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>80</td>
                                                <td>
                                                    <p class="font-w600 mb-1">VM Usage Credit Hours (Compute)</p>

                                                </td>
                                                <td class="text-right">$40.00</td>
                                            </tr>
                                            <tr>
                                                <td>10</td>
                                                <td>
                                                    <p class="font-w600 mb-1">Image Template Snapshots (Storage)</p>

                                                </td>
                                                <td class="text-right">$10.00</td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" class="font-w600 text-right">Subtotal</td>
                                                <td class="text-right">$50.00</td>
                                            </tr>

                                            <tr>
                                                <td colspan="2" class="font-w600 text-right">Taxes</td>
                                                <td class="text-right">$0.00</td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" class="font-w700 text-uppercase text-right bg-body-light">Total Due Today</td>
                                                <td class="font-w700 text-right bg-body-light">$50.00</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- END Table -->

                                <!-- Footer -->
                                <p class="text-muted text-center my-5">
                                    @if(session('plan_id_period') == 'mo')
                                        Your plan will renew each month starting on {{ now()->addMonth()->toFormattedDateString() }}
                                    @elseif(session('plan_id_period') == 'yr')
                                        Your plan will renew each year starting on {{ now()->addYear()->toFormattedDateString() }}
                                    @endif
                                </p>
                                <!-- END Footer -->
                            </div>

                            <div class="text-center">
                                <button type="submit" class="btn btn-success btn-lg">Pay Now & Activate Subscription<i class="fa fa-chevron-right ml-2"></i></button>
                            </div>
                            <div class="text-center mt-4">
                                <a class="btn btn-outline-secondary btn-sm" href="{{ route('user.dashboard.index') }}">Not ready? Proceed with free plan and upgrade later</a>
                            </div>

                        </form>
                        <!-- END Sign Up Form -->
                    </div>

                    <!-- Footer -->
                    <div class="block-content bg-body-light border-top py-3">
                        <div class="row">
                            <div class="col-8">
                                <span class="small">
                                    &copy; 2019 <a target="_blank" href="https://www.cloudstration.com">Cloudstration Inc.</a> All Rights Reserved.<br />
                                    <a target="_blank" href="{{ route('legal.terms.show') }}">Terms of Service</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a target="_blank" href="{{ route('legal.privacy.show') }}">Privacy Policy</a>
                                </span>
                            </div>
                            <div class="col-4 text-right">
                                <a class="btn btn-outline-info mt-1" target="_blank" href="mailto:support@trylabs.cloud"><i class="fa fa-question-circle mr-2"></i>Contact Support</a>
                            </div>
                        </div>
                    </div>
                    <!-- END Footer -->

                </div>
            </div>
        </div>
    </div>

@endsection

@section('js_before')
@endsection

@section('js_after')
@endsection
