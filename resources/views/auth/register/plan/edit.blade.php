@extends('_layouts.auth')

@section('content')

    <div class="bg-image" style="background-image: url({{ asset('images/bg-auth-couch.jpg') }});">
        <div class="row no-gutters justify-content-center bg-black-75">
            <div class="hero-static col-sm-12 col-md-11 col-lg-10 col-xl-8 d-flex align-items-center p-2 px-sm-0">
                <div class="block block-transparent block-rounded w-100 mb-0 overflow-hidden">

                    <div class="block-content block-content-full px-lg-5 px-xl-6 py-4 py-md-5 py-lg-6 bg-white">
                        <!-- Header -->
                        <div class="mb-5 text-center">
                            <a class="font-w700 font-size-h1" target="_blank" href="https://www.trylabs.io">
                                <span class="text-primary">Try</span><span class="text-gray-dark">Labs</span>
                            </a>
                            <p class="text-uppercase font-w700 font-size-sm text-muted">Please select a plan that fits your expected usage.</p>
                        </div>
                        <!-- END Header -->

                        <form class="" action="{{ route('auth.register.plan.update') }}" method="POST">
                            @csrf
                            @method('PATCH')

                                @if($errors->any())
                                    <div class="alert alert-warning">
                                        <strong>Whoops! It looks like you forgot an incantation.</strong>
                                        <ul class="mb-0">
                                            @foreach($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif

                                <div class="form-group row items-push mb-0">

                                    <div class="col-sm-12 col-md-4">
                                        <div class="custom-control custom-block custom-control-primary mb-1">
                                            @if($request->old('plan_id_prefix') != null)
                                                <input type="radio" class="custom-control-input" id="try1u-plan-20-5" name="plan_id_prefix" value="try1u-plan-20-5" {!! $request->old('plan_id_prefix') == "try1u-plan-20-5" ? 'checked="checked"' : '' !!}>
                                            @else
                                                <input type="radio" class="custom-control-input" id="try1u-plan-20-5" name="plan_id_prefix" value="try1u-plan-20-5" {!! $request->session()->get('plan_id_prefix') == "try1u-plan-20-5" ? 'checked="checked"' : '' !!}>
                                            @endif
                                            @include('auth.register.plan._partials.magician')
                                            <span class="custom-block-indicator">
                                                <i class="fa fa-check"></i>
                                            </span>
                                        </div>
                                    </div>

                                    <div class="col-sm-12 col-md-4">
                                        <div class="custom-control custom-block custom-control-primary mb-1">
                                            @if($request->old('plan_id_prefix') != null)
                                                <input type="radio" class="custom-control-input" id="try1u-plan-80-10" name="plan_id_prefix" value="try1u-plan-80-10" {!! $request->old('plan_id_prefix') == "try1u-plan-80-10" ? 'checked="checked"' : '' !!}>
                                            @else
                                                <input type="radio" class="custom-control-input" id="try1u-plan-80-10" name="plan_id_prefix" value="try1u-plan-80-10" {!! $request->session()->get('plan_id_prefix') == "try1u-plan-80-10" ? 'checked="checked"' : '' !!}>
                                            @endif
                                            @include('auth.register.plan._partials.wizard')
                                            <span class="custom-block-indicator">
                                                <i class="fa fa-check"></i>
                                            </span>
                                        </div>
                                    </div>

                                    <div class="col-sm-12 col-md-4">
                                        <div class="custom-control custom-block custom-control-primary mb-1">
                                            @if($request->old('plan_id_prefix') != null)
                                                <input type="radio" class="custom-control-input" id="try1u-plan-250-25" name="plan_id_prefix" value="try1u-plan-250-25" {!! $request->old('plan_id_prefix') == "try1u-plan-250-25" ? 'checked="checked"' : '' !!}>
                                            @else
                                                <input type="radio" class="custom-control-input" id="try1u-plan-250-25" name="plan_id_prefix" value="try1u-plan-250-25" {!! $request->session()->get('plan_id_prefix') == "try1u-plan-250-25" ? 'checked="checked"' : '' !!}>
                                            @endif
                                            @include('auth.register.plan._partials.sorcerer')
                                            <span class="custom-block-indicator">
                                                <i class="fa fa-check"></i>
                                            </span>
                                        </div>
                                    </div>

                                    <div class="col-sm-12 col-md-12">
                                        <div class="custom-control custom-block custom-control-primary mb-1">
                                            @if($request->old('plan_id_prefix') != null)
                                                <input type="radio" class="custom-control-input" id="try1u-plan-free" name="plan_id_prefix" value="try1u-plan-free" {!! $request->old('plan_id_prefix') == "try1u-plan-free" ? 'checked="checked"' : '' !!}>
                                            @else
                                                <input type="radio" class="custom-control-input" id="try1u-plan-free" name="plan_id_prefix" value="try1u-plan-free" {!! $request->session()->get('plan_id_prefix') == "try1u-plan-free" ? 'checked="checked"' : '' !!}>
                                            @endif
                                            @include('auth.register.plan._partials.free')
                                            <span class="custom-block-indicator">
                                                <i class="fa fa-check"></i>
                                            </span>
                                        </div>
                                    </div>

                            </div>

                            <div class="row">
                                <div class="col-12">
                                    <ul class="list-group push">
                                        <li class="list-group-item">
                                            <div class="custom-control custom-checkbox custom-control-lg mb-1">
                                                <input type="checkbox" class="custom-control-input" id="flag_terms_accepted" name="flag_terms_accepted">
                                                @include('auth.register.plan._partials.terms')
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                            <div class="text-center">
                                <button type="submit" class="btn btn-lg btn-outline-success">
                                    Next Step<i class="fa fa-chevron-right ml-2"></i>
                                </button>
                            </div>

                        </form>
                        <!-- END Sign Up Form -->
                    </div>

                    <!-- Footer -->
                    @include('auth._partials.footer')
                    <!-- END Footer -->

                </div>
            </div>
        </div>
    </div>

@endsection
