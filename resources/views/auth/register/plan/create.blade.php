@extends('_layouts.auth')

@section('content')

    <div class="bg-image" style="background-image: url({{ asset('images/bg-auth-couch.jpg') }});">
        <div class="row no-gutters justify-content-center bg-black-75">
            <div class="hero-static col-sm-12 col-md-11 col-lg-10 col-xl-8 d-flex align-items-center p-2 px-sm-0">
                <div class="block block-transparent block-rounded w-100 mb-0 overflow-hidden">

                    <div class="block-content block-content-full px-lg-5 px-xl-6 py-4 py-md-5 py-lg-6 bg-white">
                        <!-- Header -->
                        <div class="mb-5 text-center">
                            <a class="font-w700 font-size-h1" target="_blank" href="https://www.trylabs.io">
                                <span class="text-primary">Try</span><span class="text-gray-dark">Labs</span>
                            </a>
                            <p class="text-uppercase font-w700 font-size-sm text-muted">Please select a plan that fits your expected usage.</p>
                        </div>
                        <!-- END Header -->

                        <form class="" action="{{ route('auth.register.plan.store') }}" method="POST">
                            @csrf

                                @if($errors->any())
                                    <div class="alert alert-warning">
                                        <strong>Whoops! It looks like you forgot an incantation.</strong>
                                        <ul class="mb-0">
                                            @foreach($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif

                                <ul class="nav nav-tabs nav-tabs-block justify-content-center bg-white mb-5" data-toggle="tabs" role="tablist" >
                                    <li class="nav-item" >
                                        <a class="btn btn-lg btn-outline-info mr-3 active" href="#monthly">Monthly</a>
                                    </li>
                                    <li class="nav-item" >
                                        <a class="btn btn-lg btn-outline-info" href="#annual">Annual <span class="badge badge-info ml-2">2 Months Free</span></a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="monthly" role="tabpanel">
                                        <div class="form-group row items-push mb-0">

                                            <div class="col-sm-12 col-md-4">
                                                <div class="custom-control custom-block custom-control-primary mb-1">
                                                    <input type="radio" class="custom-control-input" id="try1u-plan-20-5-mo" name="plan_id" value="try1u-plan-20-5-mo">
                                                    @include('auth.register.plan._partials.magician_mo')
                                                    <span class="custom-block-indicator">
                                                        <i class="fa fa-check"></i>
                                                    </span>
                                                </div>
                                            </div>

                                            <div class="col-sm-12 col-md-4">
                                                <div class="custom-control custom-block custom-control-primary mb-1">
                                                    <input type="radio" class="custom-control-input" id="try1u-plan-80-10-mo" name="plan_id" value="try1u-plan-80-10-mo">
                                                    @include('auth.register.plan._partials.wizard_mo')
                                                    <span class="custom-block-indicator">
                                                        <i class="fa fa-check"></i>
                                                    </span>
                                                </div>
                                            </div>

                                            <div class="col-sm-12 col-md-4">
                                                <div class="custom-control custom-block custom-control-primary mb-1">
                                                    <input type="radio" class="custom-control-input" id="try1u-plan-250-25-mo" name="plan_id" value="try1u-plan-250-25-mo">
                                                    @include('auth.register.plan._partials.sorcerer_mo')
                                                    <span class="custom-block-indicator">
                                                        <i class="fa fa-check"></i>
                                                    </span>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="tab-pane" id="annual" role="tabpanel">
                                        <div class="form-group row items-push mb-0">

                                            <div class="col-sm-12 col-md-4">
                                                <div class="custom-control custom-block custom-control-primary mb-1">
                                                    <input type="radio" class="custom-control-input" id="try1u-plan-20-5-yr" name="plan_id" value="try1u-plan-20-5-yr">
                                                    @include('auth.register.plan._partials.magician_yr')
                                                    <span class="custom-block-indicator">
                                                        <i class="fa fa-check"></i>
                                                    </span>
                                                </div>
                                            </div>

                                            <div class="col-sm-12 col-md-4">
                                                <div class="custom-control custom-block custom-control-primary mb-1">
                                                    <input type="radio" class="custom-control-input" id="try1u-plan-80-10-yr" name="plan_id" value="try1u-plan-80-10-yr">
                                                    @include('auth.register.plan._partials.wizard_yr')
                                                    <span class="custom-block-indicator">
                                                        <i class="fa fa-check"></i>
                                                    </span>
                                                </div>
                                            </div>

                                            <div class="col-sm-12 col-md-4">
                                                <div class="custom-control custom-block custom-control-primary mb-1">
                                                    <input type="radio" class="custom-control-input" id="try1u-plan-250-25-yr" name="plan_id" value="try1u-plan-250-25-yr">
                                                    @include('auth.register.plan._partials.sorcerer_yr')
                                                    <span class="custom-block-indicator">
                                                        <i class="fa fa-check"></i>
                                                    </span>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row items-push mb-0">

                                    <div class="col-sm-12 col-md-12">
                                        <div class="custom-control custom-block custom-control-primary mb-1">
                                            <input type="radio" class="custom-control-input" id="try1u-plan-free" name="plan_id" checked="checked" value="try1u-plan-free-mo">
                                            @include('auth.register.plan._partials.free')
                                            <span class="custom-block-indicator">
                                                <i class="fa fa-check"></i>
                                            </span>
                                        </div>
                                    </div>

                                </div>

                            <div class="row">
                                <div class="col-12">
                                    <ul class="list-group push">
                                        <li class="list-group-item">
                                            <div class="custom-control custom-checkbox custom-control-lg mb-1">
                                                <input type="checkbox" class="custom-control-input" id="flag_terms_accepted" name="flag_terms_accepted">
                                                @include('auth.register.plan._partials.terms')
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                            <div class="text-center">
                                <button type="submit" class="btn btn-lg btn-outline-success">
                                    Next Step<i class="fa fa-chevron-right ml-2"></i>
                                </button>
                            </div>

                        </form>
                        <!-- END Sign Up Form -->
                    </div>

                    <!-- Footer -->
                    @include('auth._partials.footer')
                    <!-- END Footer -->

                </div>
            </div>
        </div>
    </div>

@endsection
