<label class="custom-control-label" for="flag_terms_accepted">
    I accept the <a target="_blank" href="{{ route('legal.terms.show') }}">Terms of Service</a>.<br />
    <span class="small text-secondary">
        <span class="font-w700">TL;DR</span> We are an infrastructure service provider and will do our best to support your use of our platform. We are not responsible for how you configure your virtual machines and cannot provide support for your end users. We reserve the right to suspend your account if you do something that you shouldn't (violate the terms, abuse the system, break the law, etc).
    </span>
</label>
