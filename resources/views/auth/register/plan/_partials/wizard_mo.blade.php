<label class="custom-control-label" for="try1u-plan-80-10-mo">
    <span class="d-block font-w400 text-center my-3">
        <i class="icon-Witch-Hat" style="font-size: 3em;"></i><br />
        <div class="font-size-h2 font-w600">Wizard</div>
        <div class="text-uppercase font-w700 font-size-sm text-muted mb-3">~10 Lab Days per Month</div>

        <p>
            <strong>80</strong> VM Credit Hours per Month<br />
            <strong>10</strong> Image Templates<br />
            Access to Community Forums<br />
            Email Support<br />
            <br />
        </p>
        <div class="py-2">
            <p class="display-4 font-w700 my-0 text-info">$50</p>
            <p class="h6 text-muted">per month</p>
        </div>
    </span>
</label>
