<label class="custom-control-label" for="try1u-plan-free">
    <span class="d-block font-w400 text-center my-3">
        <div class="font-size-h2 font-w600">FREE</div>
        <div class="text-uppercase font-w700 font-size-sm text-muted mb-3">hands-on feature preview</div>
        <p>
            You have access to all of the features to explore the platform and decide if it's right for your needs.<br />You will not be able to power up virtual machines until you upgrade to a paid plan.<br />
        </p>
    </span>
</label>
