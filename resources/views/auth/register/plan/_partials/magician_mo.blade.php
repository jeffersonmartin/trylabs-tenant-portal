<label class="custom-control-label" for="try1u-plan-20-5-mo">
    <span class="d-block font-w400 text-center my-3">
        <i class="icon-Hat" style="font-size: 3em;"></i><br />
        <div class="font-size-h2 font-w600">Magician</div>
        <div class="text-uppercase font-w700 font-size-sm text-muted mb-3">2-3 Lab Days per Month</div>

        <p>
            <strong>20</strong> VM Credit Hours per Month<br />
            <strong>5</strong> Image Templates<br />
            Access to Community Forums<br />
            Email Support<br />
            <br />
        </p>
        <div class="py-2">
            <p class="display-4 font-w700 my-0 text-primary">$15</p>
            <p class="h6 text-muted">per month</p>
        </div>
    </span>
</label>
