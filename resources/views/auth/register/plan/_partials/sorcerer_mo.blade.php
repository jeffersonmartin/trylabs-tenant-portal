<label class="custom-control-label" for="try1u-plan-250-25-mo">
    <span class="d-block font-w400 text-center my-3">
        <i class="icon-Wizard" style="font-size: 3em;"></i><br />
        <div class="font-size-h2 font-w600">Sorcerer</div>
        <div class="text-uppercase font-w700 font-size-sm text-muted mb-3">Lab'ing Full Time</div>

        <p>
            <strong>250</strong> VM Credit Hours per Month<br />
            <strong>25</strong> Image Templates<br />
            Access to Community Forums<br />
            Priority Email Support<br />
            Feature Roadmap Voting<br />
        </p>
        <div class="py-2">
            <p class="display-4 font-w700 my-0 text-success">$100</p>
            <p class="h6 text-muted">per month</p>
        </div>
    </span>
</label>
